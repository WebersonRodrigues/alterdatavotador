'use strict'
const path = require('path')
const utils = require('./utils')
const webpack = require('webpack')
const merge = require('webpack-merge')
const baseWebpackConfig = require('./webpack.base.conf')
const HtmlWebpackPlugin = require('html-webpack-plugin')


const mode = 'production'
const config = baseWebpackConfig(mode)
const webpackConfig = merge(config, {
  mode: mode,
  devtool: '#source-map',
  output: {
    path: utils.distFolder,
    filename: 'js/[name].[chunkhash].js',
    chunkFilename: 'js/[name].[chunkhash].js'
  },  
  plugins: [        
    new HtmlWebpackPlugin({
      filename: utils.indexHTML,
      template: 'index.html',
      inject: true,
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeAttributeQuotes: true
      },
      chunksSortMode: 'dependency'
    }),    
    new webpack.HashedModuleIdsPlugin(),
    new webpack.optimize.ModuleConcatenationPlugin()
  ]
})

module.exports = webpackConfig;

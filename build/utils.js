'use strict'
const path = require('path')

exports.distFolder = path.resolve(__dirname, '..', 'AlterdataVotador', 'wwwroot');

exports.indexHTML = path.resolve(exports.distFolder, 'Views', 'index.html')
exports.assetsPublicPath = '/Content/'
exports.assetsSubDirectory = 'static'


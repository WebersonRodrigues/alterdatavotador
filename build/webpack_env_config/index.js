'use strict'
// Template version: 1.3.1
// see http://vuejs-templates.github.io/webpack for documentation.

const path = require('path')
const projectName = 'AlterdataVotador';
const distFolder = path.resolve(__dirname, '..', projectName, 'wwwroot');
console.log(distFolder);

module.exports = {
  dev: {
    assetsSubDirectory: 'static',
    assetsPublicPath: '/Content/',
    proxyTable: {},
    useEslint: true,
    showEslintErrorsInOverlay: false,
    devtool: 'cheap-module-eval-source-map',
    cacheBusting: true,
    cssSourceMap: true
  },

  build: {
    index: path.resolve(distFolder, 'Views', 'index.html'),
    assetsRoot: distFolder,
    assetsSubDirectory: '',
    assetsPublicPath: '/Content/js',
    productionSourceMap: true,
    devtool: '#source-map',
    productionGzip: false,
    productionGzipExtensions: ['js', 'css'],
    bundleAnalyzerReport: process.env.npm_config_report
  }
}

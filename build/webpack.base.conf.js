'use strict'
const webpack = require('webpack')
const path = require('path')
const utils = require('./utils')

const CopyWebpackPlugin = require('copy-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const OptimizeCSSAssetPlugin = require('optimize-css-assets-webpack-plugin')

const basePath = path.resolve(__dirname, "..", "client");

function resolve(dir) {
  return path.join(__dirname, '..', 'client', dir)  
}

function resolveLoader(dir) {
    dir = dir || '.';
    return path.join(__dirname, '..', 'build', dir)    
}

function getStyleLoader(env) {
    if(env !== 'production') 
        return 'vue-style-loader'
    else
        return { loader: MiniCssExtractPlugin.loader }
}

module.exports = function(env) {
    env = env || 'development'
    
    return {
        context: basePath,
        entry: {
            app: './src/main.js'
        },
        output: {
            path: utils.distFolder,
            filename: 'js/[name].js',
            publicPath: utils.assetsPublicPath
        },
        //Isso aqui pega todas as dependências, inclusive as do node módulos e cria um arquivo chamado app"hash".js
        //com todos as dependencias inclusas.
        //Devido ao Chunks: all, será criado um tensor do app"hash".js" 
        resolve: {
            extensions: ['.js', '.vue', '.json'],
            alias: {
                'vue$': resolve('node_modules/vue/dist/vue.esm.js'),
                '@': resolve('src')
            },
            modules: [ resolve("node_modules"), resolveLoader("node_modules") ]
        },
        optimization: {
            splitChunks: {
              chunks: 'all',
              minSize: 30000,
              minChunks: 1,
              maxAsyncRequests: 5,
              maxInitialRequests: 3,
              automaticNameDelimiter: '~',
              name: true,
              cacheGroups: {
                vendors: {
                  test: /[\\/]node_modules[\\/]/,
                  priority: -10
                },
                default: {
                  minChunks: 2,
                  priority: -20,
                  reuseExistingChunk: true
                }
              }
            },
            minimizer: [
              new UglifyJsPlugin({
                sourceMap: true,
                parallel: true
              }),
              new OptimizeCSSAssetPlugin({})
            ]
          },
        plugins: [
            new webpack.ProvidePlugin({
                $: "jquery",
                jQuery: "jquery",
                'window.jQuery': 'jquery'
            }),
            new HtmlWebpackPlugin({
                filename: utils.indexHTML,
                template: 'index.html',
                inject: true
            }),
            new CopyWebpackPlugin([
                {
                  from: path.resolve(basePath, 'static'),
                  to: '',
                  ignore: ['.*']
                }
            ]),
            new MiniCssExtractPlugin({
                filename: 'css/[name].[hash].css',
                chunkFilename: "css/[name].[hash].css"
            })
        ],
        module: {
            rules: [
                {
                    test: /\.css$/,
                        use: [
                            'vue-style-loader',
                            'css-loader'
                        ],
                    },
                {
                    test: /\.scss$/,
                        use: [
                            'vue-style-loader',
                            'css-loader',
                            'sass-loader'
                        ],
                    },
                {
                    test: /\.sass$/,
                        use: [
                            'vue-style-loader',
                            'css-loader',
                            'sass-loader?indentedSyntax'
                        ],
                },
                {
                    test: /\.vue$/,
                    loader: 'vue-loader',
                    options: {
                        transformToRequire: {
                            video: ['src', 'poster'],
                            source: 'src',
                            img: 'src',
                            image: 'xlink:href'
                          }
                    }
                },
                {
                    test: /\.js$/,
                    loader: 'babel-loader',
                    exclude: /(node_modules)/,
                    include: [ resolve('src') ]
                },
                {
                    test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
                    loader: 'url-loader',
                    options: {
                        limit: 10000,
                        name: 'img/[name].[hash:7].[ext]'
                    }
                },
                {
                    test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
                    loader: 'url-loader',
                    options: {
                        limit: 10000,
                        name: 'media/[name].[hash:7].[ext]'
                    }
                },
                {
                    test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
                    loader: 'url-loader',
                    options: {
                        limit: 10000,
                        name: 'fonts/[name].[hash:7].[ext]'
                    }
                }
            ]
        }
    }
}
const {clean, restore, build, test, publish, run} = require('gulp-dotnet-cli');
const path = require('path');
const fs = require('fs');
const gulp = require('gulp');
const Promise = require('bluebird');
const pump = require('pump');
const yaml = require('js-yaml');
const webpack = require('webpack');

const commandLineArgs = require('command-line-args');
const optionDefinitions = [
    { name: 'platform', type: String, defaultValue: 'any' },
    { name: 'host', type: String, defaultValue: 'IP_DO_SERVIDOR' },
    { name: 'username', type: String, defaultValue: 'USUARIO' },
    { name: 'password', type: String, defaultValue: 'SENHA' },
    { name: 'resolvenginx', type: Boolean, defaultValue: false },
]

const options = commandLineArgs(optionDefinitions, { partial: true} );

const PROJECT_NAME = 'AlterdataVotador';

const VERSION = getVersionFromCompose();

const publishOutput = '\\\\alterdata.matriz\\mapeamentos\\G\\Temp\\WEBERSON\\DSN\\PROJETO_ALTERDATA_VOTADOR';

const allCsProjPath = '../**/*.csproj';
const testProjPath = path.resolve('..', PROJECT_NAME + '.Test', PROJECT_NAME + '.Test.csproj');
const mainProjPath = path.resolve('..', PROJECT_NAME, PROJECT_NAME + '.csproj');

const slnPath = path.resolve("..", PROJECT_NAME + '.sln');

gulp.task('clean', ()=>{
    return gulp.src(allCsProjPath, {read: false})
            .pipe(clean());
});

gulp.task('restore', ()=>{
    return gulp.src(allCsProjPath, {read: false})
            .pipe(restore());
});

gulp.task('build', ['clean', 'restore'], ()=>{
    return gulp.src(slnPath, {read: false, version: VERSION})
        .pipe(build());
});

gulp.task('test', ['clean', 'restore'], ()=>{
    return gulp.src(testProjPath, {read: false})
        .pipe(test({ logger: 'trx' }))
});

gulp.task('webpack', () => {
    const webpackConfig = require('./webpack.prod.conf.js')
    return new Promise((resolve, reject) => {
        webpack(webpackConfig, (err, stats) => {
            if (err) {
                console.error(err.stack || err);
                if (err.details)
                    console.error(err.details);
                
                return reject(err)
            }
            
            const info = stats.toJson();
        
            if (stats.hasErrors())
                info.errors.map((err) => console.error(err))
        
            if (stats.hasWarnings())
                info.warnings.map((warn) => console.warn(warn))

            resolve()
        })
    })
})

gulp.task('publish', [ 'test', 'webpack' ], () => {
    let config = {
        configuration: 'Release',
        version: VERSION
    }

    switch (options.platform.toLowerCase()) {
        case 'any': break;
        case 'win': config.runtime = 'win7-x64'; break;
        case 'linux': config.runtime = 'linux-x64'; break;
        default: 
            console.log('Valor de plataforma inválido. Informe \'win\', \'linux\' ou deixe vazio.');
            return 1;
            break;
    }

    if(config.runtime)
        console.log('Publicando no modo SCD (Self-contained Deployment).');
    else
        console.log('Publicando no modo FDD (Framework-dependent Deployment).');

    
    return new Promise((resolve, reject) => {
        pump(
            gulp.src(mainProjPath, {read: false}),
            publish(config),
            (err) => {
                if(err)
                    return reject(err)
                
                resolve()
            }
        )
    });
});

gulp.task('deploy:docker', [ 'test', 'webpack' ], () => {
    return new Promise((resolve, reject) => {
        cleanProjectsOutput();
        copyProjectArchivesToTempFolder()
            .then((destFolder) => {
                    const projectName = PROJECT_NAME.toLowerCase();
                    upload({
                        ftpPath: `/home/dsnErp/docker/${projectName}/${projectName}`,
                        options: options,
                        filesToBeSent: destFolder
                    })
                    .then( () => {
                        console.log('Rodando script de deploy do docker...')
                        let command = `node ~/scripts/docker/deploy_docker.js ${PROJECT_NAME} --copy-files --push-image  ${ options.resolvenginx ? "--resolve-nginx" : "" }`;
                        console.log(`Comando a ser executado: ${command}`)
                        return performssh({
                            command: command,
                            options: options
                        })
                        .then(resolve)
                        .catch(reject);
                    })
                    .catch(reject)
            })
            .catch(reject)
            
    })
});

gulp.task('publish:zip', [ 'publish' ], () => {
    const archiver = require('archiver');
    const fs = require('fs');
    const archive = archiver('zip', { 
        zlib: { level: 9 }
     });
    
    const targetFolder = path.resolve(publishOutput, PROJECT_NAME);
    try {
        fs.statSync(targetFolder)
    }
    catch (err) {
        if(err.code === 'ENOENT')
            fs.mkdirSync(targetFolder);
        else {
            console.log('Ocorreu um erro: \n' + err);
            return 1;
        }
    }
     
    const out = fs.createWriteStream(path.resolve(publishOutput, PROJECT_NAME, `${VERSION}.zip`));
    archive.directory(resolveFolderName(), VERSION);
    archive.pipe(out);
    console.log('Comprimindo arquivos...');
    return new Promise((resolve, reject) => {
        out.on('close', () => {
            console.log(`Arquivos comprimidos com acesso. ~${parseFloat(archive.pointer() / (1024 * 1024)).toFixed(2)} MBs totais.`);
            resolve();
        });

        archive.on('error', (err) => {
            console.log('Ocorreu um erro na compressão dos arquivos.');
            reject(err);
        });

        archive.finalize();
    });
});

gulp.task('upload', [ 'publish:zip' ], () => {
    return upload({ ftpPath: `/home/dsnErp/${projectName}/${projectName}_new`, options: options });
});

gulp.task('run', ['restore'], ()=>{
    return gulp.src(mainProjPath, {read: false})
            .pipe(run());
});

function resolveFolderName() {
    let folderName = 'publish';
    switch(options.platform) {
        case 'win': folderName = path.join('win7-x64', folderName); break;
        case 'linux': folderName = path.join('linux-x64', folderName); break;
    }

    return path.resolve('..', PROJECT_NAME, 'bin', 'Release', 'netcoreapp2.0', folderName) + path.sep;
}

function upload(args) {
    if(options.platform !== "linux") {
        console.log('Não é possível publicar aplicação em plataforma diferente de Linux.');
        return 1;
    }

    return new Promise((resolve, reject) => {
        const client = require('scp2');
        const ftpPath = args.ftpPath;
        const options = args.options;

        let filesToBeSent = args.filesToBeSent || resolveFolderName();
        
        console.log(`Enviando arquivos para ${options.host} | (${filesToBeSent} => ${ftpPath})`)

        client.scp(filesToBeSent, {
            host: options.host,
            username: options.username,
            password: options.password,
            path: ftpPath
        }, (err) => {
            if(err) {
                console.log('Ocorreu um erro: \n' + err);
                reject();
            }

            console.log('Arquivos enviados com sucesso')
            resolve();
        });
    });
}

function performssh(args) {
    const ssh = require('ssh-exec');
    const options = args.options;
    
    return new Promise((resolve, reject) => {
        let task = ssh(args.command, {
            host: options.host,
            user: options.username,
            password: options.password
        });

        task.pipe(process.stdout);
        task.on('error', (err) => {
            console.log('Ocorreu um erro: \n' + err);
            reject(err);
        })

        task.on('finish', resolve);
    });
}

function getProjectsArchivesName() {
    const fs = require('fs');
    return fs.readdirSync('..').filter(f => f.indexOf(PROJECT_NAME) > -1 || 
                                            f === "docker-compose.yaml" || 
                                            f === "Dockerfile")
}

function getProjectsArchivesPath() {
    const path = require('path');
    return getProjectsArchivesName().map(d => path.resolve('..', d));
}

function getProjectsDirectoryName() {
    return getProjectsArchivesName().filter(f => f.indexOf(".sln") === -1);
}

function getProjectsDirectoryPath() {
    const path = require('path');
    return getProjectsDirectoryName().map(d => path.resolve('..', d));
}

function cleanProjectsOutput() {
    const fs = require('fs');
    const path = require('path');
    let directories = getProjectsDirectoryPath();
    directories.map(dir => {
        const bin = path.resolve(dir, 'bin');
        const obj = path.resolve(dir, 'obj');
        const testResults = path.resolve(dir, 'TestResults');
        
        if(fs.existsSync(bin))
            rmdir(bin);
        
        if(fs.existsSync(obj))
            rmdir(obj);

        if(fs.existsSync(testResults))
            rmdir(testResults);
    });
}

function copyProjectArchivesToTempFolder() {
    return new Promise((resolve, reject) => {
        const async = require('async');
        const fs = require('fs-extra');
        const path = require('path');

        let archives = getProjectsArchivesPath();
        let destFolder = path.resolve('.', `.${PROJECT_NAME.toLowerCase()}`)

        async.each(archives, 
                (src, callback) => { 
                    let pathArr = src.split(path.sep);
                    let dest = path.resolve(destFolder, pathArr[pathArr.length - 1]);
                    fs.copy(src, dest, callback)
                }, 
                (err) => {
                    if(err)
                        reject(err);
                    else
                        resolve(destFolder)
                }
            )
    });
}

let rmdir = function(dir) {
    let fs = require("fs");
    let path = require("path");
	let list = fs.readdirSync(dir);
	for(let i = 0; i < list.length; i++) {
		let filename = path.join(dir, list[i]);
		let stat = fs.statSync(filename);
		
		if(filename == "." || filename == "..") {
			// ignoro
		} else if(stat.isDirectory()) {
			rmdir(filename);
		} else {
			fs.unlinkSync(filename);
		}
	}
	fs.rmdirSync(dir);
};

function getComposeFilePath(checkIfExists) {
    const filePath = path.resolve('..', 'docker-compose.yaml');
    
    if(checkIfExists && !fs.existsSync(filePath))
        throw new Error('Arquivo docker-compose.yml não encontrado.');

    return filePath;
}

function readCompose() {
    return yaml.safeLoad(fs.readFileSync(getComposeFilePath()));
}

function getVersionFromCompose() {
    const split = readCompose().services.web.image.split(':');
    return split[split.length-1];
} 
if ($args.count -lt 3){
	echo "Quantidade de argumentos invalida."
	exit 1
}

$tipo = $args[0]
$plataforma = $args[1]
$resolvernginx = $args[2]

if($tipo -ne "Compactar" -and $tipo -ne "Publicar") {
	Write-Output "Tipo informado inválido. Informe 'Compactar' ou 'Publicar'."
	exit 1
}

if ($plataforma.length -eq 0) {
	Write-Output "Por favor, informe a plataforma."
	exit 1
}

if($plataforma -eq "Windows") {
	$plataforma = "win"
}
elseif ($plataforma -eq "Linux")
{
    $plataforma = "linux"
}
else {
	$plataforma = "any"
}

function Build {
    $comando = "cd ../client && npm install && cd ../build && npm install && npm run build -- --platform $($plataforma)"
    Return cmd.exe /c $comando
}

function DeployDocker {
    $comando = "cd .. && npm run resolve-dependencias && cd build && npm run deploy -- --platform $($plataforma) $(if ($resolvernginx -eq "true") { "--resolvenginx" })"
    Return cmd.exe /c $comando
}


if ($tipo -eq "Compactar") {
    Build
}
else {
    DeployDocker
}
Exit $LASTEXITCODE

--Comandos iniciais:

INSERT INTO alterdatavotador.departamento(
            empresa_id, codigo, nome, data_inativacao)
    VALUES 
     (1, '0123', 'Desenvolvimento ERP', null),
     (1, '0124', 'Desenvolvimento Pack', null),
     (1, '0125', 'Desenvolvimento Shop', null),
     (1, '0126', 'Desenvolvimento Immobile', null);

	 
INSERT INTO alterdatavotador.empresa(
            codigo, nome, data_inativacao)
    VALUES ('00205', 'ALTERDATA SOFTWARE', null),
	   ('00301', 'ALTERDATA TECNOLOGIA', null),
	   ('00231', 'ALTERDATA DESENVOLVIMENTO', null);
	   
INSERT INTO alterdatavotador.funcao(
            codigo, nome, data_inativacao)
    VALUES ('0011', 'PROGRAMADOR B9', null),
	   ('0012', 'PROGRAMADOR C1', null),
	   ('0013', 'PROGRAMADOR C9', null),
	   ('0014', 'INSPETOR DE QUALIDADE C9', null);
	   
	   
INSERT INTO alterdatavotador.funcionario(
            empresa_id, departamento_id, funcao_id, nome, cpf, pis, email, 
            ramal, data_inativacao, guid_ad)
    VALUES 
    (1, 1, 1, 'weberson.dsn.erp', '12345678901', '12345678901', 'weberson.dsn.erp@alterdata.com.br', '4852', null, '02308209-4c8e-462c-af84-f590868d618e'),
    (1, 2, 2, 'leonardo.sqa', '12345670001', '12345611101', 'leonardo@alterdata.com.br', '4853', null, '52526ba4-7ec2-400d-a159-18ad9087e34f');

INSERT INTO alterdatavotador.perfil(
            descricao)
    VALUES ('administrador'),
	   ('padrao');
	   
INSERT INTO alterdatavotador.recurso(
            codigo_processo, nome_produto, descricao, detalhes, data_abertura, 
            data_alteracao, status)
    VALUES ('000123456', 'Faturamento', 'Descricção do processo de teste', 'Detalhes do processo ...', '2019-05-21 13:35:00', null, 0),
	   ('000654321', 'Disponível', 'Descricção do processo', 'Detalhes do processo ... texto e mais texto ...', '2019-04-12 12:22:00', null, 0),
	   ('000666622', 'Disponível', 'Permitir integração com sistema XYZ', 'Detalhes ... texto e mais texto ...', '2019-03-10 07:11:00', null, 0),
	   ('000776622', 'Estoque', 'Alinhar estoque automaticamente', 'Detalhes ... texto e mais texto ...', '2019-05-10 08:39:00', null, 0);

INSERT INTO alterdatavotador.usuario(
            login, guid, perfil_id, funcionario_id)
    VALUES ('weberson.dsn.erp', 'c869f475-6512-423d-8f61-61f8d2131b51', 1, 1),
           ('leonardo.sqa', 'e123fcbb-39fb-4365-83a8-f6e374cf4a11', 1, 2);
           

INSERT INTO alterdatavotador.voto(
            data_realizado, funcionario_id, recurso_id, comentario)
    VALUES ('2019-05-10 08:39:00', 1, 1,'Comentário teste recurso 1'),
	   ('2019-05-11 10:32:00', 1, 2,'Comentário teste recurso 2'),
	   ('2019-05-16 11:12:00', 2, 1,'Comentário teste outro funcionario recurso 1'),
	   ('2019-05-16 11:12:00', 2, 2,'Comentário teste outro funcionario recurso 2'),
	   ('2019-05-16 11:12:00', 2, 3,'Comentário teste outro funcionario recurso 3');		  

-- inserir usuarios com base nos cadastros da API
INSERT INTO alterdatavotador.usuario(
            login, guid, perfil_id, funcionario_id)
    VALUES ('weberson.dsn.erp', '52cdda20-b2d6-4a30-b704-feb7042aa73f',1, 1),
	    ('bernardo.dsn.erp', '6aec4b7f-4a58-4b15-83fe-a2e5145c5562',1, 2),
	    ('castro.dsn.erp', 'f72bcefe-c3e5-4ec2-a725-4b3d9688e7cb',1, 3),
	    ('danniel.dsn.erp', 'a877a436-683a-4234-84f9-6b38259bc271',1, 4); 
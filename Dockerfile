FROM microsoft/aspnetcore-build:2.0 AS build-env
WORKDIR /app

COPY AlterdataVotador.sln ./
COPY AlterdataVotador/AlterdataVotador.csproj ./AlterdataVotador/
COPY AlterdataVotador.Infra/AlterdataVotador.Infra.csproj ./AlterdataVotador.Infra/
COPY AlterdataVotador.Migrations/AlterdataVotador.Migrations.csproj ./AlterdataVotador.Migrations/
COPY AlterdataVotador.TestAlterdataVotador.Test.csproj ./AlterdataVotador.Test/

RUN dotnet restore

COPY . ./
RUN cd AlterdataVotador && dotnet publish -c Release -o out

FROM microsoft/aspnetcore:2.0
WORKDIR /app
COPY --from=build-env /app/AlterdataVotador/out .
ENTRYPOINT ["dotnet", "AlterdataVotador.dll"]


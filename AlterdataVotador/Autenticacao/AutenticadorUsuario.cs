namespace AlterdataVotador.Autenticacao
{
    using Nancy;
    using Nancy.Authentication.Forms;
    using Nancy.Security;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using AlterdataVotador.Dominio.Entidades;
    using AlterdataVotador.Dominio.Entidades.Classes;
    using AlterdataVotador.Dominio.Repositorio;
    using AlterdataVotador.Dominio.Repositorio.Classes;
    using AlterdataVotador.Dominio.Repositorio.Interfaces;
    using Serilog;
    using AlterdataVotador.Autenticacao;

    /// <summary>
    /// Classe para autenticar o usuário
    /// </summary>
    public class AutenticadorUsuario : IAutenticador, IUserMapper
    {
        private ILogger Log;
        private IList<Sessao> _sessoes = new List<Sessao>();
        private IRepositorioUsuario RepositorioUsuario;



        /// <summary>
        /// Construtor da classe de autenticador do usuário
        /// </summary>
        /// <param name="repUsuario">Reposiório do usuário</param>
        /// <param name="log">Classe de log para efetuar a gravação de logs do sistema</param>
        public AutenticadorUsuario(IRepositorioUsuario repUsuario, ILogger log) 
        {
            RepositorioUsuario = repUsuario;
            Log = log;
        }



        /// <summary>
        /// Método utilizado pelo nancy para autenticar o usuário. Método chamado automaticamente pelo Nancy na rotina de autenticação
        /// </summary>
        /// <param name="identifier">Identificação do usuário, aqui passar o GUID do usuário</param>
        /// <param name="context"></param>
        /// <returns>Retorna um ClaimsPrincipal</returns>
        public ClaimsPrincipal GetUserFromIdentifier(Guid identifier, NancyContext context)
        {
            Sessao sessao = null;
            try
            {
                sessao = this.Obter(identifier);
            }
            catch (AutenticacaoException)
            {
                return null;
            }

            return new ClaimsPrincipal(sessao.Usuario);
        }

        /// <summary>
        /// Validar sessão do usuário. Verificar se a sessão está expirada e remove a sessão
        /// </summary>
        /// <param name="sessao">Sessão do usuário contendo o identificador GUID, o Usuário e a data de expiração</param>
        private void ValidarSessao(Sessao sessao)
        {
            if (sessao == null)
                throw new AutenticacaoException("Usuário não autenticado.");

            if (sessao.Expirada)
            {
                Deslogar(sessao);
                throw new AutenticacaoException("Usuário com sessão expirada.");
            }
        }



        /// <summary>
        /// Obter sessão por identificador GUID
        /// </summary>
        /// <param name="guid">Identificador GUID</param>
        /// <returns>Um objeto de Sessao</returns>
        /// 
        public Sessao Obter(Guid guid)
        {
            var sessao = _sessoes.FirstOrDefault(s => s.Usuario.Guid.Equals(guid));
            ValidarSessao(sessao);
            return sessao;
        }



        /// <summary>
        /// Obter a sessão do login
        /// </summary>
        /// <param name="login">Login do usuário</param>
        /// <returns>Retorna um objeto de Sessao</returns>
        public Sessao Obter(string login)
        {
            var sessao = _sessoes.FirstOrDefault(s => s.Usuario.Login.Equals(login.ToUpper()));
            ValidarSessao(sessao);
            return sessao;
        }

        /// <summary>
        /// Adiciona uma sessõo para o usuário e adiciona na lista de sessões ativas
        /// </summary>
        /// <param name="usuario">Usuário logado</param>
        /// <param name="expires">Data de expiração</param>
        /// <returns></returns>
        public Guid Logar(Usuario usuario, DateTime expires)
        {
            var sessao = new Sessao(Guid.NewGuid(), usuario, expires);
            usuario.Guid = sessao.Guid;
            _sessoes.Add(sessao);
            return sessao.Guid;
        }


        /// <summary>
        /// Remove a sessão do usuário da lista de Sessões para deslogar o usuário
        /// </summary>
        /// <param name="sessao"></param>
        public void Deslogar(Sessao sessao)
        {
            try
            {
                _sessoes.Remove(sessao);
            }
            catch (AutenticacaoException)
            {
                // Sessão não existe, logo, não é necessário fazer nada.
            }
        }

        /// <summary>
        /// Torina utilizada para remover as sessões expiradas de tempo em tempos
        /// </summary>
        public void RemoverSessoesExpiradas()
        {
            var sessoesExpiradas = _sessoes.Where(s => s.DataExpiracao <= DateTime.Now);

            foreach (var sessao in sessoesExpiradas)
            {
                Deslogar(sessao);
            }
        }
    }
}


namespace AlterdataVotador.Autenticacao
{
    using System;
    using Novell.Directory.Ldap;

    /// <summary>
    /// Classe utilizada para autenticação com o LDAP
    /// </summary>
    public class AutenticadorLDAP : IAutenticadorLDAP
    {
        /// <summary>
        /// Método para autenticar o usuário na Rede
        /// </summary>
        /// <param name="usuario">Login ou email do usuário na Rede</param>
        /// <param name="senha">Senha de rede</param>
        /// <returns></returns>
        public bool Autenticar(string usuario, string senha)
        {
            usuario = usuario.ToLower().Replace("@alterdata.com.br", "");

            if (string.IsNullOrEmpty(senha))
                return false;

            var ldapConn = new LdapConnection();
            ldapConn.Connect("nt07", 389);

            try
            {
                ldapConn.Bind("alterdata\\" + usuario, senha);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
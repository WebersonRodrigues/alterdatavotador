namespace AlterdataVotador.Autenticacao
{
    using System;
    using AlterdataVotador.Dominio.Entidades.Classes;

    /// <summary>
    /// Interface para fazer o contrato do Autenticador
    /// </summary>
    public interface IAutenticador
    {
        /// <summary>
        /// Obter sessão pdo usuário pelo Identificador
        /// </summary>
        /// <param name="guid">Identificador GUID</param>
        /// <returns>Sessao</returns>
        Sessao Obter(Guid guid);

        /// <summary>
        /// Obter sessão pdo usuário pelo login do usuário
        /// </summary>
        /// <param name="login">Login do usuário</param>
        /// <returns>Sessao</returns>
        Sessao Obter(string login);

        /// <summary>
        /// Adicionar o usuário na lista de sessoes ativas, logar no sistema
        /// </summary>
        /// <param name="usuario">Usuário que esta acessando o sistema</param>
        /// <param name="expires">Data de expiração da autenticação</param>
        /// <returns>Guid, identificador do usuário</returns>
        Guid Logar(Usuario usuario, DateTime expires);

        /// <summary>
        /// Remove o usuário da lista de sessoes ativas
        /// </summary>
        /// <param name="sessao">Objeto de sessao do usuário</param>
        void Deslogar(Sessao sessao);

        /// <summary>
        /// Remove todas as sessoes expiradas em um timer de tempo
        /// </summary>
        void RemoverSessoesExpiradas();
    }
}

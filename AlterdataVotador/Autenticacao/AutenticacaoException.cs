namespace AlterdataVotador.Autenticacao
{
    using System;
    /// <summary>
    /// Classe de excepiton utilizada para autenticação
    /// </summary>
    public class AutenticacaoException : Exception
    {
        
        /// <summary>
        /// Exception para autenticação
        /// </summary>
        /// <param name="message">Mensagem que será apresentada quando a exceção for lançada.</param>
        /// <returns>Throw</returns>
        public AutenticacaoException(string message) : base(message) { }
    }
}

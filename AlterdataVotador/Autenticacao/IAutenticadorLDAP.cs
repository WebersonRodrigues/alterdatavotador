namespace AlterdataVotador.Autenticacao
{
    /// <summary>
    /// Interface de contrado do autenticador com o LDAP
    /// </summary>
    public interface IAutenticadorLDAP
    {
        /// <summary>
        /// Autenticar o usuário no LDAP
        /// </summary>
        /// <param name="usuario">Nome ou login do usuário de acesso ao servidor de LDAP</param>
        /// <param name="senha">Senha de acesso de usuário ao servidor de LDAP</param>
        /// <returns>Retorna true ou false de acordo com as credenciasis do usuário informado</returns>
        bool Autenticar(string usuario, string senha);
    }
}
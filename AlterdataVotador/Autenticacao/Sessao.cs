namespace AlterdataVotador.Autenticacao
{
    using System;
    using AlterdataVotador.Dominio.Entidades.Classes;

    /// <summary>
    /// Classe responsável pelas sessao
    /// </summary>
    public class Sessao
    {
        /// <summary>
        /// Identificador da sessão
        /// </summary>
        public Guid Guid { get; private set; }

        /// <summary>
        /// Usuario da sessão
        /// </summary>
        public Usuario Usuario { get; private set; }

        /// <summary>
        /// Data da expiração da sessão
        /// </summary>
        public DateTime DataExpiracao { get; set; }

        /// <summary>
        /// Retorna true ou false de acordo com a data expiranção
        /// </summary>
        public bool Expirada => this.DataExpiracao <= DateTime.Now;

        /// <summary>
        /// Construtor do objeto sessao
        /// </summary>
        /// <param name="guid">Identificador do usuário</param>
        /// <param name="usuario">Usuário da sessão</param>
        /// <param name="dataExpiração">Data da expiração da sessão</param>
        public Sessao(Guid guid, Usuario usuario, DateTime dataExpiração)
        {
            this.Guid = guid;
            this.Usuario = usuario;
            this.DataExpiracao = dataExpiração;
        }

        /// <summary>
        /// Sobrescrevendo o metodo base Equal para verificar se as sessões são iguais
        /// </summary>
        /// <param name="obj">Sessão do usuário</param>
        /// <returns>true ou false</returns>
        public override bool Equals(object obj)
        {
            var sessaoTemp = obj as Sessao;
            if (sessaoTemp == null)
                return false;


            return (sessaoTemp.Usuario.Equals(this.Usuario) && sessaoTemp.Guid == this.Guid && sessaoTemp.DataExpiracao == this.DataExpiracao);
        }

        /// <summary>
        /// Gera um Hashcode baseado no Guid da sessão
        /// </summary>
        /// <returns>Retorna um rashcode do tipo inteiro</returns>
        public override int GetHashCode()
        {
            return Guid.GetHashCode();
        }
    }
}
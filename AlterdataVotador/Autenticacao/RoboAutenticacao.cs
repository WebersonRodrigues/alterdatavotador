namespace AlterdataVotador.Autenticacao
{
    using System;
    using System.Timers;
    
    /// <summary>
    /// Classe do Robo de autenticação que fica verificando de tempos em tempos as sessões expiradas
    /// </summary>
    public class RoboAutenticacao
    {
        /// <summary>
        /// timer da classe
        /// </summary>
        private Timer timer;

        /// <summary>
        /// Objeto autenticador da classe
        /// </summary>
        private IAutenticador Autenticador {get; set;}

        /// <summary>
        /// Contrutor da classe do robo de autenticação
        /// </summary>
        /// <param name="autenticador">autenticador utilizado pelo robo</param>
        public RoboAutenticacao(IAutenticador autenticador)
        {
            this.Autenticador = autenticador;
            InicitarTempoDeAgendamento();
        }

        /// <summary>
        /// Metodo que inicia o tempo de agendamento para o robo remover as sessoes expiradas
        /// </summary>
        private void InicitarTempoDeAgendamento()
        {
            timer = new Timer();
            timer.Interval = new TimeSpan(2, 0, 0).TotalMilliseconds;
            timer.Elapsed += (s, e) =>
            {
                Autenticador.RemoverSessoesExpiradas();
            };
        }

        /// <summary>
        ///Metodo que inicia o robo de autenticação 
        /// </summary>
        public void IniciarRoboAutenticacao()
        {
            timer.Start();
        }

    }
}

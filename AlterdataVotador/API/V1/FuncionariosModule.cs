namespace AlterdataVotador.API.V1
{
    using AlterdataVotador.Autenticacao;
    using AlterdataVotador.Dominio.Repositorio.Interfaces;
    using AlterdataVotador.Dominio.Entidades.Classes;
    using AlterdataVotador.Servicos.Interfaces;
    using Nancy;
    using Nancy.Extensions;
    using Nancy.IO;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using System;

    /// <summary>
    /// Classe utilizada para API de funcionários
    /// </summary>
    public class FuncionarioModule : NancyModule
    {
        /// <summary>
        /// Construtor do Modulo de Funcionario que é chamado automaticamente pelo Nancy nas requisições HTTP.
        /// </summary>
        /// <param name="autenticador">Autenticador do usuário</param>
        /// <param name="repFuncionario">Repositório de Funcionario</param>
        /// <param name="repDepartamento">Repositório de Departamento</param>
        /// <param name="repEmpresa">Repositório de Empresa</param>
        /// <param name="repFuncao">Repositorio de Função</param>
        /// <param name="servicoRota">Servico de roteamento</param>
        /// <returns></returns>
        public FuncionarioModule(IAutenticador autenticador,
                                IRepositorioFuncionario repFuncionario,
                                IRepositorioDepartamento repDepartamento,
                                IRepositorioEmpresa repEmpresa,
                                IRepositorioFuncao repFuncao,
                                IServicoRota servicoRota) : base("/api/v1/")
        {
            Get("funcionarios", _ =>
            {
                var funcionarios = repFuncionario.ObterTodos();

                return servicoRota.CriarJsonResponse(funcionarios)
                                  .WithStatusCode(HttpStatusCode.OK);
            });

            Post("funcionarios", _ =>
            {
                var jArr = JArray.Parse(RequestStream.FromStream(Request.Body).AsString());
                try
                {
                    foreach (var item in jArr)
                    {
                        repFuncionario.Inserir(ConstruirFuncionario(item, repDepartamento, repFuncao, repEmpresa));
                    }

                    return servicoRota.CriarJsonResponse(new { mensagem = "Funcionarios cadastrados com sucesso!" })
                                      .WithStatusCode(HttpStatusCode.OK);
                }
                catch(ArgumentException e)
                {
                    return servicoRota.CriarJsonResponse(new { erro = e.Message })
                                      .WithStatusCode(HttpStatusCode.BadRequest);
                }
                catch (Exception )
                {
                    return servicoRota.CriarJsonResponse(new { erro = "Falha ao cadastrar funcionarios, confirme se os parametros informados estão corretos." })
                                      .WithStatusCode(HttpStatusCode.InternalServerError);
                }
            });

            Put("funcionarios", _ =>
            {
                var jArr = JArray.Parse(RequestStream.FromStream(Request.Body).AsString());
                try
                {
                    foreach (var item in jArr)
                    {
                        var funcionario = ConstruirFuncionario(item, repDepartamento, repFuncao, repEmpresa);
                        var funcionarioParaAtualizar = repFuncionario.Obter(funcionario.Id);

                        if (funcionarioParaAtualizar == null)
                        {
                            return servicoRota.CriarJsonResponse(new { erro = $"Não foi possível atualizar o funcionário, motivo: não encontrado. {funcionario.ToString()}" })
                                      .WithStatusCode(HttpStatusCode.BadRequest);
                        }

                        repFuncionario.Atualizar(funcionario);
                    }

                    return servicoRota.CriarJsonResponse(new { mensagem = "Funcionarios atualizados com sucesso!" })
                                      .WithStatusCode(HttpStatusCode.OK);
                }
                catch(ArgumentException e)
                {
                    return servicoRota.CriarJsonResponse(new { erro = e.Message })
                                      .WithStatusCode(HttpStatusCode.BadRequest);
                }
                catch (Exception)
                {
                    return servicoRota.CriarJsonResponse(new { erro = "Falha ao atualizar funcionarios, confirme se os parametros informados estão corretos." })
                                      .WithStatusCode(HttpStatusCode.BadRequest);
                }
            });

            Get("funcionarios/departamentos", _ =>
            {
                var departamentos = repDepartamento.ObterTodos();
                return servicoRota.CriarJsonResponse(departamentos
                )
                                      .WithStatusCode(HttpStatusCode.OK);
            });

            Post("funcionarios/departamentos", _ =>
            {
                var jArr = JArray.Parse(RequestStream.FromStream(Request.Body).AsString());
                try
                {
                    foreach (var item in jArr)
                    {
                        repDepartamento.Inserir(ConstruirDepartamento(item, repDepartamento));
                    }

                    return servicoRota.CriarJsonResponse(new { mensagem = "Departamentos cadastrados com sucesso!" })
                                      .WithStatusCode(HttpStatusCode.OK);
                }
                catch(ArgumentException e)
                {
                    return servicoRota.CriarJsonResponse(new { erro = e.Message })
                                      .WithStatusCode(HttpStatusCode.BadRequest);
                }
                catch (Exception)
                {
                    return servicoRota.CriarJsonResponse(new { erro = $"Falha ao cadastrar os departamentos." })
                                      .WithStatusCode(HttpStatusCode.InternalServerError);
                }
            });

            Put("funcionarios/departamentos", _ =>
            {
                var jArr = JArray.Parse(RequestStream.FromStream(Request.Body).AsString());
                try
                {
                    foreach (var item in jArr)
                    {
                        var departamentoParaAtualizar = ConstruirDepartamento(item, repDepartamento);
                        var departamentoAtual = repDepartamento.Obter(departamentoParaAtualizar.Id);

                        if (departamentoAtual == null)
                        {
                            return servicoRota.CriarJsonResponse(
                                new { erro = $"Não foi possível atualizar o departamento, o mesmo não está cadastrado. Departamento {departamentoParaAtualizar}" })
                                      .WithStatusCode(HttpStatusCode.BadRequest);
                        }
                        repDepartamento.Atualizar(departamentoParaAtualizar);
                    }

                    return servicoRota.CriarJsonResponse(new { mensagem = "Departamentos cadastrados com sucesso!" })
                                      .WithStatusCode(HttpStatusCode.OK);
                }
                catch(ArgumentException e)
                {
                    return servicoRota.CriarJsonResponse(new { erro = e.Message })
                                      .WithStatusCode(HttpStatusCode.BadRequest);
                }
                catch (Exception)
                {
                    return servicoRota.CriarJsonResponse(new { erro = $"Falha ao cadastrar os departamentos." })
                                      .WithStatusCode(HttpStatusCode.InternalServerError);
                }
            });

            Get("funcionarios/empresas", _ =>
            {
                var empresas = repEmpresa.ObterTodos();
                return servicoRota.CriarJsonResponse(empresas)
                                      .WithStatusCode(HttpStatusCode.OK);
            });

            Post("funcionarios/empresas", _ =>
            {
                var jArr = JArray.Parse(RequestStream.FromStream(Request.Body).AsString());
                try
                {
                    foreach (var item in jArr)
                    {
                        repEmpresa.Inserir(ConstruirEmpresa(item, repEmpresa));
                    }

                    return servicoRota.CriarJsonResponse(new { mensagem = "Empresas cadastradas com sucesso!" })
                                      .WithStatusCode(HttpStatusCode.OK);
                }
                catch(ArgumentException e)
                {
                    return servicoRota.CriarJsonResponse(new { erro = e.Message })
                                      .WithStatusCode(HttpStatusCode.BadRequest);
                }
                catch (Exception)
                {
                    return servicoRota.CriarJsonResponse(new { erro = $"Falha ao cadastrar as empresas" })
                                      .WithStatusCode(HttpStatusCode.InternalServerError);
                }
            });

            Put("funcionarios/empresas", _ =>
            {
                var jArr = JArray.Parse(RequestStream.FromStream(Request.Body).AsString());
                try
                {
                    foreach (var item in jArr)
                    {
                        var empresaParaAtualizar = ConstruirEmpresa(item, repEmpresa);
                        if (empresaParaAtualizar == null)
                        {
                            return servicoRota.CriarJsonResponse(
                                new { erro = $"Não foi possível atualizar a empresa, o mesma não está cadastrada. Empresa: {empresaParaAtualizar}" })
                                      .WithStatusCode(HttpStatusCode.BadRequest);
                        }
                        repEmpresa.Atualizar(empresaParaAtualizar);
                    }

                    return servicoRota.CriarJsonResponse(new { mensagem = "Empresas atualizadas com sucesso!" })
                                         .WithStatusCode(HttpStatusCode.OK);
                }
                catch(ArgumentException e)
                {
                    return servicoRota.CriarJsonResponse(new { erro = e.Message })
                                      .WithStatusCode(HttpStatusCode.BadRequest);
                }
                catch (Exception)
                {
                    return servicoRota.CriarJsonResponse(new { erro = $"Falha ao atualizar as empresas." })
                                      .WithStatusCode(HttpStatusCode.InternalServerError);
                }
            });

            Get("funcionarios/funcoes", _ =>
            {
                var funcoes = repFuncao.ObterTodos();
                return servicoRota.CriarJsonResponse(funcoes)
                                      .WithStatusCode(HttpStatusCode.OK);
            });

            Post("funcionarios/funcoes", _ =>
            {
                var jArr = JArray.Parse(RequestStream.FromStream(Request.Body).AsString());
                try
                {
                    foreach (var item in jArr)
                    {
                        repFuncao.Inserir(ConstruirFuncao(item));
                    }

                    return servicoRota.CriarJsonResponse(new { mensagem = "Funcões cadastradas com sucesso!" })
                                      .WithStatusCode(HttpStatusCode.OK);
                }
                catch(ArgumentException e)
                {
                    return servicoRota.CriarJsonResponse(new { erro = e.Message })
                                      .WithStatusCode(HttpStatusCode.BadRequest);
                }
                catch (Exception )
                {
                    return servicoRota.CriarJsonResponse(new { erro = $"Falha ao cadastrar as funções" })
                                      .WithStatusCode(HttpStatusCode.InternalServerError);
                }
            });

            Put("funcionarios/funcoes", _ =>
            {
                var jArr = JArray.Parse(RequestStream.FromStream(Request.Body).AsString());
                try
                {
                    foreach (var item in jArr)
                    {

                        var funcaoAtualizada = ConstruirFuncao(item);

                        if(funcaoAtualizada == null)
                        {
                            return servicoRota.CriarJsonResponse(
                                new { erro = $"Não foi possível atualizar a funcão, o mesma não está cadastrada. Funcão: {funcaoAtualizada}" })
                                      .WithStatusCode(HttpStatusCode.BadRequest);
                        }

                        repFuncao.Inserir(funcaoAtualizada);
                    }

                    return servicoRota.CriarJsonResponse(new { mensagem = "Funcões atualziadas com sucesso!" })
                                      .WithStatusCode(HttpStatusCode.OK);
                }
                catch(ArgumentException e)
                {
                    return servicoRota.CriarJsonResponse(new { erro = e.Message })
                                      .WithStatusCode(HttpStatusCode.BadRequest);
                }
                catch (Exception)
                {
                    return servicoRota.CriarJsonResponse(new { erro = $"Falha ao cadastrar as funcções" })
                                      .WithStatusCode(HttpStatusCode.InternalServerError);
                }
            });
        }



        /// <summary>
        /// Método para construir funcao com base nos dados obtidos da API na requisição
        /// </summary>
        /// <param name="item"> Funcao recuperada da requisição</param>
        /// <returns>Funcao</returns>
        private Funcao ConstruirFuncao(JToken item)
        {
            try
            {
                return new Funcao((long)item["id"],
                    (string)item["codigo"],
                    (string)item["nome"],
                    (DateTime?)item["dataInativacao"]);
            }
            catch (ArgumentException)
            {
                throw new ArgumentException($"Falha ao tentar cadastrar a função Id:{(long)item["id"]}, confirme os parãmetros.");
            }

        }

        /// <summary>
        /// Método para construir empresa com base nos dados obtidos da API na requisição
        /// </summary>
        /// <param name="item"> Empresa recuperado da requisição</param>
        /// <param name="repEmpresa">Repositório da empresa</param>
        /// <returns></returns>
        private Empresa ConstruirEmpresa(JToken item, IRepositorioEmpresa repEmpresa)
        {
            try
            {
                return new Empresa((long)item["id"],
                    (string)item["codigo"],
                    (string)item["nome"],
                    (DateTime?)item["dataInativacao"]);
            }
            catch (ArgumentException)
            {
                throw new ArgumentException($"Falha ao tentar cadastrar a empresa Id:{(long)item["id"]}, confirme os parãmetros.");
            }

        }

        /// <summary>
        /// Método para construir departamento com base nos dados obtidos da API na requisição
        /// </summary>
        /// <param name="item">Departamento recuperado da requisição</param>
        /// <param name="repDepartamento">Repositório de departamento</param>
        /// <returns></returns>
        private Departamento ConstruirDepartamento(JToken item, IRepositorioDepartamento repDepartamento)
        {
            try
            {
                return new Departamento((long)item["id"],
                                    (string)item["codigo"],
                                     (string)item["nome"],
                                    (DateTime?)item["dataInativacao"]);
            }
            catch (ArgumentException)
            {
                throw new ArgumentException("Os parametros para o departamento estão incorretos.");
            }

        }

        /// <summary>
        /// Método para construir funcionarios com base nos dados obtidos da API na requisição
        /// </summary>
        /// <param name="item">Funcionário recuperado na requisição</param>
        /// <param name="repDepartamento">Repositório de departamento</param>
        /// <param name="repFuncao">Repositório de função</param>
        /// <param name="repEmpresa">Repositório de empresa</param>
        /// <returns></returns>
        private Funcionario ConstruirFuncionario(JToken item,
        IRepositorioDepartamento repDepartamento,
        IRepositorioFuncao repFuncao,
        IRepositorioEmpresa repEmpresa)
        {
            try
            {
                var funcionarioId = (Convert.ToString(item["id"]) != null) ? (long)item["id"] : 0;

                var departamento = repDepartamento.Obter((long)item["empresa"]["id"]);
                if (departamento == null)
                    throw new ArgumentException($"Departamento não encontrado, confirme se o departamento Id:{departamento.Id} já está cadastrado.");

                var funcao = repFuncao.Obter((long)item["funcao"]["id"]);
                if (funcao == null)
                    throw new ArgumentException($"Função não encontrada, confirme se a função Id:{funcao.Id} já está cadastrada.");

                var empresa = repEmpresa.Obter((long)item["empresa"]["id"]);
                if (empresa == null)
                    throw new ArgumentException($"Empresa não encontrada, confirme se a empresa Id:{funcao.Id} já está cadastrada.");

                return new Funcionario(funcionarioId,
                                    item["nome"].ToString(),
                                    item["cpf"].ToString(),
                                    item["email"].ToString(),
                                    empresa,
                                    departamento,
                                    funcao,
                                    item["ramal"].ToString(),
                                    (DateTime?)item["dataInativacao"],
                                    new Guid(item["guidAd"].ToString()));


            }
            catch (ArgumentException e)
            {
                throw new ArgumentException($"Falha ao cadastrar/atualizar o funcionario Id {(long)item["id"]}, erro: {e.Message}");
            }
        }
    }
}
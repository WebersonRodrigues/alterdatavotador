namespace AlterdataVotador.API.V1
{    
    using AlterdataVotador.Dominio.Entidades.Classes;
    using AlterdataVotador.Dominio.Repositorio.Interfaces;
    using AlterdataVotador.Servicos.Interfaces;
    using Nancy;
    using Nancy.Extensions;
    using Nancy.IO;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using System;

    
    /// <summary>
    /// Classe utilizada pela API para requisições de Recursos. Essa classe é utilizada pelo Nancy
    /// </summary>
    public class RecursosModule : NancyModule
    {
        /// <summary>
        /// Construtor da classe de Recursos utilizada pela API. Está classe é construida automaticamente pelo Nancy
        /// </summary>
        /// <param name="repRecurso">Repositório de Recurso</param>
        /// <param name="servicoRota">Serviço de Roteamento</param>
        /// <returns></returns>
        public RecursosModule(  IRepositorioRecurso repRecurso,
                                IServicoRota servicoRota) : base("/api/v1/")
        {         
            Get("recursos", _ =>
            {
                var recursos = repRecurso.ObterTodos();
                return servicoRota.CriarJsonResponse(recursos)
                                      .WithStatusCode(HttpStatusCode.OK);
            });

            Post("recursos", _ =>
            {
                var jArr = JArray.Parse(RequestStream.FromStream(Request.Body).AsString());
                try
                {
                    foreach (var item in jArr)
                    {
                        repRecurso.Inserir(ConstruirRecurso(item, repRecurso));
                    }

                    return servicoRota.CriarJsonResponse(new { mensagem = "Recursos cadastrados com sucesso!" })
                                      .WithStatusCode(HttpStatusCode.OK);
                }
                catch(ArgumentException e)
                {
                    return servicoRota.CriarJsonResponse(new { erro = e.Message })
                                      .WithStatusCode(HttpStatusCode.BadRequest);
                }
                catch (Exception)
                {
                    return servicoRota.CriarJsonResponse(new { erro = "Falha ao cadastrar recursos, confirme se os parametros informados estão corretos." })
                                      .WithStatusCode(HttpStatusCode.InternalServerError);
                }
            });

            Put("recursos", _ =>
            {
                var jArr = JArray.Parse(RequestStream.FromStream(Request.Body).AsString());
                try
                {
                    foreach (var item in jArr)
                    {
                        var recursoParaAtualizar = ConstruirRecurso(item, repRecurso);
                        var recursoAtual =repRecurso.Obter(recursoParaAtualizar.Id);

                        if(recursoAtual == null)
                        {
                            return servicoRota.CriarJsonResponse(
                                new { erro = $"Não foi possível atualizar o recurso, o mesmo não está cadastrado. Recurso: {recursoParaAtualizar}" })
                                      .WithStatusCode(HttpStatusCode.BadRequest);
                        }
                        repRecurso.Atualizar(recursoParaAtualizar);
                    }

                    return servicoRota.CriarJsonResponse(new { mensagem = "Recursos atualizados com sucesso!" })
                                      .WithStatusCode(HttpStatusCode.OK);
                }
                catch(ArgumentException e)
                {
                    return servicoRota.CriarJsonResponse(new { erro = e.Message })
                                      .WithStatusCode(HttpStatusCode.BadRequest);
                }
                catch (Exception)
                {
                    return servicoRota.CriarJsonResponse(new { erro = "Falha ao cadastrar recursos, confirme se os parametros informados estão corretos." })
                                      .WithStatusCode(HttpStatusCode.InternalServerError);
                }
            });

        }

        /// <summary>
        /// Método para construir recurso com base no objeto Recurso da recuperado na requisição da API
        /// </summary>
        /// <param name="item">Recurso obtido na requisição</param>
        /// <param name="repRecurso">Repositório de recursos</param>
        /// <returns></returns>
        private Recurso ConstruirRecurso(JToken item, IRepositorioRecurso repRecurso)
        {
            try
            {
                return new Recurso((long)item["id"],
                                    (string)item["codigoProcesso"],
                                    (string)item["nomeProduto"],
                                    (string)item["descricao"],
                                    (string)item["detalhes"],
                                    Convert.ToDateTime(item["dataAbertura"]),
                                    Status.Aberto,
                                    (DateTime?)item["dataAlteracao"]);
            }
            catch (ArgumentException)
            {
                throw new ArgumentException($"Falha ao construir o recurso Id{(long)item["id"]}, confirme se os parãmetros estão corretos.");
            }
        }
    }
}
namespace AlterdataVotador.API.V1
{
    using AlterdataVotador.Dominio.Repositorio.Interfaces;
    using AlterdataVotador.Servicos.Interfaces;
    using Nancy;
    
    
    /// <summary>
    /// Classe utilizada para API de Votos
    /// </summary>
    public class VotosModule : NancyModule
    {
        /// <summary>
        /// Construtor do Modulo de Votos utilizado pela API. Esse módulo é chamado automaticamente pelo nancy quando a API
        /// </summary>
        /// <param name="repVoto">Repositório de Voto</param>
        /// <param name="servicoRota">Serviço de reteamento</param>
        /// <returns></returns>
        public VotosModule(IRepositorioVoto repVoto,
                           IServicoRota servicoRota): base("/api/v1/")
        {           
            Get("votos", _ =>
            {
                var votos = repVoto.ObterTodos();
                return servicoRota.CriarJsonResponse(votos)
                                      .WithStatusCode(HttpStatusCode.OK);
            });
        }
    }
}
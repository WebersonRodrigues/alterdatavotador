namespace AlterdataVotador.API.Token
{   
    using AlterdataVotador.Dominio.Repositorio.Interfaces;
    using AlterdataVotador.Servicos.Interfaces;
    using Microsoft.Extensions.Configuration;
    using Nancy;
    using Nancy.Security;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    /// <summary>
    /// Classe utilizada para geração do Token JWT via API 
    /// </summary>
    public class TokenModule : NancyModule
    {
        /// <summary>
        /// Construtor da Classe de Token utilizada automaticamente pelo Nancy na API
        /// </summary>
        /// <param name="configurator">Configurador so sistema, permite acessar o appsettings.json</param>
        /// <param name="servicoRota">Servio de roteamento</param>
        public TokenModule(IConfiguration configurator, IServicoRota servicoRota)
        {

            Post("/api/token", _ =>
            {
                //TODO Criar aqui a regra para gerar o token de autenticação caso tenha tempo para implementar.
                return servicoRota.CriarJsonResponse(new {token = "aqui vai ir o token... implementar"})
                                  .WithStatusCode(HttpStatusCode.OK);
            });
        }
    }
}
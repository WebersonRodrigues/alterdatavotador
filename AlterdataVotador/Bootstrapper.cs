namespace AlterdataVotador
{
    using Nancy;
    using Nancy.Conventions;
    using System;
    using System.IO;
    using System.IO.Compression;
    using System.Linq;
    using Nancy.Bootstrapper;
    using Nancy.TinyIoc;
    using Nancy.Authentication.Forms;
    using AlterdataVotador.Autenticacao;
    using AlterdataVotador.Configuracao;
    using Microsoft.Extensions.Configuration;
    using Microsoft.AspNetCore.Hosting;
    using AlterdataVotador.Dominio.Repositorio.Classes;
    using Serilog;
    using AlterdataVotador.Dominio.Repositorio;
    using AlterdataVotador.Dominio.Repositorio.Interfaces;
    using AlterdataVotador.Servicos.Classes;
    using AlterdataVotador.Servicos.Interfaces;

    /// <summary>
    /// Classe principal do nancy quem é executada assim que aplicação é iniciada
    /// </summary>
    public class Bootstrapper : DefaultNancyBootstrapper
    {
        /// <summary>
        /// Configurador da aplicação
        /// </summary>
        private IConfiguration Configurador;

        /// <summary>
        /// Ambiente que aplicação vai rodar (Produção ou Desenvolvimento)
        /// </summary>
        private IHostingEnvironment env;

        /// <summary>
        /// Biblioteca de log responsável para logar em toda a aplicação
        /// </summary>
        private ILogger Log;

        /// <summary>
        /// Repositorio de Departamento
        /// </summary>
        private IRepositorioDepartamento RepositorioDepartamento;

        /// <summary>
        /// Repositorio de Empresa
        /// </summary>
        private IRepositorioEmpresa RepositorioEmpresa;

        /// <summary>
        /// Repositorio de Funcao
        /// </summary>
        private IRepositorioFuncao RepositorioFuncao;

        /// <summary>
        /// Repositorio de funcionario
        /// </summary>
        private IRepositorioFuncionario RepositorioFuncionario;

        /// <summary>
        /// Repositorio de recurso
        /// </summary>
        private IRepositorioRecurso RepositorioRecurso;

        /// <summary>
        /// Repositorio de usuário
        /// </summary>
        private IRepositorioUsuario RepositorioUsuario;

        /// <summary>
        /// Repositorio de Votos
        /// </summary>
        private IRepositorioVoto RepositorioVoto;

        /// <summary>
        /// Servico votaçao
        /// </summary>
        private IServicoVotacao ServicoVotacao;

        /// <summary>
        /// Construtor da classe Bootstrapper
        /// </summary>
        /// <param name="ambiente">Ambiente da aplicação</param>
        /// <param name="configuracao">Configuração</param>
        public Bootstrapper(IHostingEnvironment env, IConfiguration configuracao)
        {
            this.env = env;
            Configurador = configuracao;
            Log = Serilog.Log.Logger;
        }

        /// <summary>
        /// Onde tudo começa, assim que a aplicação é iniciada, o nancy executa este metodo
        /// Geralmente utilizado para colocar metodos e funções que devem ser executados assim que aplicação se inicia.
        /// </summary>
        /// <param name="container">Container utilizado pelo nancy para injeção de dependências</param>
        /// <param name="pipelines">Pipelines que podem ser utilizados</param>
        protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines)
        {
            Log.Information("Alterdata Votador sendo iniciado ...");
            Log.Information("Por favor, aguarde ...");

            RegistrarContainers(container);
            container.Register<IConfiguration>(Configurador);
            container.Register<IHostingEnvironment>(env);
            var configuradores = container.ResolveAll<StartupConfigurator>().Distinct();
            foreach(var configurador in configuradores)
                configurador.Configure(container, pipelines);

           new RoboAutenticacao(container.Resolve<IAutenticador>()).IniciarRoboAutenticacao();

            pipelines.BeforeRequest += LogBefore;
            pipelines.AfterRequest += LogAfter;

            Log.Information("Alterdata Votador iniciado com sucesso!");
        }

        /// <summary>
        /// Metodo que inicia os serviços e cria os repositorios.
        /// Em seguida ele registra tudo no container do Nancy
        /// </summary>
        /// <param name="container">container para injeção de dependências</param>
        private void RegistrarContainers(TinyIoCContainer container)
        {
            var cp = new ProvedorDeConexao(Configurador);
            RepositorioDepartamento = new RepositorioDepartamento(cp);
            RepositorioEmpresa = new RepositorioEmpresa(cp);
            RepositorioFuncao = new RepositorioFuncao(cp);
            RepositorioFuncionario = new RepositorioFuncionario(cp, RepositorioDepartamento, RepositorioEmpresa, RepositorioFuncao);
            RepositorioRecurso = new RepositorioRecurso(cp);
            RepositorioUsuario = new RepositorioUsuario(cp, Configurador, RepositorioFuncionario);
            RepositorioVoto = new RepositorioVoto(cp, RepositorioFuncionario, RepositorioRecurso);
            var autenticador = new AutenticadorUsuario(RepositorioUsuario, Log);
            var servicoRota = new ServicoRota();
            ServicoVotacao = new ServicoVotacao(RepositorioFuncionario, RepositorioRecurso, RepositorioVoto);

            container.Register<IServicoRota>(servicoRota);
            container.Register<IServicoVotacao>(ServicoVotacao);
            container.Register<IUserMapper>(autenticador);
            container.Register<IAutenticador>(autenticador);
            container.Register<ILogger>(Log);
            container.Register<ILogger>(Log);
            container.Register<IRepositorioUsuario>(RepositorioUsuario);
            container.Register<IProvedorDeConexao>(cp);
            container.Register<IUserMapper>(autenticador);
            container.Register<IAutenticador>(autenticador);
            container.Register<IConfiguration>(Configurador);
            container.Register<IHostingEnvironment>(env);

            container.Register<IRepositorioDepartamento>(RepositorioDepartamento);
            container.Register<IRepositorioEmpresa>(RepositorioEmpresa);
            container.Register<IRepositorioFuncao>(RepositorioFuncao);
            container.Register<IRepositorioFuncionario>(RepositorioFuncionario);
            container.Register<IRepositorioRecurso>(RepositorioRecurso);
            container.Register<IRepositorioUsuario>(RepositorioUsuario);
            container.Register<IRepositorioVoto>(RepositorioVoto);
 
        }

        /// <summary>
        /// Metodo para configurar o primeiro request da aplicação
        /// Direciona para a tela de login
        /// </summary>
        /// <param name="requestContainer">Container que pserá utilizado nos requests</param>
        /// <param name="pipelines">Pipiles que podem ser utilizados</param>
        /// <param name="context">Contexto a qual o Request será executado</param>
        protected override void RequestStartup(TinyIoCContainer requestContainer, IPipelines pipelines, NancyContext context)
        {
            var disableRedirect = (context.Request.Path == "/login" && 
                                context.Request.Method == "POST");

            var formsAuthConfiguration =
                new FormsAuthenticationConfiguration()
                {
                    RedirectUrl = "~/login",
                    UserMapper = requestContainer.Resolve<IUserMapper>(),
                    DisableRedirect = disableRedirect
                };

            FormsAuthentication.Enable(pipelines, formsAuthConfiguration);
        }

        /// <summary>
        /// Metodo configure do Nancy
        /// Pode ser utilizado para fazer algumas configurações
        /// </summary>
        /// <param name="ambiente">ambiente que aplicação está rodando</param>
        public override void Configure(Nancy.Configuration.INancyEnvironment environment)
        {
            if(env.IsDevelopment())
                environment.Views(runtimeViewUpdates: true, runtimeViewDiscovery: true);
        }

        /// <summary>
        /// Log que é executado antes do request bater no endpoint
        /// </summary>
        /// <param name="ctx">Contexto do nancy</param>
        /// <returns>Response</returns>
        private Response LogBefore(NancyContext ctx)
        {
            var loginUsuario = string.Empty;
            var user = ctx.CurrentUser;
            var json = string.Empty;
            
            if(user != null && user.Identity != null && user.Identity.IsAuthenticated)
                loginUsuario = user.Identity.Name;

            var positionBefore = ctx.Request.Body.Position;
            var streamCopy = new MemoryStream();
            ctx.Request.Body.CopyTo(streamCopy);
            byte[] byteInput = streamCopy.ToArray();
            streamCopy.Seek(0, SeekOrigin.Begin);

            ctx.Request.Body.Position = positionBefore;

            Log.Information(string.Format("[{0}] {1} - {2} - Request Recebido", ctx.Request.Method, ctx.Request.Path, loginUsuario));
            return null;
        }


        /// <summary>
        /// log que é executado depois do request bater no endpoint
        /// </summary>
        /// <param name="ctx">Contexto do nancy</param>
        private void LogAfter(NancyContext ctx)
        {
            var loginUsuario = string.Empty;
            var user = ctx.CurrentUser;
            
            if(user != null && user.Identity != null && user.Identity.IsAuthenticated)
                try
                {
                    loginUsuario = user.Identity.Name;
                }
                catch(Exception)
                {
                    Log.Warning($"LOGOUT >> Houve uma exception no logout do usuário: {user.Identity.Name}");
                }


            Log.Information($"[{ ctx.Request.Method}] - {ctx.Request.Path} - {ctx.Response.StatusCode}");
        }

        /// <summary>
        /// Sobrescreve o metodo RootPathProvider
        /// </summary>
        /// <value>Retorna um RootPathProvider customisado com base no ambiente da aplicação</value>
  
        protected override IRootPathProvider RootPathProvider
        {
            get { return new CustomRootPathProvider(env); }
        }
    }
    
    /// <summary>
    /// Classe responsável por pegar um caminho customizavel
    /// </summary>
    public class CustomRootPathProvider : IRootPathProvider
    {
        /// <summary>
        /// Ambiente que a aplicação está sendo executada
        /// </summary>
        private IHostingEnvironment Env;

        
        /// <summary>
        /// Construtor da classe
        /// </summary>
        /// <param name="env">Ambiente</param>
        public CustomRootPathProvider(IHostingEnvironment env)
        {
            Env = env;
        }

        // <summary>
        /// Pegar o caminho raiz
        /// </summary>
        /// <returns>String</returns>
        public string GetRootPath()
        {
            return Env.WebRootPath;
        }
    }
}

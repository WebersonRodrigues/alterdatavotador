namespace AlterdataVotador.Dominio.Repositorio.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using AlterdataVotador.Dominio.Entidades.Classes;
    using AlterdataVotador.Dominio.Repositorio.Interfaces;
    using AlterdataVotador.Infra.SqlCommands;
    using AlterdataVotador.Servicos;
    using Dapper;

    /// <summary>
    /// Repositorio da Empresa. Classe para conexão com o banco de dados
    /// </summary>
    public class RepositorioEmpresa : IRepositorioEmpresa
    {
        /// <summary>
        /// Provedor de conexão com o banco de dados
        /// </summary>
        private IProvedorDeConexao ProvedorDeConexao;
        private const string SCHEMA_E_TABELA = "alterdatavotador.empresa";
        private const string CAMPOS = "id, codigo, nome, data_inativacao as datainativacao";
    
        /// <summary>
        /// Construtor do repositorio de Empresa
        /// </summary>
        /// <param name="connection">Provedor de conexão</param>
        public RepositorioEmpresa(IProvedorDeConexao connection)
        {
            this.ProvedorDeConexao = connection;
        }
        
        /// <summary>
        /// Metodo ára atualizar a Empresa no banco de dados
        /// </summary>
        /// <param name="empresa">E<mpresa a ser atualizada/param>
        public void Atualizar(Empresa empresa)
        {
            var query = $@"UPDATE {SCHEMA_E_TABELA}
                            SET codigo=@Codigo, nome=@Nome, data_inativacao=@DataInativacao
                            WHERE id = @Id;";

            using (var conn = ProvedorDeConexao.CriarNovaConexao())
            {
                conn.Query(query, new { Id = empresa.Id,
                                        Codigo = empresa.Codigo,
                                        Nome = empresa.Nome,
                                        DataInativacao = empresa.DataInativacao });
            }
        }

        /// <summary>
        /// Metodo para inserir a Empresa no banco de dados
        /// </summary>
        /// <param name="empresa">Empresa</param>
        public void Inserir(Empresa empresa)
        {
            var query = $@"INSERT INTO {SCHEMA_E_TABELA}(
                            codigo, nome, data_inativacao)
                            VALUES (@Codigo, @Nome, @DataInativacao);";

            using (var conn = ProvedorDeConexao.CriarNovaConexao())
            {
                conn.Query(query, new { Codigo = empresa.Codigo,
                                        Nome = empresa.Nome,
                                        DataInativacao = empresa.DataInativacao });
            }
        }

        /// <summary>
        /// Obter empresa pelo Id (chave única no banco)
        /// </summary>
        /// <param name="id">Id da empresa</param>
        /// <returns>Empresa</returns>
        public Empresa Obter(long id)
        {
            var query = $@"SELECT {CAMPOS} 
                FROM {SCHEMA_E_TABELA} 
                WHERE Id = @Id;";

            using (var conn = ProvedorDeConexao.CriarNovaConexao())
            {
               return conn.Query<Empresa>(query,new {Id = id}).FirstOrDefault();
            }
        }

        /// <summary>
        /// Obter empresa pelo codigo da empresa
        /// </summary>
        /// <param name="codigo">código da empresa</param>
        /// <returns>Empresa</returns>
        public Empresa Obter(string codigo)
        {
            var query = $@"SELECT {CAMPOS} 
                            FROM {SCHEMA_E_TABELA} 
                            WHERE codigo = @Codigo;";

            using (var conn = ProvedorDeConexao.CriarNovaConexao())
            {
               return conn.Query<Empresa>(query,new {Codigo = codigo}).FirstOrDefault();
            }
        }

        /// <summary>
        /// Obter uma coleção com todas as Empresas
        /// </summary>
        /// <returns>IEnumerable<Empresa></returns>
        public IEnumerable<Empresa> ObterTodos()
        {
            var query = $@"SELECT {CAMPOS} 
                          FROM {SCHEMA_E_TABELA};";

            using (var conn = ProvedorDeConexao.CriarNovaConexao())
            {
               return conn.Query<Empresa>(query);
            }
        }

        /// <summary>
        /// Metodo para deletar todas as empresas do banco de dados.
        /// Só utilizar este metodo para testes unitários
        /// </summary>
        public void DeletarTodos()
        {
            using (var conn = ProvedorDeConexao.CriarNovaConexao())
            {
                conn.Query($"DELETE FROM {SCHEMA_E_TABELA};");
            }
        }
    }
}
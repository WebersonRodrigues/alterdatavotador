﻿namespace AlterdataVotador.Dominio.Repositorio.Classes
{
    using System;
    using System.Configuration;
    using System.Data;
    using AlterdataVotador.Infra.Database;
    using Microsoft.Extensions.Configuration;
    using AlterdataVotador.Dominio.Repositorio.Interfaces;

    /// <summary>
    /// classe responsável por criar as conexões com o banco de dadoa
    /// </summary>
    public class ProvedorDeConexao : IProvedorDeConexao
    {
        /// <summary>
        /// Configurador utilizado na classe
        /// </summary>
        IConfiguration Configurador;

        /// <summary>
        /// String de conexão utilizada para conectar ao banco de dados
        /// </summary>
        private static string ConnectionString;

        /// <summary>
        /// Construtor da classe. Aqui também é iniciado o Migrations
        /// </summary>
        /// <param name="configurador">Configurador</param>
        public ProvedorDeConexao(IConfiguration configurador)
        {
            Configurador = configurador;
            ConnectionString = Configurador["ConnectionStrings:Database"];

            string path = (new System.Uri(System.Reflection.Assembly.GetExecutingAssembly().CodeBase)).AbsolutePath;

            path = System.IO.Path.GetDirectoryName(System.Uri.UnescapeDataString(path)) + System.IO.Path.DirectorySeparatorChar;

            DatabaseInstaller.Install("postgres", ConnectionString, path + "AlterdataVotador.Migrations.dll");
        }

        /// <summary>
        /// Cria uma nova conexão baseada na string de conexão obitida do appsettings.json
        /// </summary>
        /// <returns>IDbConnection</returns>
        public IDbConnection CriarNovaConexao()
        {
            return new Npgsql.NpgsqlConnection(ConnectionString);
        }

    }
}
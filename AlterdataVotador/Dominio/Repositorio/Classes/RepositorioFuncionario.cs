namespace AlterdataVotador.Dominio.Repositorio.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using AlterdataVotador.Dominio.Entidades.Classes;
    using AlterdataVotador.Dominio.Repositorio.Interfaces;
    using AlterdataVotador.Infra.SqlCommands;
    using AlterdataVotador.Servicos;
    using Dapper;

    /// <summary>
    /// Repositorio de Funcionario.
    /// Classe responsável pela comunicação com o banco de dados
    /// </summary>
    public class RepositorioFuncionario : IRepositorioFuncionario
    {
        /// <summary>
        /// Provedor de conexão com o banco de dados
        /// </summary>
        private IProvedorDeConexao ProvedorConexao;

        /// <summary>
        /// Repositorio de Departamento
        /// </summary>
        private IRepositorioDepartamento RepositorioDepartamento;

        /// <summary>
        /// Repositorio de Empresa
        /// </summary>
        private IRepositorioEmpresa RepositorioEmpresa;

        /// <summary>
        /// Repositorio de Funcao
        /// </summary>
        private IRepositorioFuncao RepositorioFuncao;

        /// <summary>
        /// Schema e Tabela do banco de dados
        /// </summary>
        private const string SCHEMA_E_TABELA = "alterdatavotador.funcionario";

        /// <summary>
        /// Campos utilizados na consulta
        /// </summary>
        private const string CAMPOS = @"id, empresa_id as empresaid, departamento_id as departamentoid, 
                                            funcao_id as funcaoid, nome, cpf, email, 
                                            ramal, data_inativacao as datainativacao, guid_ad as guidad";
    
        /// <summary>
        /// Construtor do repositorio de Funcionario
        /// </summary>
        /// <param name="provedorConexao">Provedor de conexão</param>
        /// <param name="repDepartamento">Repositório de departamento</param>
        /// <param name="repEmpresa">Repositório de empresa</param>
        /// <param name="repFuncao">Repositório de função</param>
        public RepositorioFuncionario(IProvedorDeConexao provedorConexao, 
                                        IRepositorioDepartamento repDepartamento, 
                                        IRepositorioEmpresa repEmpresa,
                                        IRepositorioFuncao repFuncao)
        {
            this.ProvedorConexao = provedorConexao;
            this.RepositorioDepartamento = repDepartamento;
            this.RepositorioEmpresa = repEmpresa;
            this.RepositorioFuncao = repFuncao;
        }

        /// <summary>
        /// Metodo para atualizar o funcionário no banco de dados
        /// </summary>
        /// <param name="funcionario">Funcionario</param>
        public void Atualizar(Funcionario funcionario)
        {
            var query = $@"UPDATE {SCHEMA_E_TABELA}
                            SET empresa_id=@EmpresaId, departamento_id=@DepartamentoId, 
                            funcao_id=@FuncaoId, nome=@Nome, cpf=@CPF, 
                            email=@Email, ramal=@Ramal, data_inativacao=@DataInativacao, guid_ad=@GuidAD
                            WHERE id = @Id;";

            using (var conn = ProvedorConexao.CriarNovaConexao())
            {
                conn.Query(query, funcionario.ToObject());
            }
        }

        /// <summary>
        /// Metodo para inserir um funcionario no banco de dados
        /// </summary>
        /// <param name="funcionario">Funcionario</param>
        public void Inserir(Funcionario funcionario)
        {
            var query = $@"INSERT INTO {SCHEMA_E_TABELA}(
                                empresa_id, departamento_id, funcao_id, nome, 
                                cpf, email, ramal, data_inativacao, guid_ad)
                        VALUES (@EmpresaId, @DepartamentoId, @FuncaoId, @Nome,
                                @CPF, @Email, @Ramal, @DataInativacao, @GuidAD);";

            var fun= funcionario.ToObject();

            using (var conn = ProvedorConexao.CriarNovaConexao())
            {
                conn.Query(query, fun);
            }
        }

        /// <summary>
        /// Metodo para obter o funcionario pelo Id (chave única)
        /// </summary>
        /// <param name="id">Id do funcionario</param>
        /// <returns></returns>
        public Funcionario Obter(long id)
        {
            var query = $@"SELECT {CAMPOS} 
                            FROM {SCHEMA_E_TABELA}
                            WHERE Id = @Id;";

            using (var conn = ProvedorConexao.CriarNovaConexao())
            {
               return conn.Query<dynamic>(query,new {Id = id})
               .Select(e => CriarFuncionario(e)).FirstOrDefault();
            }
        }
        
        /// <summary>
        /// Metodo para criar um funcionário
        /// </summary>
        /// <param name="e">Elemento encontrado no banco de dados</param>
        /// <returns>Funcionario válido</returns>
        private Funcionario CriarFuncionario(dynamic e)
        {
            var departamento = RepositorioDepartamento.Obter((long)e.departamentoid);
            var empresa = RepositorioEmpresa.Obter((long)e.empresaid);
            var funcao = RepositorioFuncao.Obter((long) e.funcaoid);

            return new Funcionario((long)e.id, 
                                    (string)e.nome,
                                    (string)e.cpf,
                                    (string)e.email,
                                    empresa,
                                    departamento,
                                    funcao,
                                    (string)e.ramal,
                                    Convert.ToDateTime(e.datainativacao),
                                    new Guid(Convert.ToString(e.guidad)));
        }

        /// <summary>
        /// Metodo para obter o funcionario pelo email
        /// </summary>
        /// <param name="email">Email do funcionário</param>
        /// <returns>Funcionario</returns>
        public Funcionario Obter(string email)
        {
            var query = $@"SELECT {CAMPOS} 
                            FROM {SCHEMA_E_TABELA} 
                            WHERE email = @Email;";

            using (var conn = ProvedorConexao.CriarNovaConexao())
            {
               return conn.Query<dynamic>(query,new {Email = email})
               .Select(e => CriarFuncionario(e)).FirstOrDefault();
            }
        }

        /// <summary>
        /// Obter uma coleção com todos os funcionários encontrados no banco de dados
        /// </summary>
        /// <returns>IEnumerable<Funcionario></returns>
        public IEnumerable<Funcionario> ObterTodos()
        {
            var query = $@"SELECT {CAMPOS} 
                FROM {SCHEMA_E_TABELA};";

            using (var conn = ProvedorConexao.CriarNovaConexao())
            {
               return conn.Query<dynamic>(query)
               .Select(e => (Funcionario) CriarFuncionario(e));
               //TODO: Feito este typecast devido ao bug nesta versão do vscode.
            }
        }

        /// <summary>
        /// Metodo para deletar todos os funcionarios do Banco de dados
        /// Utilizar este metodo somente em testes unitários
        /// </summary>
        public void DeletarTodos()
        {
            using (var conn = ProvedorConexao.CriarNovaConexao())
            {
                conn.Query("DELETE FROM alterdatavotador.funcionario;");
            }
        }
    }
}
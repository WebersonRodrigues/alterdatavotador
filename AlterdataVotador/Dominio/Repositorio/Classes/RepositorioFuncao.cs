namespace AlterdataVotador.Dominio.Repositorio.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using AlterdataVotador.Dominio.Entidades.Classes;
    using AlterdataVotador.Dominio.Repositorio.Interfaces;
    using AlterdataVotador.Infra.SqlCommands;
    using AlterdataVotador.Servicos;
    using Dapper;

    /// <summary>
    /// Repositorio de Funcao. Responsável por manter conexão com o banco de dados
    /// </summary>
    public class RepositorioFuncao : IRepositorioFuncao
    {
        /// <summary>
        /// Provedor de conexão
        /// </summary>
        private IProvedorDeConexao ProvedorDeConexao;
        
        private const string SCHEMA_E_TABELA = "alterdatavotador.funcao";
        private const string CAMPOS = "id, codigo, nome, data_inativacao as datainativacao";
    
        /// <summary>
        /// Construtor da classe Funcao
        /// </summary>
        /// <param name="connection">Provedor de conexão</param>
        public RepositorioFuncao(IProvedorDeConexao connection)
        {
            this.ProvedorDeConexao = connection;
        }

        /// <summary>
        /// Metodo para atualizar a Funcao no banco de dados
        /// </summary>
        /// <param name="funcao">funcao do funcionário</param>
        public void Atualizar(Funcao funcao)
        {
            var query = $@"UPDATE {SCHEMA_E_TABELA} 
                SET codigo=@Codigo, nome=@Nome, data_inativacao=@DataInativacao
                WHERE id = @Id;";

            using (var conn = ProvedorDeConexao.CriarNovaConexao())
            {
                conn.Query(query, new { Id = funcao.Id,
                                        Codigo = funcao.Codigo,
                                        Nome = funcao.Nome,
                                        DataInativacao = funcao.DataInativacao });
            }
        }

        /// <summary>
        /// Metodo para inserir a Funcao no banco de dados
        /// </summary>
        /// <param name="funcao">Funcao do funcionário</param>
        public void Inserir(Funcao funcao)
        {
            var query = $@"INSERT INTO {SCHEMA_E_TABELA }(
                        codigo, nome, data_inativacao)
                        VALUES (@Codigo, @Nome, @DataInativacao);";

            using (var conn = ProvedorDeConexao.CriarNovaConexao())
            {
                conn.Query(query, new { Codigo = funcao.Codigo,
                                        Nome = funcao.Nome,
                                        DataInativacao = funcao.DataInativacao });
            }
        }

        /// <summary>
        /// Metodo para obter a função por Id (chave única)
        /// </summary>
        /// <param name="id">Id da função</param>
        /// <returns>Funcao</returns>
        public Funcao Obter(long id)
        {
             var query = $@"SELECT {CAMPOS} 
                            FROM {SCHEMA_E_TABELA} 
                            WHERE Id = @Id;";

            using (var conn = ProvedorDeConexao.CriarNovaConexao())
            {
               return conn.Query<Funcao>(query,new {Id = id}).FirstOrDefault();
            }
        }

        /// <summary>
        /// Metodo para obter a Funcao pelo código
        /// </summary>
        /// <param name="codigo">Código da função</param>
        /// <returns></returns>
        public Funcao Obter(string codigo)
        {
            var query = $@"SELECT {CAMPOS} 
                FROM {SCHEMA_E_TABELA} 
                WHERE codigo = @Codigo;";

            using (var conn = ProvedorDeConexao.CriarNovaConexao())
            {
               return conn.Query<Funcao>(query,new {Codigo = codigo}).FirstOrDefault();
            }
        }

        /// <summary>
        /// Obter uma coleção com todas as funções cadastradas no Banco de dados
        /// </summary>
        /// <returns>IEnumerable<Funcao></returns>
        public IEnumerable<Funcao> ObterTodos()
        {
            var query = $@"SELECT {CAMPOS} 
                            FROM {SCHEMA_E_TABELA};";

            using (var conn = ProvedorDeConexao.CriarNovaConexao())
            {
               return conn.Query<Funcao>(query);
            }
        }

        /// <summary>
        /// Metodo para deletar todas as funções do banco
        /// Só utilizar para testes unitários
        /// </summary>
        public void DeletarTodos()
        {
            using (var conn = ProvedorDeConexao.CriarNovaConexao())
            {
                conn.Query($"DELETE FROM {SCHEMA_E_TABELA};");
            }
        }
    }
}
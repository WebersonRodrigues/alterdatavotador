﻿namespace AlterdataVotador.Dominio.Repositorio.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using AlterdataVotador.Dominio.Entidades.Classes;
    using AlterdataVotador.Dominio.Repositorio.Interfaces;
    using AlterdataVotador.Infra.SqlCommands;
    using AlterdataVotador.Servicos;
    using Dapper;
    using Microsoft.Extensions.Configuration;
    using Nancy;
    using Nancy.Security;
    using Novell.Directory.Ldap;

    /// <summary>
    /// Repositorio de Usuario.
    /// Classe responsável pela comunicação com o banco de dados
    /// </summary>
    public class RepositorioUsuario :  IRepositorioUsuario
    {
        /// <summary>
        /// Configurador da aplicação
        /// </summary>
        private IConfiguration Configurador;

        /// <summary>
        /// Repositorio de Funcionário
        /// </summary>
        private IRepositorioFuncionario RepositorioDeFuncionario;

        //Provedor de conexão
        private IProvedorDeConexao ProvedorDeConexao;

        /// <summary>
        /// Schema e Tabela do banco de dados
        /// </summary>
        private const string SCHEMA_E_TABELA = "alterdatavotador.usuario u";

        /// <summary>
        /// Campos utilizados em consultas
        /// </summary>
        private const string CAMPOS = " id, login, guid, perfil_id as perfilid, funcionario_id as funcionarioid ";

        /// <summary>
        /// Construtor do repositorio de funcionario
        /// </summary>
        /// <param name="provedorDeConexao">Provedor de conexão</param>
        /// <param name="configurador">Configurador</param>
        /// <param name="repFuncionario">Repositorio de funcionário</param>
        public RepositorioUsuario(IProvedorDeConexao provedorDeConexao, IConfiguration configurador, IRepositorioFuncionario repFuncionario)
        {
            Configurador = configurador;
            this.ProvedorDeConexao = provedorDeConexao;
            this.RepositorioDeFuncionario = repFuncionario;
        }

        /// <summary>
        /// Construtor do repositório de funcionário
        /// </summary>
        /// <param name="provedorDeConexao">Provedor de conexão</param>
        /// <param name="repFuncionario">Repositorio de funcionário</param>
        public RepositorioUsuario(IProvedorDeConexao provedorDeConexao, IRepositorioFuncionario repFuncionario)
        {
            this.ProvedorDeConexao = provedorDeConexao;
            this.RepositorioDeFuncionario = repFuncionario;
        }

        /// <summary>
        /// Metodo para autenticar o usuário no servidor de LDAP
        /// </summary>
        /// <param name="usuario">Usuário cadastrado no servidor LDAP</param>
        /// <param name="senha">Senha cadastrada no servidor LDAP</param>
        /// <returns>bool</returns>
        public bool Autenticar(string usuario, string senha)
        {
            if (string.IsNullOrEmpty(senha))
                return false;

            usuario = usuario.ToLower().Replace("@alterdata.com.br", "");
      
            var ldapConn = new LdapConnection();
            ldapConn.Connect(Configurador["LDAP:Host"], int.Parse(Configurador["LDAP:Porta"]));

            try
            {
                ldapConn.Bind("alterdata\\" + usuario, senha);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        
        /// <summary>
        /// Obter o usuário pelo login
        /// </summary>
        /// <param name="login">Login do usuário</param>
        /// <returns>Usuario</returns>
        public Usuario Obter(string login)
        {
            return ObterInterno("LOWER(u.login) = LOWER(@login)", new { login = login }).FirstOrDefault();
        }

        /// <summary>
        /// Metodo generico privado para Obter o usuário do Banco com base no dados passados por parâmetros
        /// </summary>
        /// <param name="where">Filtro a ser passado no Where</param>
        /// <param name="param">Parametros a serem passados</param>
        /// <returns></returns>
        private IEnumerable<Usuario> ObterInterno(string where = "", object param = null)
        {
      
            var query = $@"SELECT {CAMPOS}
                            FROM {SCHEMA_E_TABELA}";
        
            if (!string.IsNullOrEmpty(where))
                query += " WHERE " + where;

            using (var conn = ProvedorDeConexao.CriarNovaConexao())
            {
                return conn.Query<dynamic>(query, param)
                .Select(u => (Usuario) ContruirFuncionario(u));
            }
           
        }

        /// <summary>
        /// Metodo para construir um usuario valido
        /// </summary>
        /// <param name="e">elemento</param>
        /// <returns>Usuario</returns>
        private Usuario ContruirFuncionario(dynamic e)
        {
            return new Usuario((long)e.id,
                                (string)e.login,
                                new Guid(Convert.ToString(e.guid)),
                                (long)e.perfilid,
                                RepositorioDeFuncionario.Obter((long)e.funcionarioid));
        }

        /// <summary>
        /// Metodo para obter uma coleção com todos os usuários encontrados no banco de dados
        /// </summary>
        /// <returns>IEnumerable<Usuario></returns>
        public IEnumerable<Usuario> ObterTodos()
        {
            return ObterInterno();
        }

        /// <summary>
        /// Metodo para inserir usuário no banco de dados
        /// </summary>
        /// <param name="usuario">Usuário aser inserirdo</param>
        public void Inserir(Usuario usuario)
        {
            var query = $@"INSERT INTO alterdatavotador.usuario(
                        login, guid, perfil_id, funcionario_id)
                        VALUES (@Login, @Guid, @PerfilId, @FuncionarioId);";

            using (var conn = ProvedorDeConexao.CriarNovaConexao())
            {
                conn.Query(query, new { Login = usuario.Login,
                                            Guid = usuario.Guid,
                                            PerfilId = usuario.PerfilId,
                                            FuncionarioId = usuario.Funcionario.Id});
            }
        }

        /// <summary>
        /// Usuário a ser atualizado
        /// </summary>
        /// <param name="usuario">Usuário</param>
        public void Atualizar(Usuario usuario)
        {
            var query = $@"UPDATE {SCHEMA_E_TABELA}
                            SET login=@Login, guid=@Guid, perfil_id=@PerfilId, funcionario_id=@FuncionarioId
                            WHERE id = @Id";
            
            using (var conn = ProvedorDeConexao.CriarNovaConexao())
            {
                conn.Query(query, new { Id = usuario.Id,
                                        Login = usuario.Login,
                                        Guid = usuario.Guid,
                                        PerfilId = usuario.PerfilId,
                                        FuncionarioId = usuario.Funcionario.Id });
            }
        }

        /// <summary>
        /// Metodo que deleta todos os usuários do banco de dados.
        /// Deve ser utilziado somente em testes unitários
        /// </summary>
        public void DeletarTodos()
        {
            using (var conn = ProvedorDeConexao.CriarNovaConexao())
            {
                conn.Query($"DELETE FROM {SCHEMA_E_TABELA}");
            }
        }

        /// <summary>
        /// Obter o usuário pelo Id (chave única)
        /// </summary>
        /// <param name="id">Id do usuário</param>
        /// <returns></returns>
        public Usuario Obter(long id)
        {
            return this.ObterInterno("u.id = @id", new { id = id }).DefaultIfEmpty(new Usuario()).FirstOrDefault();
        }


    }
}
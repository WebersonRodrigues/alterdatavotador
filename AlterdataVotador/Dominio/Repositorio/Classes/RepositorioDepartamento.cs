namespace AlterdataVotador.Dominio.Repositorio.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using AlterdataVotador.Dominio.Entidades.Classes;
    using AlterdataVotador.Dominio.Repositorio.Interfaces;
    using AlterdataVotador.Infra.SqlCommands;
    using AlterdataVotador.Servicos;
    using Dapper;

    /// <summary>
    /// Repositorio de Departamento. Classe responsável por centralizar toda comunicação com o banco de dados.
    /// </summary>
    public class RepositorioDepartamento : IRepositorioDepartamento
    {
        /// <summary>
        /// Provedor de conexão para o reposítorio
        /// </summary>
        private IProvedorDeConexao ProvedorDeConexao;
        private const string SCHEMA_E_TABELA = "alterdatavotador.departamento";
        private const string CAMPOS= "id, codigo, nome, data_inativacao as datainativacao";
    
        /// <summary>
        /// Construtor do repositório de Departamento
        /// </summary>
        /// <param name="connection">Provedor de conexão</param>
        public RepositorioDepartamento(IProvedorDeConexao connection)
        {
            this.ProvedorDeConexao = connection;
        }

        /// <summary>
        /// Metodo para atualizar o Departamento no banco de dados
        /// </summary>
        /// <param name="departamento">Departamento</param>
        public void Atualizar(Departamento departamento)
        {
            var query = $@"UPDATE {SCHEMA_E_TABELA}
                SET codigo=@Codigo, nome=@Nome, data_inativacao=@DataInativacao
                WHERE id = @Id;";

            using (var conn = ProvedorDeConexao.CriarNovaConexao())
            {
                conn.Query(query, new { Id = departamento.Id,
                                        Codigo = departamento.Codigo,
                                        Nome = departamento.Nome,
                                        DataInativacao = departamento.DataInativacao });
            }
        }

        /// <summary>
        /// Metodo responsável por inserir um Departamento no banco de dados
        /// </summary>
        /// <param name="departamento">Departamento</param>    
        public void Inserir(Departamento departamento)
        {
            var query = $@"INSERT INTO {SCHEMA_E_TABELA}(
                        codigo, nome, data_inativacao)
                        VALUES (@Codigo, @Nome, @DataInativacao);";

            using (var conn = ProvedorDeConexao.CriarNovaConexao())
            {
                conn.Query(query, new { Codigo = departamento.Codigo,
                                        Nome = departamento.Nome,
                                        DataInativacao = departamento.DataInativacao });
            }
        }

        /// <summary>
        /// Metodo para obter o Departamento pelo Id (Chave única)
        /// </summary>
        /// <param name="id">Id do departamento</param>
        /// <returns>Departamento</returns>
        public Departamento Obter(long id)
        {
            var query = $@"SELECT {CAMPOS} 
                        FROM {SCHEMA_E_TABELA} 
                        WHERE Id = @Id;";

            using (var conn = ProvedorDeConexao.CriarNovaConexao())
            {
               return conn.Query<Departamento>(query,new {Id = id}).FirstOrDefault();
            }
        }

        /// <summary>
        /// Metodo para obter o departamento pelo código
        /// </summary>
        /// <param name="codigo">Código do departamento</param>
        /// <returns>Departamento</returns>
        public Departamento Obter(string codigo)
        {
            var query = $@"SELECT {CAMPOS}
                        FROM {SCHEMA_E_TABELA} 
                        WHERE codigo = @Codigo;";

            using (var conn = ProvedorDeConexao.CriarNovaConexao())
            {
               return conn.Query<Departamento>(query,new {Codigo = codigo}).FirstOrDefault();
            }
        }

        /// <summary>
        /// Metodo para obter uma coleção de Departamentos
        /// </summary>
        /// <returns>IEnumerable<Departamento></returns>
        public IEnumerable<Departamento> ObterTodos()
        {
            var query = $@"SELECT {CAMPOS} 
                            FROM {SCHEMA_E_TABELA};";

            using (var conn = ProvedorDeConexao.CriarNovaConexao())
            {
               return conn.Query<Departamento>(query);
            }
        }
    
        /// <summary>
        /// Metodo criado para deletar da base de dados todos os departamentos.
        /// Deve ser utilizado somente em testes unitários.
        /// </summary>
        public void DeletarTodos()
        {
            using (var conn = ProvedorDeConexao.CriarNovaConexao())
            {
                conn.Query($"DELETE FROM {SCHEMA_E_TABELA};");
            }
        }
    }
}
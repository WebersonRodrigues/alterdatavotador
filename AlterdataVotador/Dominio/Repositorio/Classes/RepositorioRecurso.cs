namespace AlterdataVotador.Dominio.Repositorio.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using AlterdataVotador.Dominio.Entidades.Classes;
    using AlterdataVotador.Dominio.Repositorio.Interfaces;
    using AlterdataVotador.Infra.SqlCommands;
    using AlterdataVotador.Servicos;
    using Dapper;

    /// <summary>
    /// Repositorio de Recurso.
    /// Classe responsável pela comunicação com o banco de dados
    /// </summary>
    public class RepositorioRecurso : IRepositorioRecurso
    {
        /// <summary>
        /// Provedor de comunicação com o banco de dados
        /// </summary>
        private IProvedorDeConexao ProvedorDeConexao;

        /// <summary>
        /// Schema e Tabela do banco de dados
        /// </summary>
        private const string SCHEMA_E_TABELA = "alterdatavotador.recurso";

        /// <summary>
        /// Campos utilizados na consulta
        /// </summary>
        private const string CAMPOS = @" id, codigo_processo as codigoprocesso, 
                                        nome_produto as nomeproduto, descricao, 
                                        detalhes, data_abertura as dataabertura, 
                                        data_alteracao as dataalteracao, status ";
    
        /// <summary>
        /// Construtor do repositorio de Recurso
        /// </summary>
        /// <param name="provedorDeConexao">Provedor de conexao com o banco de dados</param>
        public RepositorioRecurso(IProvedorDeConexao provedorDeConexao)
        {
            this.ProvedorDeConexao = provedorDeConexao;
        }

        /// <summary>
        /// Metodo para atualizar o recurso no banco de dados
        /// </summary>
        /// <param name="recurso">Recurso a ser atualizado</param>
        public void Atualizar(Recurso recurso)
        {
            var query = $@"UPDATE {SCHEMA_E_TABELA} 
                            SET codigo_processo=@CodigoProcesso, nome_produto=@NomeProduto, 
                            descricao=@Descricao, detalhes=@Detalhes, 
                                data_abertura=@DataAbertura, data_alteracao=@DataAlteracao, status=@Status 
                            WHERE id =  @Id;";

            using (var conn = ProvedorDeConexao.CriarNovaConexao())
            {
                conn.Query(query, new { Id = recurso.Id,
                                        CodigoProcesso = recurso.CodigoProcesso,
                                        NomeProduto = recurso.NomeProduto,
                                        Descricao = recurso.Descricao,
                                        Detalhes = recurso.Detalhes,
                                        DataAbertura = recurso.DataAbertura,
                                        DataAlteracao = recurso.DataAlteracao,
                                        Status = recurso.Status });
            }
        }

        /// <summary>
        /// Metodo para inserir o recurso no banco de dados
        /// </summary>
        /// <param name="recurso">Recurso</param>
        public void Inserir(Recurso recurso)
        {
            var query = $@"INSERT INTO {SCHEMA_E_TABELA}(
                        codigo_processo, nome_produto, descricao, detalhes, data_abertura, 
                        data_alteracao, status)
                        VALUES (@CodigoProcesso, @NomeProduto, @Descricao, @Detalhes, @DataAbertura, 
                        @DataAlteracao, @Status);";

            using (var conn = ProvedorDeConexao.CriarNovaConexao())
            {
                conn.Query(query, new { CodigoProcesso = recurso.CodigoProcesso,
                                        NomeProduto = recurso.NomeProduto,
                                        Descricao = recurso.Descricao,
                                        Detalhes = recurso.Detalhes,
                                        DataAbertura = recurso.DataAbertura,
                                        DataAlteracao = recurso.DataAlteracao,
                                        Status = recurso.Status });
            }
        }

        /// <summary>
        /// Metodo para obter o Recurso pelo Id (chave única)
        /// </summary>
        /// <param name="id">Id do recurso</param>
        /// <returns>Recurso</returns>
        public Recurso Obter(long id)
        {
            var query = $@"SELECT {CAMPOS} 
                        FROM {SCHEMA_E_TABELA} WHERE id = @Id;";

            using (var conn = ProvedorDeConexao.CriarNovaConexao())
            {
                return conn.Query<Recurso>(query, new { Id = id}).FirstOrDefault();
            }
        }

        /// <summary>
        ///  Metodo para obter o Recurso pelo código do processo
        /// </summary>
        /// <param name="codigoProcesso">Código do Processo</param>
        /// <returns>Recurso</returns>
        public Recurso Obter(string codigoProcesso)
        {
            var query = $@"SELECT {CAMPOS}
                           FROM {SCHEMA_E_TABELA} 
                           WHERE codigo_processo = @CodigoProcesso;";

            using (var conn = ProvedorDeConexao.CriarNovaConexao())
            {
                return conn.Query<Recurso>(query, new { CodigoProcesso = codigoProcesso }).FirstOrDefault();
            }
        }

        /// <summary>
        /// Obter uma coleção com todos os recursos encontrados no banco de dados
        /// </summary>
        /// <returns>IEnumerable<Recurso></returns>
        public IEnumerable<Recurso> ObterTodos()
        {
            var query = $@"SELECT {CAMPOS}  
                            FROM {SCHEMA_E_TABELA};";

            using (var conn = ProvedorDeConexao.CriarNovaConexao())
            {
                return conn.Query<Recurso>(query);
            }
        }
        
        /// <summary>
        /// Deletar todos os recurso do banco de dados.
        /// Metodo só deve ser utilziado em testes unitários
        /// </summary>
        public void DeletarTodos()
        {
            using (var conn = ProvedorDeConexao.CriarNovaConexao())
            {
                conn.Query($"DELETE FROM {SCHEMA_E_TABELA};");
            }
        }

        /// <summary>
        /// Obter uma coleção de recursos com base na data de abertura dos recursos
        /// </summary>
        /// <param name="dataAberturaInicial">Data inicial da abertura do recurso</param>
        /// <param name="dataAberturaFinal">Data final da abertura do recurso</param>
        /// <returns>IEnumerable<Recurso></returns>
        public IEnumerable<Recurso> Obter(DateTime dataAberturaInicial, DateTime dataAberturaFinal)
        {
            var query = $@"SELECT {CAMPOS} 
                            FROM {SCHEMA_E_TABELA} 
                            WHERE data_abertura between @DataInicio AND @DataFim;";

            var obj = new { DataInicio = dataAberturaInicial.Date,
                            DataFim = dataAberturaFinal.Date + new TimeSpan(23,59,59)};

            using (var conn = ProvedorDeConexao.CriarNovaConexao())
            {
                return conn.Query<Recurso>(query, obj);
            }
        }
    }
}
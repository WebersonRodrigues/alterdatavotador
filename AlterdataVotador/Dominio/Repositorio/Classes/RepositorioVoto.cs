namespace AlterdataVotador.Dominio.Repositorio.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using AlterdataVotador.Dominio.Entidades.Classes;
    using AlterdataVotador.Dominio.Repositorio.Interfaces;
    using AlterdataVotador.Infra.SqlCommands;
    using AlterdataVotador.Servicos;
    using Dapper;

    /// <summary>
    /// Repositorio de Voto
    /// Classe responsável pela comunicação com o banco de dados
    /// </summary>
    public class RepositorioVoto : IRepositorioVoto
    {
        /// <summary>
        /// Provedor de conexão
        /// </summary>
        private IProvedorDeConexao ProvedorDeConexao;

        /// <summary>
        /// Repositorio do funcionario
        /// </summary>
        private IRepositorioFuncionario RepositorioDeFuncionario;

        /// <summary>
        /// Repositorio de recurso
        /// </summary>
        private IRepositorioRecurso RepositorioDeRecurso;
        
        /// <summary>
        /// Schema e Tabela do banco de dados
        /// </summary>
        private const string SCHEMA_E_TABELA = " alterdatavotador.voto ";

        /// <summary>
        /// Campos utilizados em consultas
        /// </summary>
        private const string CAMPOS = @" id, data_realizado as datarealizado, 
                                funcionario_id as funcionarioid, recurso_id as recursoid, comentario ";

    
        /// <summary>
        /// Construtor do Repositorio de Voto
        /// </summary>
        /// <param name="provedorDeConexao">Provedor de conexão</param>
        /// <param name="repositorioFuncionario">Repositorio do funcionário</param>
        /// <param name="repositorioRecurso">Repositorio de recurso</param>
        public RepositorioVoto(IProvedorDeConexao provedorDeConexao,
        IRepositorioFuncionario repositorioFuncionario,
        IRepositorioRecurso repositorioRecurso)
        {
            this.ProvedorDeConexao = provedorDeConexao;
            this.RepositorioDeFuncionario = repositorioFuncionario;
            this.RepositorioDeRecurso = repositorioRecurso;
        }

        /// <summary>
        /// Metodo para atualizar o Voto
        /// </summary>
        /// <param name="voto">Voto</param>
        public void Atualizar(Voto voto)
        {
           var query = $@"UPDATE {SCHEMA_E_TABELA} 
                        SET data_realizado = @DataRealizado, 
                        funcionario_id = @FuncionarioId, 
                        recurso_id = @RecursoId, 
                        comentario = @Comentario 
                        WHERE id = @Id";

            using (var conn = ProvedorDeConexao.CriarNovaConexao())
            {
                conn.Query(query, new { Id = voto.Id,
                                        DataRealizado = voto.DataRealizado,
                                        FuncionarioId = voto.Funcionario.Id,
                                        RecursoId = voto.Recurso.Id,
                                        Comentario = voto.Comentario });
            }
        }

        /// <summary>
        /// Metodo para inserir voto
        /// </summary>
        /// <param name="voto">Voto</param>
        public void Inserir(Voto voto)
        {
            var query = $@"INSERT INTO {SCHEMA_E_TABELA}(
                            data_realizado, funcionario_id, recurso_id, comentario)
                        VALUES (@DataRealizado, @FuncionarioId, @RecursoId, @Comentario);";

            using (var conn = ProvedorDeConexao.CriarNovaConexao())
            { 
                conn.Query(query, new { DataRealizado = voto.DataRealizado,
                                        FuncionarioId = voto.Funcionario.Id,
                                        RecursoId = voto.Recurso.Id,
                                        Comentario = voto.Comentario });
            }            
        }

        /// <summary>
        /// Metodo para obter o voto pelo Id (chave única)
        /// </summary>
        /// <param name="id">Id do voto</param>
        /// <returns>Voto</returns>
        public Voto Obter(long id)
        {
            var query = $@"SELECT {CAMPOS} 
                            FROM {SCHEMA_E_TABELA} 
                            WHERE id = @Id;";

            using (var conn = ProvedorDeConexao.CriarNovaConexao())
            {
                return conn.Query<dynamic>(query, new { Id = id })
                .Select(e => (Voto) ConstruirVoto(e)).FirstOrDefault();
            }
        }


        /// <summary>
        /// Metodo para obter o voto pelo funcionário
        /// </summary>
        /// <param name="funcionario">Funcionario que efetuou o voto</param>
        /// <returns>Voto</returns>
        public Voto Obter(Funcionario funcionario)
        {
            var query = $@"SELECT {CAMPOS} 
                            FROM {SCHEMA_E_TABELA} 
                            WHERE funcionario_id = @FuncionarioId;";

            using (var conn = ProvedorDeConexao.CriarNovaConexao())
            {
                return conn.Query<dynamic>(query, new { FuncionarioId = funcionario.Id })
                .Select(e => (Voto) ConstruirVoto(e)).FirstOrDefault();
            }
        }

        /// <summary>
        /// Retorna uma coleção de voto com base na data de efetivcação do voto
        /// </summary>
        /// <param name="dataInicio">Data inicio efetivação</param>
        /// <param name="dataFim">Data fim efetivação</param>
        /// <returns></returns>
        public IEnumerable<Voto> Obter(DateTime dataInicio, DateTime dataFim)
        {
            var query = $@"SELECT {CAMPOS}
                            FROM {SCHEMA_E_TABELA} 
                            WHERE data_realizado between @DataInicio AND @DataFim;";

            using (var conn = ProvedorDeConexao.CriarNovaConexao())
            {
                return conn.Query<dynamic>(query, new { DataInicio = dataInicio.Date,
                                                        DataFim = dataFim.Date + new TimeSpan(23,59,59) })
                .Select(e => (Voto) ConstruirVoto(e));
            }
        }
        
        /// <summary>
        /// Metodo para contruir voto
        /// </summary>
        /// <param name="e">elemento vindo do banco de dados</param>
        /// <returns>Voto</returns>
        private Voto ConstruirVoto(dynamic e)
        {
            var funcionario = RepositorioDeFuncionario.Obter((long)e.funcionarioid);
            var recurso = RepositorioDeRecurso.Obter((long)e.recursoid);
            return new Voto((long)e.id,
                            Convert.ToDateTime(e.datarealizado),
                            funcionario,
                            recurso,
                            (string)e.comentario);
        }
       
       /// <summary>
       /// Metodo para obter uma coleção com todos os votos encontrados no banco de dados. 
       /// </summary>
       /// <returns>IEnumerable<Voto></returns>
        public IEnumerable<Voto> ObterTodos()
        {
            var query = $@"SELECT {CAMPOS} 
                            FROM {SCHEMA_E_TABELA};";

            using (var conn = ProvedorDeConexao.CriarNovaConexao())
            {
                return conn.Query<dynamic>(query)
                .Select(e => (Voto) ConstruirVoto(e));
            }
        }

        /// <summary>
        /// Metodo para deletar todos os votos do banco de dados
        /// Metodo deve ser utilizado somente em teste unitário
        /// </summary>
        public void DeletarTodos()
        {
            using (var conn = ProvedorDeConexao.CriarNovaConexao())
            {
                conn.Query($"DELETE FROM {SCHEMA_E_TABELA};");
            }
        }

        public IEnumerable<Voto> ObterPorFuncionario(Funcionario funcionario)
        {
              var query = $@"SELECT {CAMPOS}
                            FROM {SCHEMA_E_TABELA} 
                            WHERE funcionario_id = @FuncionarioId;";

            using (var conn = ProvedorDeConexao.CriarNovaConexao())
            {
                return conn.Query<dynamic>(query, new { FuncionarioId = funcionario.Id })
                .Select(e => (Voto) ConstruirVoto(e));
            }
        }
    }
}
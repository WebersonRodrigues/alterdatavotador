﻿namespace CalculoDePreco.Dominio.Repositorio.Exceptions
{
    using System;

    /// <summary>
    /// classe exeções no repositorio
    /// </summary>
    public class RepositorioException : Exception
    {
        /// <summary>
        /// Metodo para lançar exceção dentro do repositorio
        /// </summary>
        /// <param name="msg">Mensagem disparada exeção</param>
        /// <returns></returns>
        public RepositorioException(string msg) : base(msg) { }
    }
}
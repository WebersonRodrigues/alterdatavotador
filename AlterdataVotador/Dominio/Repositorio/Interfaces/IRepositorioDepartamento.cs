namespace AlterdataVotador.Dominio.Repositorio.Interfaces
{
    using System;
    using System.Collections.Generic;
    using AlterdataVotador.Dominio.Entidades.Classes;

    /// <summary>
    /// Interface manter contrato com o Repositorio de Departamento
    /// </summary>
    public interface IRepositorioDepartamento
    {
        /// <summary>
        /// Obter departamento pelo Id (chave única)
        /// </summary>
        /// <param name="id">Id do departamento</param>
        /// <returns>Departamento</returns>
        Departamento Obter(long id);

        /// <summary>
        /// Obter departamento pelo codigo
        /// </summary>
        /// <param name="codigo">Código do departamento</param>
        /// <returns>Departamento</returns>
        Departamento Obter(string codigo);

        /// <summary>
        /// /// Obter coleção com todos os departamentos encontrados 
        /// </summary>
        /// <returns>IEnumerable<Departamento></returns>
        IEnumerable<Departamento> ObterTodos();

        /// <summary>
        /// Atualizar departamento
        /// </summary>
        /// <param name="departamento">Departamento a ser atualizado</param>
        void Atualizar(Departamento departamento);

        /// <summary>
        /// Inserir departamento
        /// </summary>
        /// <param name="departamento">Departamento a ser inserido</param>
        void Inserir(Departamento departamento);
    }

}
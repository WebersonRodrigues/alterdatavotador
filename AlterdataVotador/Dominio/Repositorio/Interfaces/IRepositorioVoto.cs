namespace AlterdataVotador.Dominio.Repositorio.Interfaces
{
    using System;
    using System.Collections.Generic;
    using AlterdataVotador.Dominio.Entidades.Classes;

    /// <summary>
    /// Interface para manter contrato com o repositorio de Voto
    /// </summary>
    public interface IRepositorioVoto
    {
        /// <summary>
        /// Obter voto pelo Id (chave única)
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Voto</returns>
        Voto Obter(long id);

        /// <summary>
        /// Obter voto pelo Funcionario
        /// </summary>
        /// <param name="funcionario">funcionario que evetuou o voto</param>
        /// <returns>Voto</returns>
        Voto Obter(Funcionario funcionario);

        /// <summary>
        /// Obter coleção de votos feitos pelo funcionario
        /// </summary>
        /// <param name="funcionario"></param>
        /// <returns>IEnumerable<Voto></returns>
        IEnumerable<Voto> ObterPorFuncionario(Funcionario funcionario);

        /// <summary>
        ///  Obter coleção de votos pela data de efetivação
        /// </summary>
        /// <param name="dataInicioEfetivacao">Data da efetivação</param>
        /// <param name="dataFimEfetivacao">Data de efetivação</param>
        /// <returns></returns>
        IEnumerable<Voto> Obter(DateTime dataInicioEfetivacao, DateTime dataFimEfetivacao);

        /// <summary>
        /// Obter uma lista com todos os votos encontrados
        /// </summary>
        /// <returns></returns>
        IEnumerable<Voto> ObterTodos();

        /// <summary>
        /// Atualizar voto
        /// </summary>
        /// <param name="voto">Voto a ser atualizado</param>
        void Atualizar(Voto voto);

        /// <summary>
        /// Inserir voto
        /// </summary>
        /// <param name="voto">Voto a ser inserido</param>
        void Inserir(Voto voto); 
    }
}
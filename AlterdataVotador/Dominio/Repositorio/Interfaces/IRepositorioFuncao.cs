namespace AlterdataVotador.Dominio.Repositorio.Interfaces
{
    using System;
    using System.Collections.Generic;
    using AlterdataVotador.Dominio.Entidades.Classes;

    /// <summary>
    /// Interface para contrato do repositorio de Funcao
    /// </summary>
    public interface IRepositorioFuncao
    {
        /// <summary>
        /// Obter função pelo Id (chave única)
        /// </summary>
        /// <param name="id">Id da função</param>
        /// <returns>Funcao</returns>
        Funcao Obter(long id);

        /// <summary>
        /// Obter função pelo código
        /// </summary>
        /// <param name="codigo">código da função</param>
        /// <returns>Funcao</returns>
        Funcao Obter(string codigo);

        /// <summary>
        ///Obter uma coleção com todas as funções encontradas
        /// </summary>
        /// <returns>IEnumerable<Funcao></returns>
        IEnumerable<Funcao> ObterTodos();

        /// <summary>
        /// Atualizar função
        /// </summary>
        /// <param name="funcao">Função a ser atualizada</param>
        void Atualizar(Funcao funcao);

        /// <summary>
        /// Inserir função
        /// </summary>
        /// <param name="funcao">Função que será inserida</param>
        void Inserir(Funcao funcao);
    }
}
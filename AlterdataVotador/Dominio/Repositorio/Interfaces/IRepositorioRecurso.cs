namespace AlterdataVotador.Dominio.Repositorio.Interfaces
{
    using System;
    using System.Collections.Generic;
    using AlterdataVotador.Dominio.Entidades.Classes;

    /// <summary>
    /// Interface de contrato com o Repositorio de Recurso
    /// </summary>
    public interface IRepositorioRecurso
    {
        /// <summary>
        /// Obter recurso pelo ID (chave única)
        /// </summary>
        /// <param name="id">Id do recurso</param>
        /// <returns>Recurso</returns>
        Recurso Obter(long id);

        /// <summary>
        /// Obter recurso pelo código 
        /// </summary>
        /// <param name="codigo">Código do recurso</param>
        /// <returns>Recurso</returns>
        Recurso Obter(string codigo);

        /// <summary>
        /// Obter coleção de recurso com base na data de abertura inicial e final
        /// </summary>
        /// <param name="dataAberturaInicial">Data de abertura do recurso</param>
        /// <param name="dataAberturaFinal">Data de abertura do recurso</param>
        /// <returns>IEnumerable<Recurso></returns>
        IEnumerable<Recurso> Obter(DateTime dataAberturaInicial, DateTime dataAberturaFinal);

        /// <summary>
        /// Obter coleção com todos os recursos encontrados
        /// </summary>
        /// <returns>IEnumerable<Recurso></returns>
        IEnumerable<Recurso> ObterTodos();

        /// <summary>
        /// Atualizar recurso
        /// </summary>
        /// <param name="recurso">Recurso a ser atualziado</param>
        void Atualizar(Recurso recurso);

        /// <summary>
        /// Inserir recurso
        /// </summary>
        /// <param name="recurso">Recurso a ser inserido</param>
        void Inserir(Recurso recurso);
    }
}
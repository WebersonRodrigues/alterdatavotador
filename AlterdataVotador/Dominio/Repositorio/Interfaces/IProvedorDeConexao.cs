﻿namespace AlterdataVotador.Dominio.Repositorio.Interfaces
{
    using System.Data;

    /// <summary>
    /// Interface criada para contrado de comunicação com o banco de dados
    /// Todos os bancos devem implementar esta interface
    /// </summary>
    public interface IProvedorDeConexao
    {
        /// <summary>
        /// Cria uma nova conexão com o banco de dados
        /// </summary>
        /// <returns></returns>
        IDbConnection CriarNovaConexao();
    }
}

﻿namespace AlterdataVotador.Dominio.Repositorio.Interfaces
{
    using System;
    using System.Collections.Generic;
    using AlterdataVotador.Dominio.Entidades.Classes;

    /// <summary>
    /// Interface de contrato com o Repositorio de Usuario 
    /// </summary>
    public interface IRepositorioUsuario
    {
        /// <summary>
        /// Obter o usuario pélo Id (chave única)
        /// </summary>
        /// <param name="id">Id do usuário</param>
        /// <returns>Usuario</returns>
        Usuario Obter(long id);

        /// <summary>
        /// Obter usuário pelo login
        /// </summary>
        /// <param name="login">Login do usuário</param>
        /// <returns>Usuario</returns>
        Usuario Obter(string login);

        /// <summary>
        /// Obter uma coleção com todos os usuários encontrados
        /// </summary>
        /// <returns>IEnumerable<Usuario></returns>
        IEnumerable<Usuario> ObterTodos();

        /// <summary>
        /// Inserir usuário
        /// </summary>
        /// <param name="usuario">Usuário a ser inserido</param>
        void Inserir(Usuario usuario);

        /// <summary>
        /// Atualizar usuário
        /// </summary>
        /// <param name="usuario">Usuário a ser atualizado</param>
        void Atualizar(Usuario usuario);

        /// <summary>
        /// Autenticar o usuário
        /// </summary>
        /// <param name="usuario">Login ou email do usuário</param>
        /// <param name="senha">Senha do usuário</param>
        /// <returns></returns>
        bool Autenticar(string usuario, string senha);

    }

    public class RepositorioUsuarioException : Exception
    {
        public RepositorioUsuarioException(string message) : base(message) { }
    };

    public class UsuarioVazioException : RepositorioUsuarioException
    {
        public UsuarioVazioException(string message) : base(message) { }
    }
}
namespace AlterdataVotador.Dominio.Repositorio.Interfaces
{
    using System;
    using System.Collections.Generic;
    using AlterdataVotador.Dominio.Entidades.Classes;

    /// <summary>
    /// Interface para manter contrato com o Repositorio de Funcionario
    /// </summary>
    public interface IRepositorioFuncionario
    {
        /// <summary>
        /// Obter funcionario pelo Id (chave única)
        /// </summary>
        /// <param name="id">Id do funcionário</param>
        /// <returns>Funcionario</returns>
        Funcionario Obter(long id);

        /// <summary>
        /// Obter funcionario pelo email
        /// </summary>
        /// <param name="email">Email do funcionário</param>
        /// <returns>Funcionario</returns>
        Funcionario Obter(string email);

        /// <summary>
        /// Obter coleção com todos os funcionários encontrados
        /// </summary>
        /// <returns>IEnumerable<Funcionario></returns>
        IEnumerable<Funcionario> ObterTodos();

        /// <summary>
        /// Atualizar funcionario
        /// </summary>
        /// <param name="funcionario">Funcionario a ser atualizado</param>
        void Atualizar(Funcionario funcionario);

        /// <summary>
        /// Inserir funcionario
        /// </summary>
        /// <param name="funcionario">Funcionário a ser inserido</param>
        void Inserir(Funcionario funcionario);
    }
}
namespace AlterdataVotador.Dominio.Repositorio.Interfaces
{
    using System;
    using System.Collections.Generic;
    using AlterdataVotador.Dominio.Entidades.Classes;

    /// <summary>
    /// Interface para manter contrato com o Repositorio de Empresa
    /// </summary>
    public interface IRepositorioEmpresa
    {
        /// <summary>
        /// Obter a empresa pelo Id (chave única)
        /// </summary>
        /// <param name="id">Id da empresa</param>
        /// <returns>Empresa</returns>
        Empresa Obter(long id);

        /// <summary>
        /// Obter a empresa pelo código
        /// </summary>
        /// <param name="codigo">Código da empresa</param>
        /// <returns>Empresa</returns>
        Empresa Obter(string codigo);

        /// <summary>
        /// Obter uma coleção com todas as empresas encontradas
        /// </summary>
        /// <returns>IEnumerable<Empresa></returns>
        IEnumerable<Empresa> ObterTodos();

        /// <summary>
        /// Atualizar empresa no banco de dados
        /// </summary>
        /// <param name="empresa">Empresa a ser atualizada</param>
        void Atualizar(Empresa empresa);

        /// <summary>
        /// Inserir empresa
        /// </summary>
        /// <param name="empresa">Empresa a ser inserida</param>
        void Inserir(Empresa empresa);
    }
}
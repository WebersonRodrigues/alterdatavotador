namespace AlterdataVotador.Dominio.Entidades.Classes
{
    using System;
    using Newtonsoft.Json;

    /// <summary>
    /// Classe Empresa, Na regra de negocio é a empresa a qual o funcionario está relacionado
    /// </summary>
    public class Empresa
    {
        /// <summary>
        /// Id da empresa, chave primária no banco de dados
        /// </summary>
        /// <value>long</value>
        [JsonProperty(propertyName: "id")]
        public long Id { get; private set; }

        /// <summary>
        /// Código da empresa
        /// </summary>
        /// <value>String</value>
        [JsonProperty(propertyName: "codigo")]
        public string Codigo { get; set; }

        /// <summary>
        /// Nome da empresa
        /// </summary>
        /// <value>String</value>
        [JsonProperty(propertyName: "nome")]
        public string Nome { get; set; }

        /// <summary>
        /// Data de inativação da empresa
        /// </summary>
        /// <value>DateTime?</value>
        [JsonProperty(propertyName: "dataInativacao")]
        public DateTime? DataInativacao { get; set; }

        /// <summary>
        /// Construtor do objeto Empresa
        /// </summary>
        /// <param name="id">Id da empresa</param>
        /// <param name="codigo">Código da empresa</param>
        /// <param name="nome">Nome da empresa</param>
        /// <param name="dataInativacao">Data de inativação da empresa</param>
        public Empresa(long id, string codigo, string nome, DateTime? dataInativacao)
        {
            this.Id = id;
            this.Codigo = codigo;
            this.Nome = nome;
            this.DataInativacao = dataInativacao;
        }
        
        /// <summary>
        /// Sobrescrita do metodo ToString()
        /// </summary>
        /// <returns>Retorna uma string contendo o Id, Codigo e Nome da empresa</returns>
        public override string ToString()
        {
            return $"Id: {this.Id} - Codigo:{this.Codigo} - Nome: {this.Nome}";
        }
    }
}
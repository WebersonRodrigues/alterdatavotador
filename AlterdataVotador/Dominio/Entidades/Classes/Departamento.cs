namespace AlterdataVotador.Dominio.Entidades.Classes
{
    using System;
    using Newtonsoft.Json;

    public class Departamento
    {
        /// <summary>
        /// Id do departamento, chave primária no banco
        /// </summary>
        /// <value>long</value>
        [JsonProperty(propertyName: "id")]
        public long Id { get; private set; }
        
        /// <summary>
        /// Código do departamento
        /// </summary>
        /// <value>String</value>
        [JsonProperty(propertyName: "codigo")]
        public string Codigo { get; set; }

        /// <summary>
        /// Nome do departamento
        /// </summary>
        /// <value>String</value>
        [JsonProperty(propertyName: "nome")]
        public string Nome { get; set; }

        /// <summary>
        /// Data de inativação do departamento
        /// </summary>
        /// <value>DateTime?</value>
        [JsonProperty(propertyName: "dataInativacao")]
        public DateTime? DataInativacao { get; set; }

        /// <summary>
        /// Construtor da objeto Departamento
        /// </summary>
        /// <param name="id">Id do departmaneto (long)</param>
        /// <param name="codigo">Código do departamwento (string)</param>
        /// <param name="nome">Nome do departamento (string)</param>
        /// <param name="dataInativacao">Data de inativação do departamento (DateTime?)</param>
        public Departamento (long id, string codigo, string nome, DateTime? dataInativacao)
        {
            this.Id = id;
            this.Codigo = codigo;
            this.Nome = nome;
            this.DataInativacao = dataInativacao;
        }

        /// <summary>
        /// Sobrescrita no metodo ToString()
        /// </summary>
        /// <returns>Retorna uma string contendo o Id e o Nome do departamento</returns>
        public override string ToString()
        {
            return $"Id: {this.Id} - Nome: {this.Nome}";
        }

    }
}
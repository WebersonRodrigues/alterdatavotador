namespace AlterdataVotador.Dominio.Entidades.Classes
{
    using System;
    using Newtonsoft.Json;

    /// <summary>
    /// Classe de voto. Na regra de negocio o voto está vinculado a um funcionario e um recurso
    /// </summary>
    public class Voto
    {
        /// <summary>
        /// Id do voto, é chave primária no banco de dados
        /// </summary>
        /// <value>long</value>
        [JsonProperty(propertyName: "id")]
        public long Id {get; private set;}

        /// <summary>
        /// Data que o voto foi realizado. Essa informação deve vir sempre do cliente
        /// </summary>
        /// <value>DateTime</value>
        [JsonProperty(propertyName: "dataRealizado")]
        public DateTime DataRealizado {get; set;}

        /// <summary>
        /// Funcionário que efetuou o voto
        /// </summary>
        /// <value>Funcionario</value>
        [JsonProperty(propertyName: "funcionario")]
        public Funcionario Funcionario {get; set;}

        /// <summary>
        /// Recurso a qual foi votado
        /// </summary>
        /// <value>Recurso</value>
        [JsonProperty(propertyName: "recurso")]
        public Recurso Recurso {get; set;}

        /// <summary>
        /// Comentario feito pelo o usuário no momento do Voto
        /// </summary>
        /// <value>string</value>
        [JsonProperty(propertyName: "comentario")]
        public string Comentario {get; set;}

        /// <summary>
        /// Construtor da classe Voto
        /// </summary>
        /// <param name="id">Id do voto</param>
        /// <param name="dataRealizado">Data que foi realizado</param>
        /// <param name="funcionario">Funcionário que realizou o voto</param>
        /// <param name="recurso">Recurso que foi votado</param>
        /// <param name="comentario">Comentário feito no momento da votação</param>
        public Voto(long id, DateTime dataRealizado, Funcionario funcionario, Recurso recurso, string comentario)
        {
            this.Id = id;
            this.DataRealizado = dataRealizado;
            this.Funcionario = funcionario;
            this.Recurso = recurso;
            this.Comentario = comentario;
        }
    }
}
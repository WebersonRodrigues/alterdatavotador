namespace AlterdataVotador.Dominio.Entidades.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Principal;
    using Nancy.Security;

    /// <summary>
    /// Classe do usuário utilizada para autenticação no sistema
    /// </summary>
    public class Usuario : IIdentity
    {
        /// <summary>
        /// Id do usuário, é chave única no banco de dados
        /// </summary>
        /// <value>long</value>
        public long Id { get; set; }

        /// <summary>
        /// Identificador do usuário guid, utilizado na autenticação
        /// </summary>
        /// <value>Guid</value>
        public Guid Guid { get; set; }

        /// <summary>
        /// Login do usuário
        /// </summary>
        /// <value>string</value>
        public string Login { get; set; }

        /// <summary>
        /// Perfil do usuário. Será utilizado em versões futuras para separar niveis de acesso ao sistema
        /// </summary>
        /// <value>long</value>
        public long PerfilId { get; set; } 

        /// <summary>
        /// Funcionario vinculado ao usuário, Todo usuário deve está relacionado a um funcionário
        /// </summary>
        /// <value>Funcionario</value>
        public Funcionario Funcionario { get; set; }

        /// <summary>
        ///  Propriedade do tipo de autenticação utilziada pelo Nancy para criar a identidade do usuário na autenticação
        /// </summary>
        /// <value>string</value>
        public string AuthenticationType => "Default";
        
        /// <summary>
        /// Propriedade utilziada pelo nancy para saber se o usuário esta autenticado validando o Guid
        /// </summary>
        /// <value>bool</value>
        public bool IsAuthenticated => this.Guid != null;

        /// <summary>
        /// Proriedade utilizada pelo nancy para saber o nome do usuário autenticado
        /// </summary>
        /// <value>string</value>
        private string name { get; set; }

        /// <summary>
        /// Propriedade utilizada pelo nancy para obter o nome do usuário autenticado
        /// </summary>
        public string Name => name;
        // fim das propriedades para autenticação no nancy
     
        /// <summary>
        /// Construtor default do Usuario
        /// </summary>
        public Usuario(){}

        /// <summary>
        /// Construtor do Usuario
        /// </summary>
        /// <param name="login">Login do usuário</param>
        public Usuario(string login)
        {
            this.name = login;
            this.Login = login.ToUpper();
        }

        /// <summary>
        /// Construtor do Usuário
        /// </summary>
        /// <param name="guid">Guid do usuário</param>
        /// <param name="login">Login do usuário</param>
        public Usuario(Guid guid, string login)
        {
            this.Guid = guid;
            this.name = login;
            this.Login = login.ToUpper();
        }
        
        /// <summary>
        /// Construtor do Usuario
        /// </summary>
        /// <param name="id">Id do usuário</param>
        /// <param name="login">Login do usuário</param>
        /// <param name="guid">Guid do usuário</param>
        /// <param name="perfilId">Perfil do usuário</param>
        /// <param name="funcionario">Funcionario vinculado ao usuário</param>
        public Usuario(long id, string login, Guid guid, long perfilId, Funcionario funcionario)
        {
            this.Id = id;
            this.name = login;
            this.Login = login.ToUpper();
            this.Guid = guid;
            this.PerfilId = perfilId;
            this.Funcionario = funcionario;
        }

        /// <summary>
        /// sobrescrita dp metodo ToString()
        /// </summary>
        /// <returns>Retorna uma string com o Guid e o Name do usuário</returns>
        public override string ToString()
        {
            return $"{this.Guid} | {this.Name} ";
        }

        /// <summary>
        /// Sobrescrita do metodo Equals()
        /// </summary>
        /// <param name="obj"> Obj para comparação</param>
        /// <returns>bool</returns>
        public override bool Equals(object obj)
        {
            var u = obj as Usuario;
            if(u == null)
                return false;

            return (Guid.Equals(u.Guid) && Name.Equals(u.Name));
        }

        /// <summary>
        /// Sobrescrita do metodo GetHashCode()
        /// </summary>
        /// <returns>Retorna um int baseado no Id e Name</returns>
        public override int GetHashCode()
        {
            return  Id.GetHashCode() + Name.GetHashCode();
        }
    }
}

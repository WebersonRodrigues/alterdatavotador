namespace AlterdataVotador.Dominio.Entidades.Classes
{
    using System;
    using Newtonsoft.Json;

    /// <summary>
    /// Classe do objeto Funcao, a função é um objeto atribuido ao funcionário na regra de negocio.
    /// </summary>
    public class Funcao
    {
        /// <summary>
        /// Id da função, chave primária no banco de dados
        /// </summary>
        /// <value>long</value>
        [JsonProperty(propertyName: "id")]
        public long Id { get; private set; }

        /// <summary>
        /// Código da função
        /// </summary>
        /// <value>String</value>
        [JsonProperty(propertyName: "codigo")]
        public string Codigo { get; set; }

        /// <summary>
        /// Nome da função
        /// </summary>
        /// <value>string</value>
        [JsonProperty(propertyName: "nome")]
        public string Nome { get; set; }

        /// <summary>
        /// Data de inativação da função
        /// </summary>
        /// <value>DateTime?</value>
        [JsonProperty(propertyName: "dataInativacao")]
        public DateTime? DataInativacao { get; set; }

        /// <summary>
        /// Construtor do objeto Funcao
        /// </summary>
        /// <param name="id">Id da função</param>
        /// <param name="codigo">Código da função</param>
        /// <param name="nome">Nome da função</param>
        /// <param name="dataInativacao">Data de inativação da função</param>
        public Funcao(long id, string codigo, string nome, DateTime? dataInativacao)
        {
            this.Id = id;
            this.Codigo = codigo;
            this.Nome = nome;
            this.DataInativacao = dataInativacao;
        }

        /// <summary>
        /// Sobrescrita do metodo ToString()
        /// </summary>
        /// <returns>Retorna o Id, Codigo e o Nome da função</returns>
        public override string ToString()
        {
            return $"Id: {this.Id} - Codigo: {this.Codigo} - Nome: {this.Nome}";
        }
    }
}
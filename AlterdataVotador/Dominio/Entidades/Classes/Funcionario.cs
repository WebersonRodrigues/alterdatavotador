namespace AlterdataVotador.Dominio.Entidades.Classes
{
    using System;
    using Newtonsoft.Json;

    /// <summary>
    /// Classe Funcionário, na regra de negocio, é a classe refente aos funcionários da empresa
    /// </summary>
    public class Funcionario 
    {
        /// <summary>
        /// Id do funcionario, é chave primária no banco de dados
        /// </summary>
        /// <value>long</value>
        [JsonProperty(propertyName: "id")]
        public long Id { get;  set; }
        
        /// <summary>
        /// Nome do funcionario
        /// </summary>
        /// <value>string</value>
        [JsonProperty(propertyName: "nome")]
        public string Nome{ get; set; }

        /// <summary>
        /// CPF do funcionário
        /// </summary>
        /// <value>string</value>
        [JsonProperty(propertyName: "cpf")]
        public string CPF{ get; set; }

        /// <summary>
        /// Email do funcionário
        /// </summary>
        /// <value>string</value>
        [JsonProperty(propertyName: "email")]
        public string Email{ get; set; }   

        /// <summary>
        /// Empresa do funcionário
        /// </summary>
        /// <value>Empresa</value>
        [JsonProperty(propertyName: "empresa")]
        public Empresa Empresa { get; set; }

        /// <summary>
        /// Departamento do funcionário
        /// </summary>
        /// <value>Departamento</value>
        [JsonProperty(propertyName: "departamento")]
        public Departamento Departamento { get; set; }

        /// <summary>
        /// Funcão do funcionário
        /// </summary>
        /// <value>Funcao</value>
        [JsonProperty(propertyName: "funcao")]
        public Funcao Funcao { get; set; }

        /// <summary>
        /// Ramal do funcionário
        /// </summary>
        /// <value>string</value>
        [JsonProperty(propertyName: "ramal")]
        public string Ramal { get; set; }

        /// <summary>
        /// Data de inativação do funcionário
        /// </summary>
        /// <value>DateTime?</value>
        [JsonProperty(propertyName: "dataInativacao")]
        public DateTime? DataInativacao { get; set; }

        /// <summary>
        /// Identificador Guid do funcionário. Utilizado para autenticação
        /// </summary>
        /// <value>Guid</value>
        [JsonProperty(propertyName: "guidAd")]
        public Guid? GuidAD { get; set; }

        /// <summary>
        /// Construtor da classe funcionário
        /// </summary>
        /// <param name="id">Id do funcionario</param>
        /// <param name="nome">Nome do funcionario</param>
        /// <param name="cpf">CPF do funcionário</param>
        /// <param name="email">Email do funcionário</param>
        /// <param name="empresa">Empresa do funcionário</param>
        /// <param name="departamento">Departamento do funcionário</param>
        /// <param name="funcao">Função do funcionário</param>
        /// <param name="ramal">Ramal do funcionário</param>
        /// <param name="dataInativacao">DataInativação do funcionário</param>
        /// <param name="guidAD">Guid no AD do funcionário</param>
        public Funcionario(long id, string nome, string cpf, string email, Empresa empresa, 
        Departamento departamento, Funcao funcao, string ramal, DateTime? dataInativacao, Guid? guidAD)
        {
            this.Id = id;
            this.Nome = nome;
            this.CPF = cpf;
            this.Email = email.ToUpper();
            this.Empresa = empresa;
            this.Departamento = departamento;
            this.Funcao = funcao;
            this.Ramal = ramal;
            this.DataInativacao = dataInativacao;
            this.GuidAD = guidAD;
        }

        /// <summary>
        /// Construtor do objeto Funcionario
        /// </summary>
        /// <param name="id">Id do funcionário</param>
        /// <param name="nome">Nome do funcionário</param>
        /// <param name="email">Email do funcionário</param>
        public Funcionario(long id, string nome, string email)
        {
            this.Id = id;
            this.Nome = nome;
            this.Email = email;
        }

        /// <summary>
        /// Propriedade para saber se o funcionário esta Ativo ou não
        /// </summary>
        /// <value>bool</value>
        public bool Inativo 
        {
            get { return this.DataInativacao.HasValue; }
        }

        /// <summary>
        /// sobrescrita do metodo ToString()
        /// </summary>
        /// <returns>Retorna uma string contendo o Id e o Email</returns>
        public override string ToString()
        {
            return $"Id: {this.Id} - Email: {this.Email}";
        }
        
        /// <summary>
        /// sobrescrita do metodo GetHashCode()
        /// </summary>
        /// <returns>Retorna um int com base no Id, CPF, Nome e Departamento.Id</returns>
        public override int GetHashCode()
        {
            return (int)this.Id + this.CPF.GetHashCode() + this.Nome.GetHashCode()  + this.Departamento.Id.GetHashCode();
        }

        /// <summary>
        /// Converte o funcionário em um obj aceito pelo banco de dados (utilizado no Dapper)
        /// </summary>
        /// <returns>object</returns>
        public object ToObject()
        {
            return new {
                Id = this.Id,
                Nome = this.Nome,
                CPF = this.CPF,
                Email = this.Email.ToUpper(),
                EmpresaId = this.Empresa.Id,
                DepartamentoId = this.Departamento.Id,
                FuncaoId = this.Funcao.Id,
                Ramal = this.Ramal,
                DataInativacao = this.DataInativacao,
                GuidAD = this.GuidAD,
            };
        }
    }
}
namespace AlterdataVotador.Dominio.Entidades.Classes
{
    using System;
    using Newtonsoft.Json;
    public class ResumoGeral
    {
        /// <summary>
        /// Recurso único no resumo geral
        /// </summary>
        /// <value>Recurso</value>
        [JsonProperty(propertyName: "recurso")]
        public Recurso Recurso {get; set;}
        
        /// <summary>
        /// Quantidade de votos por recurso
        /// </summary>
        /// <value>long</value>
        [JsonProperty(propertyName: "quantidadeVotos")]
        public long QuantidadeVotos {get; set;}
        
        /// <summary>
        /// Comentário do voto efetuado
        /// </summary>
        /// <value>string</value>
        [JsonProperty(propertyName: "comentario")]
        public string Comentario {get; set;}
        
        /// <summary>
        /// Propriedade que informa ao funcionário atual se ele votou ou não no recurso
        /// </summary>
        /// <value>Retorna true ou false caso o funcionário tenha votado no recurso</value>
        [JsonProperty(propertyName: "votei")]
        public bool Votei {get; set;}
        
        /// <summary>
        /// Data de efetivação do voto, data e hora do voto no fuso horário da localizada onde o voto foi executado
        /// </summary>
        /// <value>DateTime?</value>
        [JsonProperty(propertyName: "dataEfetivacaoVoto")]
        public DateTime? DataEfetivacaoVoto {get; set;}

        /// <summary>
        /// Construtor da classe ResumoGeral
        /// </summary>
        /// <param name="recurso">Recurso único no resumo</param>
        public ResumoGeral(Recurso recurso)
        {
            this.Recurso = recurso;
            this.QuantidadeVotos = 0;
            this.Comentario = String.Empty;
            this.Votei = false;
            this.DataEfetivacaoVoto = DateTime.Now;
        }
    }
}
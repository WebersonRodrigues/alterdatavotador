namespace AlterdataVotador.Dominio.Entidades.Classes
{
    using System;
    using Newtonsoft.Json;

    /// <summary>
    /// Classe de Recurso. Na regra de negocio e o objeto a ser votado pelo funcionário
    /// </summary>
    public class Recurso
    {
        /// <summary>
        /// Id do funcionário, é chave primária no banco de dados
        /// </summary>
        /// <value>long</value>
        [JsonProperty(propertyName: "id")]
        public long Id {get; private set;}

        /// <summary>
        /// Código do processo
        /// </summary>
        /// <value>string</value>
        [JsonProperty(propertyName: "codigoProcesso")]
        public string CodigoProcesso {get; set;}

        /// <summary>
        /// Nome do produto vinculado ao recurso, Futuramente essa propriedade será vinculada ao id de um objeto produto
        /// </summary>
        /// <value>string</value>
        [JsonProperty(propertyName: "nomeProduto")]
        public string NomeProduto {get; set;}
        
        /// <summary>
        /// Descrição do recurso
        /// </summary>
        /// <value></value>
        [JsonProperty(propertyName: "descricao")]
        public string Descricao {get; set;}

        /// <summary>
        /// Detalhes do recurso
        /// </summary>
        /// <value></value>
        [JsonProperty(propertyName: "detalhes")]
        public string Detalhes {get; set;}

        /// <summary>
        /// Data de abertura do recurso
        /// </summary>
        /// <value>DateTime</value>
        [JsonProperty(propertyName: "dataAbertura")]
        public DateTime DataAbertura {get; private set;}

        /// <summary>
        /// Data de alteração do recurso
        /// </summary>
        /// <value>DateTime?</value>
        [JsonProperty(propertyName: "dataAlteracao")]
        public DateTime? DataAlteracao {get; set;}
        

        /// <summary>
        /// Status do produto, pode ser Aberto, Cancelado, Ponto ou EmDesenvolvimento
        /// </summary>
        /// <value>Status</value>
        [JsonProperty(propertyName: "status")]
        public Status Status {get; set;}

        /// <summary>
        /// Construtor da classe Recurso
        /// </summary>
        /// <param name="id">Id do recurso</param>
        /// <param name="codigoProcesso">Codigo do Processo vinculado ao recurso</param>
        /// <param name="nomeProduto">Nome do Produto vinculado recurso</param>
        /// <param name="descricao">Descrição do recurso</param>
        /// <param name="detalhes">Detalhes do recurso</param>
        /// <param name="dataAbertura">Data de abertura</param>
        /// <param name="status">Status do recurso</param>
        /// <param name="dataAlteracao">Data de alteração do recurso</param>
        public Recurso (long id, string codigoProcesso, string nomeProduto, string descricao, string detalhes, 
        DateTime dataAbertura, Status status, DateTime? dataAlteracao )
        {
            this.Id = id;
            this.CodigoProcesso = codigoProcesso;
            this.NomeProduto = nomeProduto;
            this.Descricao = descricao;
            this.Detalhes = detalhes;
            this.DataAbertura = dataAbertura;
            this.Status = status;
            this.DataAlteracao = dataAlteracao;
        
        }
        /// <summary>
        /// Construtor default
        /// </summary>
        public Recurso(){}

        /// <summary>
        /// sobrescrita do metodo ToString()
        /// </summary>
        /// <returns>Retorna uma string contendo o Id, Código do processo, Nome do produto, Descrição e a Data de abertura do recurso</returns>
        public override string ToString()
        {
            return $"Id: {this.Id} - Código do processo: {this.CodigoProcesso} - Produto: {this.NomeProduto} - Descrição: {this.Descricao} - Data de abertura: {this.DataAbertura} - Status: {this.Status}";
        }

        /// <summary>
        /// Metodo criado para alterar o status do recurso.
        /// </summary>
        /// <param name="dataAlteracao"> Informar a data da alteração para que não importe o fuso horário, o sistema sempre vai salvar na data que o usuario informar.</param>
        public void AlterarStatus(Status status, DateTime dataAlteracao)
        {
            this.Status = status;
            this.DataAlteracao = DateTime.Now;
        }

    }
    /// <summary>
    /// Enum referente aos Status do Recurso (Aberto = 0, Cancelado = 1, Pronto = 3, EmDesenvolvimento = 4)
    /// </summary>
    public enum Status 
    { 
        Aberto, Cancelado, Pronto, EmDesenvolvimento
    }


}
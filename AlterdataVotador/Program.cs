﻿namespace AlterdataVotador
{
    using System;
    using AlterdataVotador;
    using Microsoft.AspNetCore;
    using Microsoft.AspNetCore.Hosting;
    using Serilog;
    using Serilog.Events;
    
    public class Program
    {
        public static void Main(string[] args)
        {
            const long Megabytes = 1024 * 1024;
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                .WriteTo.Console()
                .WriteTo.RollingFile("../AlterdataVotador.logs/{Date}-log.txt", retainedFileCountLimit: 5, fileSizeLimitBytes: 25 * Megabytes)
                .CreateLogger();
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) 
        {
            var builder = WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseSerilog()
                .UseUrls("http://*:5000");

            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            if(env == null)
                builder.UseEnvironment("Development");

            return builder.Build();
        }
    }
}

namespace AlterdataVotador.Configuracao
{
    using Nancy.Bootstrapper;
    using Nancy.TinyIoc;

    /// <summary>
    /// Configurador inicial para ser utilizado em um configuração inicial diferente
    /// </summary>
    public abstract class StartupConfigurator
    {
        /// <summary>
        /// Metodo que configura informações no inicio da aplicação
        /// </summary>
        /// <param name="container">Conteinar utilizado para injeção de dependências</param>
        /// <param name="pipelines">Pipelines para uso caso necessário</param>
        public abstract void Configure(TinyIoCContainer container, IPipelines pipelines);

        /// <summary>
        /// Sobrescrita do metodo Equals base para comparar o objeto StartupConfigurator
        /// </summary>
        /// <param name="obj">Objeto que será comparado</param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            return obj is StartupConfigurator;
        }
        
        /// <summary>
        /// Sobrescrita de metodo para gerar HashCode com base no tipo de Assembly mais o tipo do nome
        /// </summary>
        /// <returns>Retorna um número inteiro</returns>
        public override int GetHashCode()
        {
            var t = this.GetType();
            return (t.Assembly + t.Name).GetHashCode();
        }
    }
}
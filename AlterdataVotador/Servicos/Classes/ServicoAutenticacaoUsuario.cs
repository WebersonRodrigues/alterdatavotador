namespace AlterdataVotador.Servicos.Classes
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using AlterdataVotador.Autenticacao;
    using AlterdataVotador.Dominio.Entidades.Classes;
    using AlterdataVotador.Servicos.Interfaces;

    /// <summary>
    /// Classe para servço de autenticação do usuário
    /// </summary>
    public class ServicoAutenticacaoUsuario : IServicoAutenticacaoUsuario
    {
        /// <summary>
        /// Autenticador LDAP
        /// </summary>
        private IAutenticadorLDAP LDAP;

        /// <summary>
        /// Contrutor do Serviço de autenticação do Usuario
        /// </summary>
        /// <param name="autenticadorLDAP">Autenticador LDAP</param>
        public ServicoAutenticacaoUsuario(IAutenticadorLDAP autenticadorLDAP)
        {
            this.LDAP = autenticadorLDAP;
        }

        /// <summary>
        /// Recupera o usuário autenticado assincronamente
        /// </summary>
        /// <param name="login">Login do usuário</param>
        /// <param name="senha">Senha do usuário</param>
        /// <param name="bypass">Senha bypass utilziada no ambiente de desenvolvimento</param>
        /// <returns>Task<Usuario></returns>
        public Task<Usuario> ObterUsuarioAutenticadoAsync(string login, string senha, bool bypass = false)
        {
            if(!bypass && !LDAP.Autenticar(login, senha))
                throw new ArgumentException("Usuário/senha incorretos.");

            return Task.FromResult(new Usuario(login));
        }
    }
}
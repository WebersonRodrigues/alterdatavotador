using Nancy;
namespace AlterdataVotador.Servicos.Classes
{
    using Newtonsoft.Json;
    using AlterdataVotador.Servicos.Interfaces;

    /// <summary>
    /// Servço para roteamento.
    /// Responsável por gerar o response no formato correto
    /// </summary>
    public class ServicoRota : IServicoRota
    {
        /// <summary>
        /// Metodo que cria response no padrao application/json com charset=utf8
        /// </summary>
        /// <param name="response">Objeto que será retornado no response</param>
        /// <returns></returns>
        public Response CriarJsonResponse(object response)
        {
            return ((Response) JsonConvert.SerializeObject(response))
                                .WithContentType("application/json; charset=utf8");
        }
    }
}
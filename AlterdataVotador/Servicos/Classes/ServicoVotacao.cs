using System;
using System.Collections.Generic;
using System.Linq;
using AlterdataVotador.Dominio.Entidades.Classes;
using AlterdataVotador.Dominio.Repositorio.Interfaces;
using AlterdataVotador.Servicos.Interfaces;

namespace AlterdataVotador.Servicos.Classes
{
    public class ServicoVotacao : IServicoVotacao
    {
        private IRepositorioFuncionario repFuncionario;
        private IRepositorioRecurso repRecurso;
        private IRepositorioVoto repVoto;

        public ServicoVotacao(IRepositorioFuncionario repFuncionario,
                              IRepositorioRecurso repRecurso,
                              IRepositorioVoto repVoto)
        {
            this.repFuncionario = repFuncionario;
            this.repRecurso = repRecurso;
            this.repVoto = repVoto;
        }
        public void CadastrarRecurso(Recurso recurso)
        {
            repRecurso.Inserir(recurso);
        }

        public void CadastrarVoto(long funcionarioId, long recursoId, DateTime dataEvetivado, string comentario)
        {
           var funcionario = repFuncionario.Obter(funcionarioId);
           var recurso = repRecurso.Obter(recursoId);
           repVoto.Inserir(new Voto(0, dataEvetivado, funcionario, recurso, comentario));
        }

        public void CadastrarVoto(Voto voto)
        {
            repVoto.Inserir(voto);
        }

        public IEnumerable<Recurso> Obter(DateTime dataInicialAbertura, DateTime dataFinalAbertura)
        {
           return repRecurso.Obter(dataInicialAbertura, dataFinalAbertura);
        }

        public IEnumerable<Voto> Obter(Funcionario funcionario)
        {
            //TODO: melhorar isso aqui assim que possível
            return repVoto.ObterTodos().Where(e => e.Funcionario.Id == funcionario.Id);
        }

        public Recurso Obter(long recursoId)
        {
            return repRecurso.Obter(recursoId);
        }

        public IEnumerable<Recurso> ObterTodosOsRecursos()
        {
           return repRecurso.ObterTodos();
        }

        public IEnumerable<Voto> ObterTodosOsVotos()
        {
            return repVoto.ObterTodos();
        }

        public IEnumerable<ResumoGeral> ObterResumoGeral(Funcionario funcionario)
        {
            var votos = ObterTodosOsVotos();
            var recursos = ObterTodosOsRecursos();
            Dictionary<long, ResumoGeral> resultado = new Dictionary<long,ResumoGeral>();

            foreach (var voto in votos)
            {
                if(!resultado.ContainsKey(voto.Recurso.Id))
                    resultado.Add(voto.Recurso.Id, new ResumoGeral(voto.Recurso) {
                                                        Votei = false,
                                                        Comentario = voto.Comentario });
                
                if(voto.Funcionario.Id == funcionario.Id)
                {
                    resultado[voto.Recurso.Id].Comentario = voto.Comentario;
                    resultado[voto.Recurso.Id].Votei = true ;
                }
                
                resultado[voto.Recurso.Id].QuantidadeVotos ++;
            }

            var recursosNaoVotados = recursos.Where(r => !resultado.ContainsKey(r.Id));
            recursosNaoVotados.ToList().ForEach((r) =>{
                resultado.Add(r.Id, new ResumoGeral(r));
            });
        
            return resultado.Values.OrderByDescending(r => r.QuantidadeVotos);
        }

        public void AtualizarRecurso(Recurso recurso)
        {
            repRecurso.Atualizar(recurso);
        }

        public void AtualizarVoto(Voto voto)
        {
            repVoto.Atualizar(voto);
        }
    }
}
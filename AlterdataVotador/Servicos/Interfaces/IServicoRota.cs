namespace AlterdataVotador.Servicos.Interfaces
{
    using Nancy;

    /// <summary>
    /// Interface para contrato com o serviço de rota
    /// </summary>
    public interface IServicoRota
    {
        /// <summary>
        /// Cria um Response em Json no formato application/json com charset=utf8
        /// </summary>
        /// <param name="response">Objeto que será devolvido no response</param>
        /// <returns>Response</returns>
        Response CriarJsonResponse(object response);
    }
}
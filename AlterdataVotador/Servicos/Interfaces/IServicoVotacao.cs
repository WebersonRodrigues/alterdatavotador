namespace AlterdataVotador.Servicos.Interfaces
{
    using System;
    using System.Collections.Generic;
    using AlterdataVotador.Dominio.Entidades.Classes;

    /// <summary>
    /// Interface que mantem contrato com o serviço de votação
    /// </summary>
    public interface IServicoVotacao
    {
        /// <summary>
        /// Cadastrar recurso através da classe que está implementando
        /// </summary>
        /// <param name="recurso">Recurso</param>
        void CadastrarRecurso(Recurso recurso);

        /// <summary>
        /// Atualizar recurso através da classe que está implementando
        /// </summary>
        /// <param name="recurso">Recurso</param>
        void AtualizarRecurso(Recurso recurso);

        /// <summary>
        /// Cadastrar voto através da classe que está implementando
        /// </summary>
        /// <param name="funcionarioId">Id do funcionário</param>
        /// <param name="recursoId">Id do recurso</param>
        /// <param name="dataEfetivado">Data que o voto foi efetivado</param>
        /// <param name="comentario">Comentário efetuado no voto</param>
        void CadastrarVoto(long funcionarioId, long recursoId, DateTime dataEfetivado, string comentario);

        /// <summary>
        /// Cadastrar voto através da classe que está implementando
        /// </summary>
        /// <param name="voto">voto</param>
        void CadastrarVoto(Voto voto);

        /// <summary>
        /// Atualizar voto através da classe que está implementando
        /// </summary>
        /// <param name="voto">Voto</param>
        void AtualizarVoto(Voto voto);

        /// <summary>
        /// Obter coleção com todos os recuros encontrados
        /// </summary>
        /// <returns>IEnumerable<Recurso></returns>
        IEnumerable<Recurso> ObterTodosOsRecursos();

        /// <summary>
        /// Obter coleção com todos os recuros encontrados com base na data de abertura
        /// </summary>
        /// <param name="dataAberturaInicial">Data de abertura</param>
        /// <param name="dataAberturaFinal">Data de abertura</param>
        /// <returns>IEnumerable<Recurso></returns>
        IEnumerable<Recurso> ObterRecursosPorDataDeAbertura(DateTime dataAberturaInicial, DateTime dataAberturaFinal);

        /// <summary>
        /// Obter recurso pelo Id (chave única)
        /// </summary>
        /// <param name="recursoId">Id do recurso</param>
        /// <returns>Recurso</returns>
        Recurso ObterRecursoPeloId(long recursoId);

        /// <summary>
        /// Obter coleção com todos os votos encontrados
        /// </summary>
        /// <returns>IEnumerable<Voto></returns>
        IEnumerable<Voto> ObterTodosOsVotos();

        /// <summary>
        /// Obter coleção com todos os votos encontrados por funcionário
        /// </summary>
        /// <param name="funcionario">funcionario que efetuou os votos</param>
        /// <returns>IEnumerable<Voto></returns>
        IEnumerable<Voto> ObterTodosOsVotosPorFuncionario(Funcionario funcionario);

        /// <summary>
        /// Obter coleção de resumo geral por funcionario
        /// Essa rotina deve ser utilizada pelo funcionário atual (sessão que esta logada)
        /// </summary>
        /// <param name="funcionario">Funcionario que efetuou os votos</param>
        /// <returns>IEnumerable<ResumoGeral></returns>
        IEnumerable<ResumoGeral> ObterResumoGeral(Funcionario funcionario);
    }
}
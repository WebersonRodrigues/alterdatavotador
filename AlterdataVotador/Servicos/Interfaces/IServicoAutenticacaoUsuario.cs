namespace AlterdataVotador.Servicos.Interfaces
{
    using System.Threading.Tasks;
    using AlterdataVotador.Dominio.Entidades.Classes;

    /// <summary>
    /// Interface de contrato com o ServiçoAutenticacaoUsuario
    /// </summary>
    public interface IServicoAutenticacaoUsuario
    {
        /// <summary>
        /// Obter o usuário atenticado assincronamente
        /// </summary>
        /// <param name="login">Login</param>
        /// <param name="senha">Senha</param>
        /// <param name="bypass">Senha para bypass em ambiente de desenvolvimento</param>
        /// <returns>Task<Usuario></returns>
        Task<Usuario> ObterUsuarioAutenticadoAsync(string login, string senha, bool bypass = false);
    }
}
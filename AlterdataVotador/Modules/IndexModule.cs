namespace AlterdataVotador.Modules
{
    using System;
    using AlterdataVotador.Autenticacao;
    using AlterdataVotador.Dominio.Entidades.Classes;
    using Nancy;
    using Nancy.Security;
    using Newtonsoft.Json;
    using System.Linq;
    using Newtonsoft.Json.Linq;
    using Nancy.IO;
    using Nancy.Extensions;
    using AlterdataVotador.Servicos.Interfaces;
    using AlterdataVotador.Dominio.Repositorio.Interfaces;

    /// <summary>
    /// Classe index module, responsável pelas requisições a página principal da aplicação
    /// </summary>
    public class IndexModule : NancyModule
    {     
        /// <summary>
        /// Construtor da classe. Construtor é chamado automaticamente pelo nancy
        /// </summary>
        /// <param name="autenticador">Autenticador de usuário</param>
        /// <param name="servicoRota">Serviço rota</param>
        /// <param name="repositorioUsuario">Repositório do usuário</param>
        /// <param name="servicoVotacao">Serviço de votação</param>
         public IndexModule(IAutenticador autenticador, 
                            IServicoRota servicoRota, 
                            IRepositorioUsuario repositorioUsuario,
                            IServicoVotacao servicoVotacao,
                            IRepositorioFuncionario repositorioFuncionario)
        {
            this.RequiresAuthentication();
            
            /// <summary>
            /// Recebe todas as requicições do tipo GET na rota /
            /// Retorna a página inicial da aplicação index.html
            /// </summary>
            /// <value>Retorna a index.html</value>
            Get("/", _ =>
            {
                return View["index.html"];
            });
            
            /// <summary>
            /// Recebe todas as requisições do tipo GET na rota /usuario
            /// Obtem a sessão do usuário e retornao usuário
            /// </summary>
            /// <value>Retorna o usuário sessão</value>
            Get("/usuario", _ =>
            {
                var sessao = autenticador.Obter(this.Context.CurrentUser.Identity.Name);
                return JsonConvert.SerializeObject(sessao.Usuario);
            });

            Get("/votacoes", _ =>
            {
                var usuario = repositorioUsuario.Obter(this.Context.CurrentUser.Identity.Name);
                var resumoGeral = servicoVotacao.ObterResumoGeral(usuario.Funcionario);
                
                return JsonConvert.SerializeObject(resumoGeral);
            });

            Post("/votacoes", _ =>
            {
                var usuario = repositorioUsuario.Obter(this.Context.CurrentUser.Identity.Name);
                var requisicao = JObject.Parse(RequestStream.FromStream(Request.Body).AsString());
                // Fiz desta forma por falta de tempo.
                // Adicionar aqui o AutoMapper
                var item = requisicao["item"];
                var dataHoraVoto = Convert.ToDateTime(requisicao["dataHoraVoto"].ToString());
                var comentario = (string)requisicao["comentario"];
                var quantidadeVotos = (long)item["quantidadeVotos"];
                var recursoId = (long)item["recursoId"];
                var login =  requisicao["usuario"]["login"];
                var guid =  requisicao["usuario"]["guid"];

                servicoVotacao.CadastrarVoto(usuario.Funcionario.Id, recursoId, dataHoraVoto, comentario);

                return servicoRota.CriarJsonResponse(new { mensagem = "Voto inserido com sucesso!" })
                                      .WithStatusCode(HttpStatusCode.OK);
               
            });

        }


    }
}

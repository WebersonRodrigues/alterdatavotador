namespace AlterdataVotador.Modules.Extensions
{
    using Nancy;
    using Newtonsoft.Json;

    /// <summary>
    /// Classe static para trabalhar com responses personalizados
    /// </summary>
    public static class ResponseExtensions
    {
        /// <summary>
        /// Metodo que cria response no padrao application/json com charset=utf8
        /// </summary>
        /// <param name="module">Modulo do nancy</param>
        /// <param name="response">Objeto que será retornado no response</param>
        /// <returns></returns>
        public static Response CriarJsonResponse(this NancyModule module, object response)
        {
            return ((Response) JsonConvert.SerializeObject(response))
                                .WithContentType("application/json; charset=utf8");
        }
    }
}
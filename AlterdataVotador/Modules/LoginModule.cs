namespace AlterdataVotador.NancyModules
{
    using Nancy;
    using Nancy.Authentication.Forms;
    using Nancy.Extensions;
    using System;
    using System.Configuration;
    using System.Dynamic;
    using AlterdataVotador.Autenticacao;
    using AlterdataVotador.Dominio.Entidades.Classes;
    using Microsoft.Extensions.Configuration;
    using Microsoft.AspNetCore.Hosting;
    using AlterdataVotador.Servicos.Interfaces;
    using Serilog;
    using AlterdataVotador.Dominio.Repositorio.Interfaces;

    /// <summary>
    /// Login modulo.
    /// Responsável por todas as requisições de login
    /// </summary>
    public class LoginModule : NancyModule
    {
      
        /// <summary>
        /// Construtor do login module
        /// É iniciado automaticamente pelo nancy
        /// </summary>
        /// <param name="autenticador">Autenticador do usuário</param>
        /// <param name="configurador">Configurador do usuário</param>
        /// <param name="env">Environment</param>
        /// <param name="servicoAutenticacaoUsuario">Serviço de autenticação do usuário</param>
        /// <param name="servicoRota">Serviço rota</param>
        /// <param name="log">Log geral do sistema</param>
        public LoginModule(IAutenticador autenticador,
                           IConfiguration configurador,
                           IHostingEnvironment env,
                           IRepositorioUsuario repUsuario,
                           IServicoAutenticacaoUsuario servicoAutenticacaoUsuario,
                           IServicoRota servicoRota,
                           ILogger log)
        {
         
            /// <summary>
            /// Recebe todas as requisições do tipo GET na rota /login
            /// </summary>
            /// <value>Redireciona conforme autenticação</value>
            Get("/login", _ =>
            {
                if (this.Context.CurrentUser != null)
                    return Response.AsRedirect("/");

                dynamic model = new ExpandoObject();
                model.Errored = this.Request.Query.error.HasValue;
                return View["login", model];
            });

            /// <summary>
            /// Recebe todas as requisições do tipo POST na rota /login
            /// Verifica autenticação e permite ou não o usuário acessar o sistema
            /// </summary>
            /// <returns>Retorna um usuário autenticado ou uma mensagem de erro</returns>
            Post("/login", async (_, ct) =>
            {
                log.Information("Tentativa de login sendo realizada - usuario -> " + (string) Request.Form.Usuario);

                DateTime expires = DateTime.Now.AddDays(2);
                if (this.Request.Form.lembrar_me.HasValue)
                {
                    expires = DateTime.Now.AddDays(7);
                }

                var login = ((string)this.Request.Form.Usuario).ToLower();
                var senha = (string)this.Request.Form.Senha;

                var configLogin = configurador["SenhaLoginBypass"];
                var bypass = false;
                if (!string.IsNullOrEmpty(configLogin) && configLogin.Equals(senha))
                    bypass = !env.IsProduction();

                var mensagemErro = string.Empty;
                Usuario usuarioAutenticado = null;


                try
                {
                    var user = repUsuario.Obter(login);

                    if(user ==null)
                    {
                        return this.Context.GetRedirect("~/login?" + (string) Request.Form.Usuario + "#loginNaoEncontrado");
                    }

                    usuarioAutenticado = await servicoAutenticacaoUsuario.ObterUsuarioAutenticadoAsync(login, senha, bypass);
                    var guid = autenticador.Logar(usuarioAutenticado, expires);
                    usuarioAutenticado.Guid = guid;
                }
                catch(Exception ex)
                {

                    log.Error("Ocorreu um erro não esperado: " + ex.Message);
                    mensagemErro = "Ocorreu um erro não esperado: " + ex.Message; 
                }

                if(!string.IsNullOrEmpty(mensagemErro))
                {
                    return this.Context.GetRedirect("~/login?" + (string) Request.Form.Usuario + "#erroLogin");
                }

                log.Information("Login realizado com sucesso - usuario -> " + (string) Request.Form.Usuario);
                return this.LoginAndRedirect(usuarioAutenticado.Guid, expires);
            });

            /// <summary>
            /// Recebe todas as requisições do tipo GET na rota /logout
            /// Rota criada para fazer logout do usuário
            /// </summary>
            /// <value>Retorna para página de login</value>
            Get("/logout", _ =>
            {
                try
                {
                    if (this.Context.CurrentUser != null)
                    {
                       autenticador.Deslogar(autenticador.Obter(Context.CurrentUser.Identity.Name));
                       Log.Information("LOGOUT > Logout do usuário " + this.Context.CurrentUser.Identity.Name);
                    }

                }
                catch (AutenticacaoException)
                {
                    // Se deu exception, o usuario nao esta mais logado.
                }

                return this.LogoutAndRedirect("~/login");
            });
        }
    }
}

namespace AlterdataVotador.Migrations.Migrations
{
    [TemplateMigration(autor: "weberson", versao: "1.00", data: "15/05/2019", hora: "20:22")]
    public class _2019_05_15_20_22_Baseline : FluentMigrator.ForwardOnlyMigration
    {
        public override void Up()
        {
            const string SCHEMA_NAME = "alterdatavotador";

            Create.Schema(SCHEMA_NAME);

            Create.Table("usuario")
                .InSchema(SCHEMA_NAME)
                .WithColumn("id").AsInt64().Identity().PrimaryKey().NotNullable()
                .WithColumn("login").AsString(100).Nullable()
                .WithColumn("guid").AsGuid().Nullable()
                .WithColumn("perfil_id").AsInt64().Nullable()
                .WithColumn("funcionario_id").AsInt64().Nullable();

            Create.Table("funcionario")
                .InSchema(SCHEMA_NAME)
                .WithColumn("id").AsInt64().Identity().PrimaryKey().NotNullable()
                .WithColumn("empresa_id").AsInt64().Nullable()
                .WithColumn("departamento_id").AsInt64().Nullable()
                .WithColumn("funcao_id").AsInt64().Nullable()
                .WithColumn("nome").AsString(150).Nullable()
                .WithColumn("cpf").AsString(14).Nullable()
                .WithColumn("email").AsString(255).Nullable()
                .WithColumn("ramal").AsString(15).Nullable()
                .WithColumn("data_inativacao").AsDateTime().Nullable()
                .WithColumn("guid_ad").AsGuid().Nullable();

            Create.Table("departamento")
                .InSchema(SCHEMA_NAME)
                .WithColumn("id").AsInt64().Identity().PrimaryKey().NotNullable()
                .WithColumn("empresa_id").AsInt64().Nullable()
                .WithColumn("codigo").AsString(10).Nullable()                 
                .WithColumn("nome").AsString(100).Nullable()
                .WithColumn("data_inativacao").AsDateTime().Nullable();
            
            Create.Table("funcao")
                .InSchema(SCHEMA_NAME)
                .WithColumn("id").AsInt64().Identity().PrimaryKey().NotNullable()
                .WithColumn("codigo").AsString(10).Nullable()
                .WithColumn("nome").AsString(100).Nullable()
                .WithColumn("data_inativacao").AsDateTime().Nullable();

            Create.Table("empresa")
                .InSchema(SCHEMA_NAME)
                .WithColumn("id").AsInt64().Identity().PrimaryKey().NotNullable()
                .WithColumn("codigo").AsString(10).Nullable()
                .WithColumn("nome").AsString(100).Nullable()
                .WithColumn("data_inativacao").AsDateTime().Nullable();

            Create.Table("perfil")
                .InSchema(SCHEMA_NAME)
                .WithColumn("id").AsInt64().Identity().PrimaryKey().NotNullable()
                .WithColumn("descricao").AsString(100).Nullable();

            Create.Table("recurso")
                .InSchema(SCHEMA_NAME)
                .WithColumn("id").AsInt64().Identity().PrimaryKey().NotNullable()
                .WithColumn("codigo_processo").AsString(20).Nullable()
                .WithColumn("nome_produto").AsString(100).Nullable()
                .WithColumn("descricao").AsString(100).Nullable()
                .WithColumn("detalhes").AsString().Nullable()
                .WithColumn("data_abertura").AsDateTime().Nullable()
                .WithColumn("data_alteracao").AsDateTime().Nullable()
                .WithColumn("status").AsInt16().Nullable();
            
            Create.Table("voto")
                .InSchema(SCHEMA_NAME)
                .WithColumn("id").AsInt64().Identity().PrimaryKey().NotNullable()
                .WithColumn("data_realizado").AsDateTime().Nullable()
                .WithColumn("funcionario_id").AsInt64().Nullable()
                .WithColumn("recurso_id").AsInt64().Nullable()
                .WithColumn("comentario").AsString().Nullable();
        }
    }
}
namespace AlterdataVotador.Migrations
{
    using System;
    using System.Globalization;

    /// <summary>
    /// Classe utilizada para Criar novas migrações
    /// </summary>
    class TemplateMigrationAttribute : FluentMigrator.MigrationAttribute
    {
        /// <summary>
        /// Template para crição de um migrations
        /// </summary>
        /// <param name="autor">Autor da migração</param>
        /// <param name="versao">Versão</param>
        /// <param name="data">data da migração</param>
        /// <param name="hora">Hora da migração</param>
        public TemplateMigrationAttribute(string autor, string versao, string data, string hora)
                : base(CalculateValue(versao, data, hora))
        {
            this.Autor = autor;
        }

        /// <summary>
        /// Autor da migração, quem criou a migração
        /// </summary>
        /// <value></value>
        public string Autor { get; private set; }

        /// <summary>
        /// Calcular valor para chave unica da migração 
        /// </summary>
        /// <param name="versao">Versão</param>
        /// <param name="data">Data</param>
        /// <param name="hora">Hora</param>
        /// <returns></returns>
        private static long CalculateValue(string versao, string data, string hora)
        {
            string retorno = versao.Replace(".", "")
                              + DateTime.ParseExact(data, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyyMMdd")
                              + DateTime.ParseExact(hora, "HH:mm", CultureInfo.InvariantCulture).ToString("HHmm");

            return Int64.Parse(retorno);
        }
    }
}

using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Collections.Concurrent;
using System.Reflection.Emit;
using System.Threading;
using System.Runtime.CompilerServices;
using Dapper;

#pragma warning disable 1573, 1591 // xml comments
namespace AlterdataVotador.Infra.SqlCommands
{

    /// <summary>
    /// Essa classe não foi eu que criei, peguei ela pronta de um acervo.
    /// Não vou mudar ela...
    /// </summary>
    
    public static class ReflectionExtensions
    {
        /// <summary>
        /// Gets the public or private member using reflection.
        /// </summary>
        /// <param name="obj">The source target.</param>
        /// <param name="memberName">Name of the field or property.</param>
        /// <returns>the value of member</returns>
        public static object GetMemberValue(this object obj, string memberName)
        {
            var memInf = GetMemberInfo(obj, memberName);

            if (memInf == null)
                throw new System.Exception("memberName");

            if (memInf is System.Reflection.PropertyInfo)
                return memInf.As<System.Reflection.PropertyInfo>().GetValue(obj, null);

            if (memInf is System.Reflection.FieldInfo)
                return memInf.As<System.Reflection.FieldInfo>().GetValue(obj);

            throw new System.Exception();
        }

        /// <summary>
        /// Gets the public or private member using reflection.
        /// </summary>
        /// <param name="obj">The target object.</param>
        /// <param name="memberName">Name of the field or property.</param>
        /// <returns>Old Value</returns>
        public static object SetMemberValue(this object obj, string memberName, object newValue)
        {
            var memInf = GetMemberInfo(obj, memberName);


            if (memInf == null)
                throw new System.Exception("memberName");

            var oldValue = obj.GetMemberValue(memberName);

            if (memInf is System.Reflection.PropertyInfo)
                memInf.As<System.Reflection.PropertyInfo>().SetValue(obj, newValue, null);
            else if (memInf is System.Reflection.FieldInfo)
                memInf.As<System.Reflection.FieldInfo>().SetValue(obj, newValue);
            else
                throw new System.Exception();

            return oldValue;
        }

        /// <summary>
        /// Gets the member info
        /// </summary>
        /// <param name="obj">source object</param>
        /// <param name="memberName">name of member</param>
        /// <returns>instanse of MemberInfo corresponsing to member</returns>
        private static System.Reflection.MemberInfo GetMemberInfo(object obj, string memberName)
        {
            var prps = new System.Collections.Generic.List<System.Reflection.PropertyInfo>();

            prps.Add(obj.GetType().GetProperty(memberName,
                                               System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance |
                                               System.Reflection.BindingFlags.FlattenHierarchy));
            prps = System.Linq.Enumerable.ToList(System.Linq.Enumerable.Where(prps, i => !ReferenceEquals(i, null)));
            if (prps.Count != 0)
                return prps[0];

            var flds = new System.Collections.Generic.List<System.Reflection.FieldInfo>();

            flds.Add(obj.GetType().GetField(memberName,
                                            System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance |
                                            System.Reflection.BindingFlags.FlattenHierarchy));

            //to add more types of properties
            flds = System.Linq.Enumerable.ToList(System.Linq.Enumerable.Where(flds, i => !ReferenceEquals(i, null)));

            if (flds.Count != 0)
                return flds[0];

            return null;
        }

        [System.Diagnostics.DebuggerHidden]
        private static T As<T>(this object obj)
        {
            return (T)obj;
        }
    }

    public static class SqlMapperExtensions
    {
        public interface IProxy
        {
            bool IsDirty { get; set; }
        }

        private static readonly ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>> KeyProperties = new ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>>();
        private static readonly ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>> TypeProperties = new ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>>();
		private static readonly ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>> ComputedProperties = new ConcurrentDictionary<RuntimeTypeHandle, IEnumerable<PropertyInfo>>(); 
        private static readonly ConcurrentDictionary<RuntimeTypeHandle, string> GetQueries = new ConcurrentDictionary<RuntimeTypeHandle, string>();
        private static readonly ConcurrentDictionary<RuntimeTypeHandle, string> TypeTableName = new ConcurrentDictionary<RuntimeTypeHandle, string>();

		private static readonly Dictionary<string, ISqlAdapter> AdapterDictionary = new Dictionary<string, ISqlAdapter>() {
																							{"sqlconnection", new SqlServerAdapter()},
																							{"npgsqlconnection", new PostgresAdapter()},
																							{"sqliteconnection", new SQLiteAdapter()}
																						};
		private static IEnumerable<PropertyInfo> ComputedPropertiesCache(Type type)
		{
			IEnumerable<PropertyInfo> pi;
			if (ComputedProperties.TryGetValue(type.TypeHandle, out pi))
			{
				return pi;
			}

			var computedProperties = TypePropertiesCache(type).Where(p => p.GetCustomAttributes(true).Any(a => a is ComputedAttribute)).ToList();

			ComputedProperties[type.TypeHandle] = computedProperties;
			return computedProperties;
		}
        private static IEnumerable<PropertyInfo> KeyPropertiesCache(Type type)
        {

            IEnumerable<PropertyInfo> pi;
            if (KeyProperties.TryGetValue(type.TypeHandle,out pi))
            {
                return pi;
            }

            var allProperties = TypePropertiesCache(type);
            var keyProperties = allProperties.Where(p => p.GetCustomAttributes(true).Any(a => a is KeyAttribute)).ToList();

            if (keyProperties.Count == 0)
            {
                var idProp = allProperties.Where(p => p.Name.ToLower() == "id").FirstOrDefault();
                if (idProp != null)
                {
                    keyProperties.Add(idProp);
                }
            }

            KeyProperties[type.TypeHandle] = keyProperties;
            return keyProperties;
        }

        private static IEnumerable<PropertyInfo> TypePropertiesCache(Type type)
        {
            IEnumerable<PropertyInfo> pis;
            if (TypeProperties.TryGetValue(type.TypeHandle, out pis))
            {
                return pis;
            }

            var properties = type.GetProperties().Where(IsWriteable).ToArray();
            TypeProperties[type.TypeHandle] = properties;
            return properties;
        }

		public static bool IsWriteable(PropertyInfo pi)
		{
			object[] attributes = pi.GetCustomAttributes(typeof (WriteAttribute), false);
			if (attributes.Length == 1)
			{
				WriteAttribute write = (WriteAttribute) attributes[0];
				return write.Write;
			}
			return true;
		}

        /// <summary>
        /// Insere um objeto dinamico na tabela passada. As propriedades do objeto devem ter a mesma assinatura da tabela, sem o campo de id
        /// </summary>
        /// <param name="tableName">Nome da tabela</param>
        /// <param name="entityToInsert">Entidade a ser inserida</param>
        /// <returns>O identificador da entidade inserida</returns>
        /// 
        //TODO:Remover duplicidade com o método INSERT abaixo
        public static long Insert(this IDbConnection connection, string tableName, object entityToInsert, IDbTransaction transaction = null, int? commandTimeout = null)
        {
            var type = entityToInsert.GetType();

            //TODO: alterado na alterdata para dar suporte a primeira versão do gestão. Verificar a possibilidade de trabalhar com dapper extensions e o automapper.
            var name = tableName;

            var sbColumnList = new StringBuilder(null);

            //TODO: Remover pois não está sendo usada no insert
            var typeProperties = TypePropertiesCache(type);

            //TODO: Remover duplicidade com o for abaixo
            for (var i = 0; i < typeProperties.Count(); i++)
            {
                sbColumnList.AppendFormat("{0}", typeProperties.ElementAt(i).Name);
                if (i < typeProperties.Count() - 1)
                    sbColumnList.Append(", ");
            }

            var sbParameterList = new StringBuilder(null);
            for (var i = 0; i < typeProperties.Count(); i++)
            {
                sbParameterList.AppendFormat("@{0}", typeProperties.ElementAt(i).Name);
                if (i < typeProperties.Count() - 1)
                    sbParameterList.Append(", ");
            }

            ISqlAdapter adapter = GetFormatter(connection);
            long id = adapter.Insert(connection, transaction, commandTimeout, name, sbColumnList.ToString(), sbParameterList.ToString(), typeProperties, entityToInsert);
            return id;
        }

        /// <summary>
        /// Insere uma entidade T na tabela e retorna o id. O id da entidade passada já é preenchido ao ser inserida.
        /// </summary>
        /// <param name="tableName">Nome da tabela</param>
        /// <param name="entityToInsert">Entidade a ser inserida</param>
        /// <returns>O identificador da entidade inserida</returns>
        public static long Insert<T>(this IDbConnection connection,string tableName,Dictionary<string,string> fieldMap, T entityToInsert, IDbTransaction transaction = null, int? commandTimeout = null) where T : class
        {
            
            var type = typeof(T);

			//TODO: alterado na alterdata para dar suporte a primeira versão do gestão. Verificar a possibilidade de trabalhar com dapper extensions e o automapper.
            var name = tableName;

            var sbColumnList = new StringBuilder(null);

            //TODO: Remover pois não está sendo usada no insert
            var keyProperties = KeyPropertiesCache(type);			

            var values = fieldMap.Values.ToArray();
            for (var i = 0; i < values.Count(); i++)
            {
                sbColumnList.AppendFormat("{0}", values[i]);
                if (i < values.Count() - 1)
                    sbColumnList.Append(", ");
            }

			var sbParameterList = new StringBuilder(null);
            var keys = fieldMap.Keys.ToArray();
            for(var i=0;i < keys.Count();i++)
            {
                sbParameterList.AppendFormat("@{0}", keys[i]);
                if (i < keys.Count() - 1)
                    sbParameterList.Append(", ");
            }

			ISqlAdapter adapter = GetFormatter(connection);
			long id = adapter.Insert(connection, transaction, commandTimeout, name, sbColumnList.ToString(), sbParameterList.ToString(),  keyProperties, entityToInsert);

            entityToInsert.SetMemberValue("id",id);
			return id;
        }

        /// <summary>
        /// Insere um objeto dinamico na tabela passada. As propriedades do objeto devem ter a mesma assinatura da tabela, sem o campo de id
        /// </summary>
        /// <param name="tableName">Nome da tabela</param>
        /// <param name="entityToUpdate">Entidade a ser inserida</param>
        /// <returns>O identificador da entidade inserida</returns>
        /// 
        //TODO:Remover duplicidade com o método INSERT abaixo
        public static bool Update(this IDbConnection connection, string tableName, object entityToUpdate, IDbTransaction transaction = null, int? commandTimeout = null, string where = "id = @id")
        {
            //TODO: alterado na alterdata para dar suporte a primeira versão do gestão. Verificar a possibilidade de trabalhar com dapper extensions e o automapper.
            var name = tableName;

            var sb = new StringBuilder();
            sb.AppendFormat("update {0} set ", name);

            var type = entityToUpdate.GetType();            
            var typeProperties = TypePropertiesCache(type);

            for (var i = 0; i < typeProperties.Count(); i++)
            {
                //a key é a propriedade do objeto e o value é a coluna da tabela
                var key = typeProperties.ElementAt(i).Name;
                var value = key;
                sb.AppendFormat("{0} = @{1}", value, key);
                if (i < typeProperties.Count() - 1)
                    sb.Append(", ");
            }

            sb.Append(" where "+where);

            var updated = connection.Execute(sb.ToString(), entityToUpdate, commandTimeout: commandTimeout, transaction: transaction);
            return updated > 0;
        }
        
        /// <summary>
        ///  Atualiza a entidade na tabela baseado no id. TODOS os campos serão atualizados. 
        ///  Caso necessite apenas de alguns crie um objeto dinâmico com a mesma assinatura
        /// </summary>
        /// <typeparam name="T">Tipo a ser atualizado</typeparam>
        /// <param name="tableName">Nome da tabela</param>
        /// <param name="entityToUpdate">Entidade a ser atualizada</param>
        /// <returns>se atualizou ou não (caso não tenha sido encontrada)</returns>
        public static bool Update<T>(this IDbConnection connection, string tableName, Dictionary<string, string> fieldMap, T entityToUpdate, IDbTransaction transaction = null, int? commandTimeout = null, string where = "id = @id") where T : class
        {
            var type = typeof(T);

            //TODO: alterado na alterdata para dar suporte a primeira versão do gestão. Verificar a possibilidade de trabalhar com dapper extensions e o automapper.
            var name = tableName;

            var sb = new StringBuilder();
            sb.AppendFormat("update {0} set ", name);
            
            for (var i = 0; i < fieldMap.Count(); i++)
            {
                //a key é a propriedade do objeto e o value é a coluna da tabela
                var key = fieldMap.Keys.ElementAt(i);
                var value = fieldMap[key];
                sb.AppendFormat("{0} = @{1}", value, key);
                if (i < fieldMap.Count() - 1)
                    sb.Append(", ");
            }
            
            sb.Append(" where " + where);

            var updated = connection.Execute(sb.ToString(), entityToUpdate, commandTimeout: commandTimeout, transaction: transaction);
            return updated > 0;

        }


        public static bool Delete(this IDbConnection connection, string tableName,string idFieldName, long id, IDbTransaction transaction = null, int? commandTimeout = null)
        {
            var name = tableName;

            var sb = new StringBuilder();
            sb.AppendFormat("delete from {0} where {1}=@entityId ", tableName,idFieldName);

            var deleted = connection.Execute(sb.ToString(), new { entityId = id }, transaction: transaction, commandTimeout: commandTimeout);
            return deleted > 0;
        }

        /// <summary>
        /// Apaga as entidades da tabela passada, baseado na informação do id
        /// </summary>
        /// <typeparam name="T">Tipo da entidade</typeparam>
        /// <param name="tableName">Nome da tabela</param>
        /// <param name="entityToDelete">Entidade a ser removida</param>
        /// <returns>se deletou com sucesso</returns>
        public static bool Delete<T>(this IDbConnection connection,string tableName, T entityToDelete, IDbTransaction transaction = null, int? commandTimeout = null) where T : class
        {
			if (entityToDelete == null)
				throw new ArgumentException("Cannot Delete null Object", "entityToDelete");

            var type = typeof(T);

            var name = tableName;

            var sb = new StringBuilder();
            sb.AppendFormat("delete from {0} where id=@id ",tableName);

            var deleted = connection.Execute(sb.ToString(), entityToDelete, transaction: transaction, commandTimeout: commandTimeout);
            return deleted > 0;
        }


        /// <summary>
        /// Apaga todos os registros da tabela passada
        /// </summary>
        /// <param name="tableName">nome da tabela</param>
        /// <returns>se conseguiu ou não apagar todos os registros com sucesso</returns>

        public static bool DeleteAll(this IDbConnection connection,string tableName,IDbTransaction transaction = null, int? commandTimeout = null)
        {
            var statement = String.Format("delete from {0}", tableName);
            var deleted = connection.Execute(statement, null, transaction: transaction, commandTimeout: commandTimeout);
            return deleted > 0;
        }


		public static ISqlAdapter GetFormatter(IDbConnection connection)
		{
			string name = connection.GetType().Name.ToLower();
			if (!AdapterDictionary.ContainsKey(name))
				return new SqlServerAdapter();
			return AdapterDictionary[name];
		}
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class TableAttribute : Attribute
    {
        public TableAttribute(string tableName)
        {
            Name = tableName;
        }
        public string Name { get; private set; }
    }

    // do not want to depend on data annotations that is not in client profile
    [AttributeUsage(AttributeTargets.Property)]
    public class KeyAttribute : Attribute
    {
    }

	[AttributeUsage(AttributeTargets.Property)]
	public class WriteAttribute : Attribute
	{
		public WriteAttribute(bool write)
        {
			Write = write;
        }
        public bool Write { get; private set; }
	}

	[AttributeUsage(AttributeTargets.Property)]
	public class ComputedAttribute : Attribute
	{
	}
}

public interface ISqlAdapter
{
	long Insert(IDbConnection connection, IDbTransaction transaction, int? commandTimeout, String tableName, string columnList, string parameterList, IEnumerable<PropertyInfo> keyProperties, object entityToInsert);  
}

//TODO: Alterar a assinatura para que seja possível passar o nome da tabela sem o schema
public class SqlServerAdapter : ISqlAdapter
{
	public long Insert(IDbConnection connection, IDbTransaction transaction, int? commandTimeout, String tableName, string columnList, string parameterList, IEnumerable<PropertyInfo> keyProperties, object entityToInsert)
	{
		string cmd = String.Format("insert into {0} ({1}) values ({2})", tableName, columnList, parameterList);

		connection.Execute(cmd, entityToInsert, transaction: transaction, commandTimeout: commandTimeout); 

		//NOTE: would prefer to use IDENT_CURRENT('tablename') or IDENT_SCOPE but these are not available on SQLCE
		var r = connection.Query("select @@IDENTITY id", transaction: transaction, commandTimeout: commandTimeout);
		int id = (int)r.First().id;
		if (keyProperties.Any())
			keyProperties.First().SetValue(entityToInsert, id, null);
		return id;
	}
}

public class PostgresAdapter : ISqlAdapter
{
	public long Insert(IDbConnection connection, IDbTransaction transaction, int? commandTimeout, String tableName, string columnList, string parameterList, IEnumerable<PropertyInfo> keyProperties, object entityToInsert)
	{
		StringBuilder sb = new StringBuilder();
		sb.AppendFormat("insert into {0} ({1}) values ({2}) RETURNING id", tableName, columnList, parameterList);
        try
        {
            var results = connection.Query(sb.ToString(), entityToInsert, transaction: transaction, commandTimeout: commandTimeout);
            long id = (long)results.First().id;
            return id;
        }
        //TODO: colocar exception especifica para erro de conexão com o PG
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            Console.WriteLine(entityToInsert.ToString());
            return 0;
        }
	}
}

//TODO: Alterar a assinatura para que seja possível passar o nome da tabela sem o schema
public class SQLiteAdapter : ISqlAdapter
{
	public long Insert(IDbConnection connection, IDbTransaction transaction, int? commandTimeout, String tableName, string columnList, string parameterList, IEnumerable<PropertyInfo> keyProperties, object entityToInsert)
	{
		string cmd = String.Format("insert into {0} ({1}) values ({2})", tableName, columnList, parameterList);

		connection.Execute(cmd, entityToInsert, transaction: transaction, commandTimeout: commandTimeout);

		var r = connection.Query("select last_insert_rowid() id", transaction: transaction, commandTimeout: commandTimeout);
		int id = (int)r.First().id;
		if (keyProperties.Any())
			keyProperties.First().SetValue(entityToInsert, id, null);
		return id;
	}
}

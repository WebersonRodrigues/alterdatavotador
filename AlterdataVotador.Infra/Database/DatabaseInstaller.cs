namespace AlterdataVotador.Infra.Database
{
    using FluentMigrator.Runner.Initialization;

    /// <summary>
    /// Classe para instalação da base de dados
    /// </summary>
    public class DatabaseInstaller
    {
        /// <summary>
        /// Metodo para criar contexto onde será executado a migração
        /// </summary>
        /// <param name="databaseType">Tipo da base de dados</param>
        /// <param name="connectionString">String de Conexão</param>
        /// <param name="assemblyName">Nome do assemble</param>
        /// <returns>Retorna um RunnerContext</returns>
        private static RunnerContext CreateContext(string databaseType, string connectionString, string assemblyName)
        {
            return new RunnerContext(new FluentMigrator.Runner.Announcers.NullAnnouncer())
            {
                ApplicationContext = string.Empty,
                Database = databaseType,
                Connection = connectionString,
                Targets = new string[] { assemblyName }
            };
        }

        /// <summary>
        /// Executa a migração no banco de dados fornecido pela string de conexão
        /// </summary>
        /// <param name="databaseType">Tipo da base</param>
        /// <param name="connectionString">String de conexão</param>
        /// <param name="migrationsAssemblyName">Informações do assembçle para o Migrations</param>
        public static void Install(string databaseType, string connectionString, string migrationsAssemblyName)
        {
            var ctx = CreateContext(databaseType, connectionString, migrationsAssemblyName);
            var executor = new TaskExecutor(ctx);
			try
			{
				executor.Execute();	
			}
			catch (System.Exception ex) 
			{
				System.Console.WriteLine (ex.ToString ());
			}
        }
    }
}

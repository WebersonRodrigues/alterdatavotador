window.addEventListener("load",
    function() {
        if (location.hash === "#erroLogin" || location.hash === "#loginNaoEncontrado") {
            var msgErro = "";

            if (location.hash === "#erroLogin")
                msgErro = "Usuário ou senha inválidos.";
            else
                msgErro = "Usuário não encontrado no sistema.<br>Por favor, solicite seu cadastro.";

            var templateErro = document.createElement("div");
            templateErro.className = "alert alert-danger alert-dismissable center";
            templateErro.id = "msgErroLogin";
            templateErro.innerHTML = '<i class="fa fa-ban"></i>' +
                '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' +
                msgErro;

            document.body.insertBefore(templateErro, document.body.firstChild);
            window.location.hash = "";

            var usuario = location.search.replace("?", "");
            document.querySelector('input[name="usuario"]').value = usuario;
        }

        alterarMensagemInputInvalido(document.querySelector('input[name="usuario"]'), "Informe seu usuário");
        alterarMensagemInputInvalido(document.querySelector('input[name="senha"]'), "Informe sua senha");
        document.querySelector('input[name="usuario"]').focus();
    });

function alterarMensagemInputInvalido(input, mensagem) {
    input.addEventListener("invalid",
        function(e) {
            e.target.setCustomValidity(mensagem);
        });
    input.addEventListener("input",
        function(e) {
            e.target.setCustomValidity("");
        });
}
import Vue from 'vue'
import App from '@/components/App.vue'
import { sync } from 'vuex-router-sync'
import router from './router'
import store from './store'
import VueGoogleCharts from 'vue-google-charts'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import pt from 'vuetify/src/locale/pt.ts'
import en from 'vuetify/src/locale/en.ts'

Vue.use(VueGoogleCharts);
Vue.use(require('vue-moment'));
Vue.use(Vuetify, {
  lang: {
    locales: { en, pt },
    current: 'pt'
  }
});

sync(store, router);

new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>'
})

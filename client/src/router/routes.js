import Recursos from '@/components/Recursos'
import Consulta from '@/components/Consulta'

export default {
    paths: [
      { path: '/', name: 'Recursos', component: Recursos, displayOnNavbar: true },
      { path: '/consulta', name: 'Consulta', component: Consulta, displayOnNavbar: false}
    ]
}
import axios from 'axios'
import camelCaseObject from '@/utils/camelCaseObject'

function obterUsuarioLogado() {
    return new Promise((resolve, reject) => {
        axios.get('/usuario')
            .then((respose) => {
                resolve(camelCaseObject(respose.data))
            })
            .catch(reject)
    });
}

export default { 
    obterUsuarioLogado
}
import axios from 'axios'
import camelCaseObject from '@/utils/camelCaseObject'

function obterRecursos() {
    return new Promise((resolve, reject) => {
        axios.get('/recursos')
            .then((respose) => {
                // resolve(respose.data)
                resolve(camelCaseObject(respose.data))
            })
            .catch(reject)
    });
}

function obterVotacoes() {
    return new Promise((resolve, reject) => {
        axios.get('/votacoes')
            .then((respose) => {
                resolve(respose.data)
                // resolve(camelCaseObject(respose.data))
            })
            .catch(reject)
    });
}


function gravar(dados) {
    return new Promise((resolve, reject) => {
        axios.post('/votacoes', dados)
            .then((respose) => {
                resolve(respose)
            })
            .catch(reject)
    });
}

function alterar(dados) {
    return new Promise((resolve, reject) => {
        axios.put('/votacoes', dados)
            .then((respose) => {
                resolve(respose)
            })
            .catch(reject)
    });
}

export default { 
    obterRecursos,
    obterVotacoes,
    gravar,
    alterar
}
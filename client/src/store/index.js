import Vue from 'vue'
import Vuex from 'vuex'
import usuario from './modules/usuario'

Vue.use(Vuex);

// Importar os módulos aqui
const state = { }

export default new Vuex.Store({ 
    state,
    modules: {
        usuario: usuario
    }
});
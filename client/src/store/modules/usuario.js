import api from '@/api/usuario'

const state = {
    isAssigned: false,
    usuario: {
        userName: ""
    }
}

const getters = { }

const actions = {
    obterLogado ({ commit }) {
        api.obterUsuarioLogado()
            .then((usuario) => {
                commit('assign', { usuario: usuario });
            })
            .catch(() => {
                // colocar em alguma store de erros globais
            })
    }
}

const mutations = {
    assign (state, payload) {
        if(state.isAssigned)
            return;
        
        state.usuario = Object.assign({}, state.usuario, payload.usuario);
        state.isAssigned = true;
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
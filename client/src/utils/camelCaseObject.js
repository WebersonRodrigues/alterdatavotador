import _ from 'lodash/index.js'

export default function(obj) {
    return _.mapKeys(obj, (v, k) => _.camelCase(k));
}
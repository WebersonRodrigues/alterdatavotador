namespace AlterdataVotador.Test.Repositorio
{
    using AlterdataVotador.Dominio.Entidades.Classes;
    using AlterdataVotador.Dominio.Repositorio.Classes;
    using AlterdataVotador.Dominio.Repositorio.Interfaces;
    using NUnit.Framework;
    using AlterdataVotador.Test.Repositorio;
    using System.Linq;
    using System;
    using AlterdataVotador.Servicos.Classes;

    /// <summary>
    /// Classe para testar o serviço de votação
    /// </summary>
    public class ServicoVotacaoTeste
    {
        /// <summary>
        /// Provedor de conexao com o banco de dados de teste
        /// </summary>
        private IProvedorDeConexao ProvedorDeConexao;

        /// <summary>
        /// Serviço de votação
        /// </summary>
        private ServicoVotacao ServicoVotacao;

        /// <summary>
        /// Repositorio de Departamento
        /// </summary>
        private IRepositorioDepartamento RepositorioDepartamento;

        /// <summary>
        /// Repositorio de Funcao
        /// </summary>
        private IRepositorioFuncao RepositorioFuncao; 

        /// <summary>
        /// Repositorio de Funcionário
        /// </summary>
        private IRepositorioFuncionario RepositorioFuncionario;

        /// <summary>
        /// Repositorio de Recurso
        /// </summary>
        private RepositorioRecurso RepositorioRecurso;

        /// <summary>
        /// Repositorio de voto
        /// </summary>
        private RepositorioVoto RepositorioVoto; 

        /// <summary>
        /// Repositorio de Empresa
        /// </summary>
        private IRepositorioEmpresa RepositorioEmpresa;

        /// <summary>
        /// Empresa modelo para uso nos testes
        /// </summary>
        /// <returns>Empresa</returns>
        private Empresa Empresa = new Empresa(1, "2223","Empresa teste", null);

        /// <summary>
        /// Funcao modelo utilizado nos testes
        /// </summary>
        /// <returns>Funcao</returns>
        private Funcao Funcao = new Funcao(1, "2343","Programador C9", null);

        /// <summary>
        /// Departamento modelo utilizado nos testes
        /// </summary>
        /// <returns>Departamento</returns>
        private Departamento Departamento = new Departamento(1, "9898","Desenvolvimento Pack", null);

        /// <summary>
        /// Funcionario modelo utilizado nos testes
        /// </summary>
        /// <returns>Funcionario</returns>
        private Funcionario FuncionarioDoBancoDeDados;

        /// <summary>
        /// Recurso obtido do banco de dados
        /// </summary>
        /// <returns>Recurso</returns>
        private Recurso RecursoDoBancoDeDados; 

        /// <summary>
        /// Funcionário modelo para testes
        /// </summary>
        /// <returns>Funcionario</returns>
        private Funcionario FuncionarioModelo()
        {
            return new Funcionario(
                1,"Weberson Rodrigues","99999999999",
                "weberson.sqa@alterdata.com.br", 
                new Empresa(1, "0205", "Alterdata Software", null),
                new Departamento(1, "213","Desenvolvimento ERP", null),
                new Funcao(1, "0123", "Programador C1", null),"1234", null,
                new Guid());
        }
        
        /// <summary>
        /// Recurso modelo para os testes
        /// </summary>
        /// <returns>Recurso</returns>
        private Recurso RecursoModelo()
        {
            return new Recurso(1, "000123456",
                                   "Faturamento",
                                   "Descricção do processo...",
                                   "Detalhes do processo ...",
                                   DateTime.Now.AddHours(-1),
                                   Status.Aberto, null);
        }

        /// <summary>
        /// Voto modelo para os testes
        /// </summary>
        /// <returns>Voto</returns>
        private Voto VotoModelo()
        {
            return new Voto(1, DateTime.Now, FuncionarioDoBancoDeDados, RecursoDoBancoDeDados, "comentario de testes modelo");
        }

        [SetUp]
        public void Init()
        {
           ProvedorDeConexao = new ProvedorDeConexaoTeste();
           RepositorioDepartamento = new RepositorioDepartamento(ProvedorDeConexao);
           RepositorioFuncao = new RepositorioFuncao(ProvedorDeConexao);
           RepositorioEmpresa = new RepositorioEmpresa(ProvedorDeConexao);
           RepositorioFuncionario = new RepositorioFuncionario(ProvedorDeConexao, RepositorioDepartamento, RepositorioEmpresa, RepositorioFuncao);
           RepositorioRecurso = new RepositorioRecurso(ProvedorDeConexao);
           RepositorioVoto = new RepositorioVoto(ProvedorDeConexao, RepositorioFuncionario, RepositorioRecurso);
           ServicoVotacao = new ServicoVotacao(RepositorioFuncionario, RepositorioRecurso, RepositorioVoto);   
           RepositorioRecurso.DeletarTodos();
           RepositorioVoto.DeletarTodos(); 
           RepositorioRecurso.DeletarTodos();
           RepositorioFuncionario.Inserir(FuncionarioModelo());
           RepositorioRecurso.Inserir(RecursoModelo());

           FuncionarioDoBancoDeDados = RepositorioFuncionario.ObterTodos().FirstOrDefault();
           RecursoDoBancoDeDados = RepositorioRecurso.ObterTodos().FirstOrDefault();
        }

        private Recurso RecursoModelo(DateTime dataAbertura)
        {
            return new Recurso(1, "000123456",
                                   "Faturamento",
                                   "Descricção do processo...",
                                   "Detalhes do processo ...",
                                   dataAbertura,
                                   Status.Aberto, null);
        }
        
        [Test]
        public void Deve_cadastrar_e_obter_o_recurso_com_sucesso()
        {
            RepositorioRecurso.DeletarTodos();
            Assert.False(ServicoVotacao.ObterTodosOsRecursos().Any());
            ServicoVotacao.CadastrarRecurso(RecursoModelo(DateTime.Now));
            Assert.True(ServicoVotacao.ObterTodosOsRecursos().Any());
        }

        [Test]
        public void Deve_cadastrar_e_obter_o_voto_com_sucesso()
        {
            Assert.False(ServicoVotacao.ObterTodosOsVotos().Any());
            ServicoVotacao.CadastrarVoto(FuncionarioDoBancoDeDados.Id, RecursoDoBancoDeDados.Id, DateTime.Now, "Comentário do voto");
            Assert.True(ServicoVotacao.ObterTodosOsRecursos().Any());
        }

        [Test]
        public void Deve_cadastrar_pelo_Objeto_Voto_e_obter_o_voto_com_sucesso()
        {
            Assert.False(ServicoVotacao.ObterTodosOsVotos().Any());
            ServicoVotacao.CadastrarVoto(VotoModelo());
            Assert.True(ServicoVotacao.ObterTodosOsRecursos().Any());
        }

        [Test]
        public void Deve_obter_todos_os_recursos_pela_data_de_abertura_com_sucesso()
        {
            RepositorioRecurso.DeletarTodos();
            Assert.False(ServicoVotacao.ObterTodosOsRecursos().Any());
            ServicoVotacao.CadastrarRecurso(RecursoModelo(DateTime.Now));
            ServicoVotacao.CadastrarRecurso(RecursoModelo(DateTime.Now.AddDays(-1)));
            ServicoVotacao.CadastrarRecurso(RecursoModelo(DateTime.Now.AddDays(1)));
            ServicoVotacao.CadastrarRecurso(RecursoModelo(DateTime.Now.AddDays(2)));
            Assert.True(ServicoVotacao.Obter(DateTime.Now, DateTime.Now.AddDays(2)).Count() == 3);
        }

        [Test]
        public void Deve_obter_os_votos_de_um_funcionario_com_sucesso()
        {
            Assert.False(ServicoVotacao.ObterTodosOsVotos().Any());
            ServicoVotacao.CadastrarVoto(VotoModelo());
            Assert.True(ServicoVotacao.Obter(FuncionarioDoBancoDeDados).Any());
        }

  
    }
}
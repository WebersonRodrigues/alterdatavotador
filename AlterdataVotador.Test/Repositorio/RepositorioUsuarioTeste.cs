﻿namespace AlterdataVotador.Test.Repositorio
{
    using AlterdataVotador.Dominio.Entidades.Classes;
    using AlterdataVotador.Dominio.Repositorio.Classes;
    using AlterdataVotador.Dominio.Repositorio.Interfaces;
    using NUnit.Framework;
    using AlterdataVotador.Test.Repositorio;
    using System.Linq;
    using System;

    /// <summary>
    /// Classe responsável por todos os testes referente ao Repositorio de Usuario
    /// </summary>
    public class RepositorioUsuarioTeste
    {
        /// <summary>
        /// Repositorio de Usuario
        /// </summary>
        private RepositorioUsuario RepositorioUsuario;

        /// <summary>
        /// Provedor de conexão
        /// </summary>
        private IProvedorDeConexao ProvedorDeConexao;
        private Funcionario FuncionarioDoBancoDeDados;

        /// <summary>
        /// Usuario modelo para os testes
        /// </summary>
        /// <returns>Usuario</returns>
        private Usuario UsuarioModelo()
        {
            return new Usuario(1, "Weberson.dsn.erp",
                                new Guid("5b193030-04c4-4277-8b77-c74511f58023"), 
                                1, FuncionarioDoBancoDeDados);
        }

        /// <summary>
        /// Funcionario modelo para os testes
        /// </summary>
        /// <returns>Funcionario</returns>
        public Funcionario FuncionarioModelo()
        {
            return new Funcionario(
                1,"Weberson Rodrigues","99999999999",
                "weberson.sqa@alterdata.com.br", 
                new Empresa(1, "0205", "Alterdata Software", null),
                new Departamento(1, "213","Desenvolvimento ERP", null),
                new Funcao(1, "0123", "Programador C1", null),"1234", null,
                new Guid());
        }

        [SetUp]
        public void Init()
        {
            ProvedorDeConexao = new ProvedorDeConexaoTeste();
            var repDepartamento = new RepositorioDepartamento(ProvedorDeConexao);
            var repEmpresa = new RepositorioEmpresa(ProvedorDeConexao);
            var repFuncao = new RepositorioFuncao(ProvedorDeConexao);
            var repFuncionario = new RepositorioFuncionario(ProvedorDeConexao, repDepartamento, repEmpresa, repFuncao);
            RepositorioUsuario = new RepositorioUsuario(ProvedorDeConexao, repFuncionario);

            RepositorioUsuario.DeletarTodos();
            repFuncionario.Inserir(FuncionarioModelo());
            FuncionarioDoBancoDeDados = repFuncionario.ObterTodos().FirstOrDefault();
        }

        [Test]
        public void Deve_inserir_e_buscar_com_sucesso()
        {
            Assert.False(RepositorioUsuario.ObterTodos().Any());
            RepositorioUsuario.Inserir(UsuarioModelo());
            Assert.True(RepositorioUsuario.ObterTodos().Any());
        }

        [Test]
        public void Deve_alterar_com_sucesso()
        {
            RepositorioUsuario.Inserir(UsuarioModelo());
            var usuario = RepositorioUsuario.ObterTodos().FirstOrDefault();
            usuario.Guid = new Guid();
            usuario.Login = "novoLogin";
            usuario.PerfilId = 3;

            RepositorioUsuario.Atualizar(usuario);

            var usuarioAtualizado = RepositorioUsuario.Obter(usuario.Id);

            Assert.AreEqual(usuario.Login.ToUpper(), usuarioAtualizado.Login);
            Assert.AreEqual(usuario.Guid, usuarioAtualizado.Guid);
            Assert.AreEqual(usuario.PerfilId, usuarioAtualizado.PerfilId);

        }

        [Test]
        public void Deve_obter_todos_com_sucesso()
        {
           Assert.False(RepositorioUsuario.ObterTodos().Any());

           RepositorioUsuario.Inserir(UsuarioModelo());
           Assert.True(RepositorioUsuario.ObterTodos().Count() == 1);

           RepositorioUsuario.Inserir(UsuarioModelo());
           Assert.True(RepositorioUsuario.ObterTodos().Count() == 2);
        }

        [Test]
        public void Deve_excluir_todos_com_sucesso()
        {
            RepositorioUsuario.Inserir(UsuarioModelo());
            RepositorioUsuario.Inserir(UsuarioModelo());
           
            RepositorioUsuario.DeletarTodos();
            Assert.False(RepositorioUsuario.ObterTodos().Any());
        }

        [Test]
        public void Deve_obter_usuario_pelo_login()
        {
            RepositorioUsuario.Inserir(UsuarioModelo());

            var usuario = RepositorioUsuario.ObterTodos().FirstOrDefault();

            Assert.AreEqual(usuario, RepositorioUsuario.Obter(usuario.Login));
        }

    }
}
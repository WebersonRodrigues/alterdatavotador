namespace AlterdataVotador.Test.Repositorio
{
    using AlterdataVotador.Dominio.Entidades.Classes;
    using AlterdataVotador.Dominio.Repositorio.Classes;
    using AlterdataVotador.Dominio.Repositorio.Interfaces;
    using NUnit.Framework;
    using AlterdataVotador.Test.Repositorio;
    using System.Linq;
    using System;

    /// <summary>
    /// Cllase para testar o repositorio de Departamento
    /// </summary>
    public class RepositorioDepartamentoTeste
    {
        /// <summary>
        /// Provedor de conexão com a base de dados de Teste
        /// </summary>
        private IProvedorDeConexao ProvedorDeConexao;

        /// <summary>
        /// Repositorio de Departamento
        /// </summary>
        private RepositorioDepartamento RepositorioDepartamento;
      
        /// <summary>
        /// Departamento modelo utilizado para os Testes
        /// </summary>
        /// <returns>Departamento</returns>
        public Departamento DepartamentoModelo()
        {
            return new Departamento(1, "213","Desenvolvimento ERP", null);
        }

        [SetUp]
        public void Init()
        {
           ProvedorDeConexao = new ProvedorDeConexaoTeste();
           RepositorioDepartamento = new RepositorioDepartamento(ProvedorDeConexao);
           RepositorioDepartamento.DeletarTodos();
        }

        
        [Test]
        public void Deve_inserir_e_buscar_com_sucesso()
        {
            Assert.False(RepositorioDepartamento.ObterTodos().Any());
            RepositorioDepartamento.Inserir(DepartamentoModelo());
            Assert.True(RepositorioDepartamento.ObterTodos().Any());
        }

        [Test]
        public void Deve_alterar_e_buscar_pelo_id_com_sucesso()
        {
            RepositorioDepartamento.Inserir(DepartamentoModelo());
            var dep = RepositorioDepartamento.ObterTodos().FirstOrDefault();
            dep.Nome = "Departamento Teste 1234";
            dep.Codigo = "999999";
            dep.DataInativacao = DateTime.Now;

            RepositorioDepartamento.Atualizar(dep);
            var depAlterado = RepositorioDepartamento.Obter(dep.Id);

            Assert.AreEqual(dep.Id , depAlterado.Id);
            Assert.AreEqual(dep.Nome , depAlterado.Nome);
            Assert.AreEqual(dep.Codigo , depAlterado.Codigo);
            Assert.AreEqual(dep.DataInativacao.Value.Date , depAlterado.DataInativacao.Value.Date);
        }

        [Test]
        public void Deve_buscar_pelo_codigo_com_sucesso()
        {
            var dep = DepartamentoModelo();
            RepositorioDepartamento.Inserir(dep);
            Assert.AreEqual(dep.Codigo, RepositorioDepartamento.Obter(dep.Codigo).Codigo);
        }

        [Test]
        public void Deve_obter_todos_com_sucesso()
        {
            RepositorioDepartamento.Inserir(DepartamentoModelo());
            Assert.AreEqual(1, RepositorioDepartamento.ObterTodos().Count());

            RepositorioDepartamento.Inserir(DepartamentoModelo());
            Assert.AreEqual(2, RepositorioDepartamento.ObterTodos().Count());

            RepositorioDepartamento.Inserir(DepartamentoModelo());
            Assert.AreEqual(3, RepositorioDepartamento.ObterTodos().Count());

            RepositorioDepartamento.DeletarTodos();
            Assert.False(RepositorioDepartamento.ObterTodos().Any());
        }
    }
}
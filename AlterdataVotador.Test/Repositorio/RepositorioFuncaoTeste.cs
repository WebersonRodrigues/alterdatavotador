namespace AlterdataVotador.Test.Repositorio
{
    using AlterdataVotador.Dominio.Entidades.Classes;
    using AlterdataVotador.Dominio.Repositorio.Classes;
    using AlterdataVotador.Dominio.Repositorio.Interfaces;
    using NUnit.Framework;
    using AlterdataVotador.Test.Repositorio;
    using System.Linq;
    using System;

    /// <summary>
    /// Classe responsável pelos teste do Repositorio de Funcao
    /// </summary>
    public class RepositorioFuncaoTeste
    {
        /// <summary>
        /// Provedor de conexão com a base de dados de Teste
        /// </summary>
        private IProvedorDeConexao ProvedorDeConexao;

        /// <summary>
        /// Repositorio de Funcao
        /// </summary>
        private RepositorioFuncao RepositorioFuncao;
      
        /// <summary>
        /// Funcao modelo utilizado nos testes
        /// </summary>
        /// <returns>Funcao</returns>
        public Funcao FuncaoModelo()
        {
            return new Funcao(1, "0123", "Programador C1", null);
        }

        [SetUp]
        public void Init()
        {
           ProvedorDeConexao = new ProvedorDeConexaoTeste();
           RepositorioFuncao = new RepositorioFuncao(ProvedorDeConexao);
           RepositorioFuncao.DeletarTodos();
        }

        
        [Test]
        public void Deve_inserir_e_buscar_com_sucesso()
        {
            Assert.False(RepositorioFuncao.ObterTodos().Any());
            RepositorioFuncao.Inserir(FuncaoModelo());
            Assert.True(RepositorioFuncao.ObterTodos().Any());
        }

        [Test]
        public void Deve_alterar_e_buscar_pelo_id_com_sucesso()
        {
            RepositorioFuncao.Inserir(FuncaoModelo());
            var funcao = RepositorioFuncao.ObterTodos().FirstOrDefault();
            funcao.Nome = "Especialista 1";
            funcao.Codigo = "1234";
            funcao.DataInativacao = DateTime.Now;

            RepositorioFuncao.Atualizar(funcao);
            var funcaoAlterada = RepositorioFuncao.Obter(funcao.Id);

            Assert.AreEqual(funcao.Id , funcaoAlterada.Id);
            Assert.AreEqual(funcao.Nome , funcaoAlterada.Nome);
            Assert.AreEqual(funcao.Codigo , funcaoAlterada.Codigo);
            Assert.AreEqual(funcao.DataInativacao.Value.Date , funcaoAlterada.DataInativacao.Value.Date);
        }

        [Test]
        public void Deve_buscar_pelo_codigo_com_sucesso()
        {
            var funcao = FuncaoModelo();
            RepositorioFuncao.Inserir(funcao);
            Assert.AreEqual(funcao.Codigo, RepositorioFuncao.Obter(funcao.Codigo).Codigo);
        }
        
        [Test]
        public void Deve_obter_todos_com_sucesso()
        {
            RepositorioFuncao.Inserir(FuncaoModelo());
            Assert.AreEqual(1, RepositorioFuncao.ObterTodos().Count());
            
            RepositorioFuncao.Inserir(FuncaoModelo());
            Assert.AreEqual(2, RepositorioFuncao.ObterTodos().Count());

            RepositorioFuncao.DeletarTodos();
            Assert.False(RepositorioFuncao.ObterTodos().Any());
        }
    }
}
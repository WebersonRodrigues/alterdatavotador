﻿namespace AlterdataVotador.Test.Repositorio
{   
    using System.Data;
    using AlterdataVotador.Dominio.Repositorio.Interfaces;

    /// <summary>
    /// Provedor de conexao para os testes unitários
    /// </summary>
    public class ProvedorDeConexaoTeste : IProvedorDeConexao
    {
        /// <summary>
        /// String de conexão com a base de teste
        /// </summary>
        private const string STRING_DE_CONEXAO = "Server=localhost;Port=5432;Database=alterdata_votador_teste;User Id=postgres;Password=123456;CommandTimeout=60;Timeout=60;";

        /// <summary>
        /// Cria uma nova conexao com a base de teste
        /// </summary>
        /// <returns></returns>
        public IDbConnection CriarNovaConexao()
        {
            return new Npgsql.NpgsqlConnection(STRING_DE_CONEXAO);
        }

    }
}
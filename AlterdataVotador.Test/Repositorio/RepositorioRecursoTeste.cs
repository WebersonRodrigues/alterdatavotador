namespace AlterdataVotador.Test.Repositorio
{
    using AlterdataVotador.Dominio.Entidades.Classes;
    using AlterdataVotador.Dominio.Repositorio.Classes;
    using AlterdataVotador.Dominio.Repositorio.Interfaces;
    using NUnit.Framework;
    using AlterdataVotador.Test.Repositorio;
    using System.Linq;
    using System;


    /// <summary>
    /// Classe responsavel pelos testes do repositorio de Recurso
    /// </summary>
    public class RepositorioRecursoTeste
    {
        /// <summary>
        /// Provedor de conexao com a base de testes
        /// </summary>
        private IProvedorDeConexao ProvedorDeConexao;

        /// <summary>
        /// Repositorio de Recurso
        /// </summary>
        private RepositorioRecurso RepositorioRecurso;

        /// <summary>
        /// Recurso modelo utilziado nos testes
        /// </summary>
        /// <param name="dataAbertura">Data de abertura do recurso</param>
        /// <returns></returns>
        public Recurso RecursoModelo(DateTime dataAbertura)
        {
            return new Recurso(1, "000123456",
                                   "Faturamento",
                                   "Descricção do processo...",
                                   "Detalhes do processo ...",
                                   dataAbertura,
                                   Status.Aberto, null);
        }

        [SetUp]
        public void Init()
        {
           ProvedorDeConexao = new ProvedorDeConexaoTeste();
           RepositorioRecurso = new RepositorioRecurso(ProvedorDeConexao);
           RepositorioRecurso.DeletarTodos();
        }

        
        [Test]
        public void Deve_inserir_e_buscar_com_sucesso()
        {
            Assert.False(RepositorioRecurso.ObterTodos().Any());
            RepositorioRecurso.Inserir(RecursoModelo(DateTime.Now));
            Assert.True(RepositorioRecurso.ObterTodos().Any());
        }

        [Test]
        public void Deve_alterar_e_buscar_pelo_id_com_sucesso()
        {
            RepositorioRecurso.Inserir(RecursoModelo(DateTime.Now));
            var recurso = RepositorioRecurso.ObterTodos().FirstOrDefault();
            recurso.CodigoProcesso = "000221133";
            recurso.Descricao = "Nova descrição";
            recurso.DataAlteracao = DateTime.Now;
            recurso.Detalhes = "Novo detalhe";
            recurso.NomeProduto = "Disponível";
            recurso.Status = Status.Cancelado;
            recurso.DataAlteracao = DateTime.Now;
    
            RepositorioRecurso.Atualizar(recurso);
            var recursoAlterado = RepositorioRecurso.Obter(recurso.Id);

            Assert.AreEqual(recurso.Id , recursoAlterado.Id);
            Assert.AreEqual(recurso.CodigoProcesso , recursoAlterado.CodigoProcesso);
            Assert.AreEqual(recurso.Descricao , recursoAlterado.Descricao);
            Assert.AreEqual(recurso.Detalhes , recursoAlterado.Detalhes);
            Assert.AreEqual(recurso.NomeProduto , recursoAlterado.NomeProduto);
            Assert.AreEqual(recurso.Status, recursoAlterado.Status);
            Assert.AreEqual(recurso.DataAlteracao.Value.Date , recursoAlterado.DataAlteracao.Value.Date);
            Assert.AreEqual(recurso.DataAbertura.Date , recursoAlterado.DataAbertura.Date);
        }

        [Test]
        public void Deve_buscar_pelo_codigo_com_sucesso()
        {
            var recurso = RecursoModelo(DateTime.Now);
            RepositorioRecurso.Inserir(recurso);

            var recursoObtido = RepositorioRecurso.Obter(recurso.CodigoProcesso);
            Assert.AreEqual(recurso.CodigoProcesso, recursoObtido.CodigoProcesso);
        }

        [Test]
        public void Deve_buscar_pelo_data_de_abertura_com_sucesso()
        {
            var recurso1 = RecursoModelo(DateTime.Now.AddDays(-3));
            var recurso2 = RecursoModelo(DateTime.Now.AddDays(-2));
            var recurso3 = RecursoModelo(DateTime.Now.AddDays(-1));
            var recurso4 = RecursoModelo(DateTime.Now.AddDays(+2));

            RepositorioRecurso.Inserir(recurso1);
            RepositorioRecurso.Inserir(recurso2);
            RepositorioRecurso.Inserir(recurso3);
            RepositorioRecurso.Inserir(recurso4);

            var resultado = RepositorioRecurso.Obter(DateTime.Now.AddDays(-2), DateTime.Now);
            Assert.True(resultado.Count() == 2);
        }

        [Test]
        public void Deve_obter_todos_com_sucesso()
        {
            RepositorioRecurso.Inserir(RecursoModelo(DateTime.Now));
            Assert.AreEqual(1, RepositorioRecurso.ObterTodos().Count());

            RepositorioRecurso.Inserir(RecursoModelo(DateTime.Now));
            Assert.AreEqual(2, RepositorioRecurso.ObterTodos().Count());

            RepositorioRecurso.DeletarTodos();
            Assert.False(RepositorioRecurso.ObterTodos().Any());
        }
    }
}
namespace AlterdataVotador.Test.Repositorio
{
    using AlterdataVotador.Dominio.Entidades.Classes;
    using AlterdataVotador.Dominio.Repositorio.Classes;
    using AlterdataVotador.Dominio.Repositorio.Interfaces;
    using NUnit.Framework;
    using AlterdataVotador.Test.Repositorio;
    using System.Linq;
    using System;

    /// <summary>
    /// Classe responsável por efetuar os testes com o repositorio de Funcionario
    /// </summary>
    public class RepositorioFuncionarioTeste
    {
        /// <summary>
        /// Provedor de conexao coma base de testes
        /// </summary>
        private IProvedorDeConexao ProvedorDeConexao;

        /// <summary>
        /// Repositorio de funcionario
        /// </summary>
        private RepositorioFuncionario RepositorioFuncionario;     

        /// <summary>
        /// Empresa modelo utilizada nos testes
        /// </summary>
        /// <returns>Empresa</returns>
        private Empresa Empresa = new Empresa(1, "2223","Empresa teste", null);

        /// <summary>
        /// Funcao modelo utilizada nos testes
        /// </summary>
        /// <returns>Funcao</returns>
        private Funcao Funcao = new Funcao(1, "2343","Programador C9", null);

        /// <summary>
        /// EDepartamento modelo utilizado nos testes
        /// </summary>
        /// <returns>Departamento</returns>
        private Departamento Departamento = new Departamento(1, "9898","Desenvolvimento Pack", null); 

        /// <summary>
        /// Funcionario modelo utilizada nos testes
        /// </summary>
        /// <returns>Funcionario</returns>
        public Funcionario FuncionarioModelo()
        {
            return new Funcionario(
                1,"Weberson Rodrigues","99999999999",
                "weberson.sqa@alterdata.com.br", 
                new Empresa(1, "0205", "Alterdata Software", null),
                new Departamento(1, "213","Desenvolvimento ERP", null),
                new Funcao(1, "0123", "Programador C1", null),"1234", null,
                new Guid());
        }

        [SetUp]
        public void Init()
        {
           ProvedorDeConexao = new ProvedorDeConexaoTeste();
           var repDepartamento = new RepositorioDepartamento(ProvedorDeConexao);
           var repEmpresa = new RepositorioEmpresa(ProvedorDeConexao);
           var repFuncao = new RepositorioFuncao(ProvedorDeConexao);
           RepositorioFuncionario = new RepositorioFuncionario(ProvedorDeConexao, repDepartamento, repEmpresa, repFuncao);

           RepositorioFuncionario.DeletarTodos();
           repEmpresa.DeletarTodos();
           repDepartamento.DeletarTodos();
           repFuncao.DeletarTodos();

           repDepartamento.Inserir(Departamento);
           repEmpresa.Inserir(Empresa);
           repFuncao.Inserir(Funcao);
        
           //Como o id é private, não posso utilizar ele para setar um novo valor
           Departamento = repDepartamento.Obter(Departamento.Codigo);
           Empresa = repEmpresa.Obter(Empresa.Codigo);
           Funcao = repFuncao.Obter(Funcao.Codigo);
        }

        
        [Test]
        public void Deve_inserir_e_buscar_com_sucesso()
        {
            Assert.False(RepositorioFuncionario.ObterTodos().Any());
            RepositorioFuncionario.Inserir(FuncionarioModelo());
            Assert.True(RepositorioFuncionario.ObterTodos().Any());
        }

        [Test]
        public void Deve_alterar_e_buscar_pelo_id_com_sucesso()
        {
            RepositorioFuncionario.Inserir(FuncionarioModelo());
            var funcionario = RepositorioFuncionario.ObterTodos().FirstOrDefault();

            funcionario.Nome = "Funcionario Teste 1234";
            funcionario.Empresa = Empresa;
            funcionario.Departamento = Departamento;
            funcionario.Funcao = Funcao;
            funcionario.DataInativacao = DateTime.Now;
            funcionario.Ramal= "1234";
            funcionario.GuidAD = new Guid();
            funcionario.CPF= "123456";


            RepositorioFuncionario.Atualizar(funcionario);
            var funcionarioAlterado = RepositorioFuncionario.Obter(funcionario.Id);

            Assert.AreEqual(funcionario.Id , funcionarioAlterado.Id);
            Assert.AreEqual(funcionario.Nome , funcionarioAlterado.Nome);
            Assert.AreEqual(funcionario.Departamento.Codigo , funcionarioAlterado.Departamento.Codigo);
            Assert.AreEqual(funcionario.Funcao.Codigo , funcionarioAlterado.Funcao.Codigo);
            Assert.AreEqual(funcionario.Empresa.Codigo , funcionarioAlterado.Empresa.Codigo);
            Assert.AreEqual(funcionario.Email , funcionarioAlterado.Email);
            Assert.AreEqual(funcionario.CPF , funcionarioAlterado.CPF);
            Assert.AreEqual(funcionario.DataInativacao.Value.Date , funcionarioAlterado.DataInativacao.Value.Date);
        }

        [Test]
        public void Deve_buscar_pelo_email_com_sucesso()
        {
            var funcionario = FuncionarioModelo();
            RepositorioFuncionario.Inserir(funcionario);
            Assert.AreEqual(funcionario.Email, RepositorioFuncionario.Obter(funcionario.Email).Email);
        }

        [Test]
        public void Deve_obter_todos_com_sucesso()
        {
            RepositorioFuncionario.Inserir(FuncionarioModelo());
            Assert.AreEqual(1, RepositorioFuncionario.ObterTodos().Count());

            RepositorioFuncionario.Inserir(FuncionarioModelo());
            Assert.AreEqual(2, RepositorioFuncionario.ObterTodos().Count());

            RepositorioFuncionario.DeletarTodos();
            Assert.False(RepositorioFuncionario.ObterTodos().Any());
        }
    }
}
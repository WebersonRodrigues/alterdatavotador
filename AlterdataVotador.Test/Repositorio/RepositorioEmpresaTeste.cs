namespace AlterdataVotador.Test.Repositorio
{
    using AlterdataVotador.Dominio.Entidades.Classes;
    using AlterdataVotador.Dominio.Repositorio.Classes;
    using AlterdataVotador.Dominio.Repositorio.Interfaces;
    using NUnit.Framework;
    using AlterdataVotador.Test.Repositorio;
    using System.Linq;
    using System;

    /// <summary>
    /// Classe responsável pelos testes unitários do Repositorio de Empresa
    /// </summary>
    public class RepositorioEmpresaTeste
    {
        /// <summary>
        /// Provedor de conexão com a base de dados de Teste
        /// </summary>
        private IProvedorDeConexao ProvedorDeConexao;

        /// <summary>
        /// Repositorio da Empresa
        /// </summary>
        private RepositorioEmpresa RepositorioEmpresa;
      
        /// <summary>
        /// Empresa modelo utilizada nos testes
        /// </summary>
        /// <returns>Empresa</returns>
        public Empresa EmpresaModelo()
        {
            return new Empresa(1, "0205", "Alterdata Software", null);
        }

        [SetUp]
        public void Init()
        {
           ProvedorDeConexao = new ProvedorDeConexaoTeste();
           RepositorioEmpresa = new RepositorioEmpresa(ProvedorDeConexao);
           RepositorioEmpresa.DeletarTodos();
        }

        
        [Test]
        public void Deve_inserir_e_buscar_com_sucesso()
        {
            Assert.False(RepositorioEmpresa.ObterTodos().Any());
            RepositorioEmpresa.Inserir(EmpresaModelo());
            Assert.True(RepositorioEmpresa.ObterTodos().Any());
        }

        [Test]
        public void Deve_alterar_e_buscar_pelo_id_com_sucesso()
        {
            RepositorioEmpresa.Inserir(EmpresaModelo());
            var empresa = RepositorioEmpresa.ObterTodos().FirstOrDefault();
            empresa.Nome = "Empresa Teste 1234";
            empresa.Codigo = "999999";
            empresa.DataInativacao = DateTime.Now;

            RepositorioEmpresa.Atualizar(empresa);
            var empresaAlterada = RepositorioEmpresa.Obter(empresa.Id);

            Assert.AreEqual(empresa.Id , empresaAlterada.Id);
            Assert.AreEqual(empresa.Nome , empresaAlterada.Nome);
            Assert.AreEqual(empresa.Codigo , empresaAlterada.Codigo);
            Assert.AreEqual(empresa.DataInativacao.Value.Date , empresaAlterada.DataInativacao.Value.Date);
        }

        [Test]
        public void Deve_buscar_pelo_codigo_com_sucesso()
        {
            var empresa = EmpresaModelo();
            RepositorioEmpresa.Inserir(empresa);
            Assert.AreEqual(empresa.Codigo, RepositorioEmpresa.Obter(empresa.Codigo).Codigo);
        }

        [Test]
        public void Deve_obter_todos_com_sucesso()
        {
            RepositorioEmpresa.Inserir(EmpresaModelo());
            Assert.AreEqual(1, RepositorioEmpresa.ObterTodos().Count());

            RepositorioEmpresa.Inserir(EmpresaModelo());
            Assert.AreEqual(2, RepositorioEmpresa.ObterTodos().Count());

            RepositorioEmpresa.DeletarTodos();
            Assert.False(RepositorioEmpresa.ObterTodos().Any());
        }
    }
}
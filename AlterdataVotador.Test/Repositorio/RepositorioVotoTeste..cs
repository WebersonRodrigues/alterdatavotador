namespace AlterdataVotador.Test.Repositorio
{
    using AlterdataVotador.Dominio.Entidades.Classes;
    using AlterdataVotador.Dominio.Repositorio.Classes;
    using AlterdataVotador.Dominio.Repositorio.Interfaces;
    using NUnit.Framework;
    using AlterdataVotador.Test.Repositorio;
    using System.Linq;
    using System;

    /// <summary>
    /// Classe responsável pelos testes do Repositorio de Voto
    /// </summary>
    public class RepositorioVotoTeste
    {
        /// <summary>
        /// Provedor de conexao com o banco de dados de teste
        /// </summary>
        private IProvedorDeConexao ProvedorDeConexao;

        /// <summary>
        /// Repositorio de Voto
        /// </summary>
        private RepositorioVoto RepositorioDeVoto;

        /// <summary>
        /// Empresa modelo para os testes
        /// </summary>
        /// <returns>Empresa</returns>
        private Empresa Empresa = new Empresa(1, "2223","Empresa teste", null);

         /// <summary>
        /// Funcao modelo para os testes
        /// </summary>
        /// <returns>Funcao</returns>
        private Funcao Funcao = new Funcao(1, "2343","Programador C9", null);

        /// <summary>
        /// Departamento modelo para os testes
        /// </summary>
        /// <returns>Departamento</returns>
        private Departamento Departamento = new Departamento(1, "9898","Desenvolvimento Pack", null);
        
        /// <summary>
        /// Funcionario obtido do banco de dados para os testes
        /// </summary>
        /// <returns>Funcionario</returns>
        private Funcionario FuncionarioDoBancoDeDados;

        /// <summary>
        /// Recurso obtido do banco de dados para os testes
        /// </summary>
        /// <returns>Recurso</returns>
        private Recurso RecursoDoBancoDeDados; 

        /// <summary>
        /// Funcionário modelo para os testes
        /// </summary>
        /// <returns>Funcionario</returns>
        private Funcionario FuncionarioModelo()
        {
            return new Funcionario(
                1,"Weberson Rodrigues","99999999999",
                "weberson.sqa@alterdata.com.br", 
                new Empresa(1, "0205", "Alterdata Software", null),
                new Departamento(1, "213","Desenvolvimento ERP", null),
                new Funcao(1, "0123", "Programador C1", null),"1234", null,
                new Guid());
        }
        
        /// <summary>
        /// Recurso modelo para os testes
        /// </summary>
        /// <returns>Recurso</returns>
        private Recurso RecursoModelo()
        {
            return new Recurso(1, "000123456",
                                   "Faturamento",
                                   "Descricção do processo...",
                                   "Detalhes do processo ...",
                                   DateTime.Now.AddHours(-1),
                                   Status.Aberto, null);
        }

        /// <summary>
        /// Voto modelo para os testes
        /// </summary>
        /// <returns>Voto</returns>
        public Voto VotoModelo()
        {
            return new Voto(1, DateTime.Now, FuncionarioDoBancoDeDados, RecursoDoBancoDeDados, "comentario de testes modelo");
        }

        [SetUp]
        public void Init()
        {
           ProvedorDeConexao = new ProvedorDeConexaoTeste();
           var repDepartamento = new RepositorioDepartamento(ProvedorDeConexao);
           var repEmpresa = new RepositorioEmpresa(ProvedorDeConexao);
           var repFuncao = new RepositorioFuncao(ProvedorDeConexao);
           var repFuncionario = new RepositorioFuncionario(ProvedorDeConexao, repDepartamento, repEmpresa, repFuncao);
           var repRecurso = new RepositorioRecurso(ProvedorDeConexao);
           RepositorioDeVoto = new RepositorioVoto(ProvedorDeConexao,repFuncionario,repRecurso);

           RepositorioDeVoto.DeletarTodos();
           repFuncionario.DeletarTodos();
           repRecurso.DeletarTodos();
           
           repFuncionario.Inserir(FuncionarioModelo());
           repRecurso.Inserir(RecursoModelo());

           FuncionarioDoBancoDeDados = repFuncionario.ObterTodos().FirstOrDefault();
           RecursoDoBancoDeDados = repRecurso.ObterTodos().FirstOrDefault();
        }

        
        [Test]
        public void Deve_inserir_e_buscar_com_sucesso()
        {
            Assert.False(RepositorioDeVoto.ObterTodos().Any());
            RepositorioDeVoto.Inserir(VotoModelo());
            Assert.True(RepositorioDeVoto.ObterTodos().Any());
        }

        [Test]
        public void Deve_alterar_e_buscar_pelo_id_com_sucesso()
        {
            RepositorioDeVoto.Inserir(VotoModelo());
            var voto = RepositorioDeVoto.ObterTodos().FirstOrDefault();
            voto.DataRealizado = DateTime.Now;
            voto.Comentario = "Comentário atualizado";

            RepositorioDeVoto.Atualizar(voto);
            var votoAlterado = RepositorioDeVoto.Obter(voto.Id);

            Assert.AreEqual(voto.Id , votoAlterado.Id);
            Assert.AreEqual(voto.DataRealizado.Date , votoAlterado.DataRealizado.Date);
            Assert.AreEqual(voto.Comentario , votoAlterado.Comentario);
        }

        [Test]
        public void Deve_buscar_pelo_funcionario_id_com_sucesso()
        {
            var voto = VotoModelo();
            RepositorioDeVoto.Inserir(voto);
            Assert.AreEqual(voto.DataRealizado.Date, RepositorioDeVoto.Obter(voto.Funcionario).DataRealizado.Date);
        }

        [Test]
        public void Deve_obter_uma_lista_de_votos_dentro_de_um_periodo_com_sucesso()
        {
            var voto1 = VotoModelo();
            var voto2 = VotoModelo();
            var voto3 = VotoModelo();
            var voto4 = VotoModelo();

            voto2.DataRealizado = DateTime.Now.AddDays(1);
            voto3.DataRealizado = DateTime.Now.AddDays(2);
            voto4.DataRealizado = DateTime.Now.AddDays(6);

            RepositorioDeVoto.Inserir(voto1);
            RepositorioDeVoto.Inserir(voto2);
            RepositorioDeVoto.Inserir(voto3);
            RepositorioDeVoto.Inserir(voto4);

            Assert.AreEqual(1, RepositorioDeVoto.Obter(DateTime.Now, DateTime.Now).Count());
            Assert.AreEqual(2, RepositorioDeVoto.Obter(DateTime.Now, DateTime.Now.AddDays(1)).Count());
            Assert.AreEqual(3, RepositorioDeVoto.Obter(DateTime.Now, DateTime.Now.AddDays(2)).Count());
            Assert.AreEqual(4, RepositorioDeVoto.Obter(DateTime.Now, DateTime.Now.AddDays(6)).Count());
        }

        [Test]
        public void Deve_obter_todos_com_sucesso()
        {
            RepositorioDeVoto.Inserir(VotoModelo());
            Assert.AreEqual(1, RepositorioDeVoto.ObterTodos().Count());

            RepositorioDeVoto.Inserir(VotoModelo());
            Assert.AreEqual(2, RepositorioDeVoto.ObterTodos().Count());

            RepositorioDeVoto.DeletarTodos();
            Assert.False(RepositorioDeVoto.ObterTodos().Any());
        }

        [Test]
        public void Deve_buscar_pelo_funcionario_com_sucesso()
        {
            var voto = VotoModelo();
            RepositorioDeVoto.Inserir(voto);

            var votoObtido = RepositorioDeVoto.ObterPorFuncionario(FuncionarioDoBancoDeDados);
        
            Assert.True(votoObtido.Any());
        }
    }
}